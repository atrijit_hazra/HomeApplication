package com.velarudh.tms.adapter;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.velarudh.tms.R;
import com.velarudh.tms.model.DateExtendDetails;
import com.velarudh.tms.model.DrawerItem;
import com.velarudh.tms.task.DateExtend;
import com.velarudh.tms.util.Url;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Atrijit on 01-06-2017.
 */

public class Date_Extend_adapter extends RecyclerView.Adapter<Date_Extend_adapter.VHolder> {

    private Context context;
    private ArrayList<DateExtendDetails> dateextendlist;
    String TAG = "Date_Extend_adapter";

    String extend_status;
    String JSON_URL_TOKEN_DATE_STATUS;

    String BASE_URL = Url.BASE_URL;

    Dialog dialog1;

    RelativeLayout date_extension;

    TextView taskname;
    TextView requestby;
    TextView cenddate;
    TextView renddate;
    TextView task_name;
    TextView sender_name;
    TextView end_date;
    TextView requested_date;

    Button accept;
    Button decline;

    Typeface bahu_thin;
    Typeface bahu_bold;

    public Date_Extend_adapter(Context context, ArrayList<DateExtendDetails> dateextendlist) {
        this.dateextendlist = dateextendlist;
        this.context = context;

    }

    @Override
    public Date_Extend_adapter.VHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.date_extend_item, viewGroup, false);
        return new Date_Extend_adapter.VHolder(view);
    }


    @Override
    public void onBindViewHolder(final Date_Extend_adapter.VHolder viewHolder, final int i) {

        DateExtendDetails di = dateextendlist.get(i);

        viewHolder.task_complete_name.setText(di.getTask_name());
        viewHolder.req_send_by.setText(di.getRequested_by());

        viewHolder.date_extend_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewExtendDetails(i);
            }
        });


    }

    public void ViewExtendDetails(final Integer i) {

        dialog1 = new Dialog(context);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.edit_date_extend_item);
        dialog1.setCancelable(true);
        dialog1.setCanceledOnTouchOutside(true);

        date_extension = (RelativeLayout) dialog1.findViewById(R.id.date_extension);

        taskname = (TextView) dialog1.findViewById(R.id.taskname);
        requestby = (TextView) dialog1.findViewById(R.id.requestby);
        cenddate = (TextView) dialog1.findViewById(R.id.cenddate);
        renddate = (TextView) dialog1.findViewById(R.id.renddate);

        task_name = (TextView) dialog1.findViewById(R.id.task_name);
        sender_name = (TextView) dialog1.findViewById(R.id.sender_name);
        end_date = (TextView) dialog1.findViewById(R.id.end_date);
        requested_date = (TextView) dialog1.findViewById(R.id.requested_date);

        accept = (Button) dialog1.findViewById(R.id.accept);
        decline = (Button) dialog1.findViewById(R.id.decline);

        taskname.setTypeface(bahu_bold);
        requestby.setTypeface(bahu_bold);
        cenddate.setTypeface(bahu_bold);
        renddate.setTypeface(bahu_bold);

        task_name.setTypeface(bahu_thin);
        sender_name.setTypeface(bahu_thin);
        end_date.setTypeface(bahu_thin);
        requested_date.setTypeface(bahu_thin);

        accept.setTypeface(bahu_thin);
        decline.setTypeface(bahu_thin);

        DateExtendDetails di = dateextendlist.get(i);

        task_name.setText(di.getTask_name());
        sender_name.setText(di.getRequested_by());
        end_date.setText(di.getEnd_date());
        requested_date.setText(di.getRequested_date());

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                extend_status = "1";
                SendDateStatus(i);
            }
        });

        decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                extend_status = "0";
                SendDateStatus(i);
            }
        });

        dialog1.show();
        Window window = dialog1.getWindow();
        window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
    }

    public void SendDateStatus(final Integer i) {
        Snackbar snackbar = Snackbar.make(date_extension, "Please confirm to submit", 3000);

        snackbar.setAction("Confirm", new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                JSON_URL_TOKEN_DATE_STATUS = BASE_URL + "date_extend_status.php?task_id=" + dateextendlist.get(i).getTask_id() + "&status=" + extend_status;


                try {
                    Log.d(TAG, "onClick:   json " + JSON_URL_TOKEN_DATE_STATUS);
                    StringRequest req2 = new StringRequest(JSON_URL_TOKEN_DATE_STATUS,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("MainActivity", response.toString());

                                    try {
                                        if (response.length() > 0) {
                                            if (response.toString().equalsIgnoreCase("1")) {
                                                Toast.makeText(context, "Status submitted", Toast.LENGTH_SHORT).show();
                                                ((DateExtend) context).getData();
                                                dialog1.dismiss();
                                            } else {
                                                Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();

                                            }

                                        } else {
                                            Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();

                                        }
                                    } catch (Exception e) {
                                        Log.d("MainActivity", "onResponse: " + e);
                                        Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();

                                    }
                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("MainActivity", "Server Error: " + error.getMessage());
                            Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();

                        }
                    });

                    RequestQueue requestQueue = Volley.newRequestQueue(context);

                    int socketTimeout = 600000;//30 seconds - change to what you want
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    req2.setRetryPolicy(policy);

                    requestQueue.add(req2);

                } catch (Exception e) {
                    Log.d("MainActivity", "getCustomer: " + e);
                    Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();

                }
            }
        });

        snackbar.show();
    }

    @Override
    public int getItemCount() {

        if (dateextendlist == null)
            return 0;
        else
            return dateextendlist.size();

    }

    public class VHolder extends RecyclerView.ViewHolder {

        RelativeLayout date_extend_layout;
        TextView task_complete_name;
        TextView req_send_by;

        private VHolder(final View itemView) {

            super(itemView);

            date_extend_layout = (RelativeLayout) itemView.findViewById(R.id.date_extend_layout);
            task_complete_name = (TextView) itemView.findViewById(R.id.task_name);
            req_send_by = (TextView) itemView.findViewById(R.id.req_send_by);

            bahu_thin = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_thin.ttf");
            bahu_bold = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_bold.ttf");

            task_complete_name.setTypeface(bahu_bold);
            req_send_by.setTypeface(bahu_thin);

        }
    }

    public interface ClickListener {
        void onClick(int position);

        void onLongClick(int position);
    }

}
