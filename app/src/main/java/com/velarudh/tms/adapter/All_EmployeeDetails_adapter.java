package com.velarudh.tms.adapter;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.velarudh.tms.R;
import com.velarudh.tms.main.Employee_Details;
import com.velarudh.tms.model.DrawerItem;
import com.velarudh.tms.model.EmployeeDetails;
import com.velarudh.tms.project.ManageAllProject;
import com.velarudh.tms.util.Url;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Atrijit on 02-06-2017.
 */

public class All_EmployeeDetails_adapter extends RecyclerView.Adapter<All_EmployeeDetails_adapter.VHolder> {

    private Context context;
    private ArrayList<EmployeeDetails> Employeedetails;
    String TAG = "All_EmployeeDls_adapter";
    ContentResolver cv;

    boolean edit = false;

    String id_value;
    String name_value;
    String email_value;
    String ph_value;
    String designation_value;
    String assign_value;
    String admin_value;

    AlertDialog dialog2 = null;

    String user_id;

    String BASE_URL = Url.BASE_URL;

    Dialog dialog1;

    LinearLayout employee_layout;
    TextView empid;
    TextView empname;
    TextView empcontact;
    TextView empmail;
    TextView empph;
    TextView empdesig;
    TextView empadmin;
    TextView empassign;

    TextView employee_id;
    EditText employee_name;
    EditText employee_email;
    EditText employee_ph;
    EditText employee_designation;

    TextView is_admin;
    TextView can_assign;

    RadioGroup edit_is_admin;
    RadioGroup edit_can_assign;

    RadioButton admin_yes;
    RadioButton admin_no;
    RadioButton assign_yes;
    RadioButton assign_no;

    Button update;
    Button delete;

    public All_EmployeeDetails_adapter(Context context, ArrayList<EmployeeDetails> Employeedetails, String user_id) {
        this.Employeedetails = Employeedetails;
        this.context = context;
        this.user_id = user_id;

    }

    @Override
    public All_EmployeeDetails_adapter.VHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.employee_details_layout, viewGroup, false);
        return new All_EmployeeDetails_adapter.VHolder(view);
    }


    @Override
    public void onBindViewHolder(final All_EmployeeDetails_adapter.VHolder viewHolder, final int i) {

        final EmployeeDetails emp = Employeedetails.get(i);

        name_value = emp.getEmployee_name();
        designation_value = emp.getEmployee_designation();

        viewHolder.emp_name.setText(name_value);
        viewHolder.emp_desig.setText(designation_value);

        viewHolder.employee_details_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditEmployee(i);
            }
        });

    }

    public void EditEmployee(final Integer i) {

        dialog1  = new Dialog(context);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.edit_employee_details);
        dialog1.setCancelable(true);
        dialog1.setCanceledOnTouchOutside(true);

        employee_layout = (LinearLayout) dialog1.findViewById(R.id.employee_layout);
        empid = (TextView) dialog1.findViewById(R.id.empid);
        empname = (TextView) dialog1.findViewById(R.id.empname);
        empcontact = (TextView) dialog1.findViewById(R.id.empcontact);
        empmail = (TextView) dialog1.findViewById(R.id.empmail);
        empph = (TextView) dialog1.findViewById(R.id.empph);
        empdesig = (TextView) dialog1.findViewById(R.id.empdesig);
        empadmin = (TextView) dialog1.findViewById(R.id.empadmin);
        empassign = (TextView) dialog1.findViewById(R.id.empassign);

        employee_id = (TextView) dialog1.findViewById(R.id.employee_id);
        employee_name = (EditText) dialog1.findViewById(R.id.employee_name);
        employee_email = (EditText) dialog1.findViewById(R.id.employee_email);
        employee_ph = (EditText) dialog1.findViewById(R.id.employee_ph);
        employee_designation = (EditText) dialog1.findViewById(R.id.employee_designation);

        is_admin = (TextView) dialog1.findViewById(R.id.is_admin);
        can_assign = (TextView) dialog1.findViewById(R.id.can_assign);

        edit_is_admin = (RadioGroup) dialog1.findViewById(R.id.edit_is_admin);
        edit_can_assign = (RadioGroup) dialog1.findViewById(R.id.edit_can_assign);

        admin_yes = (RadioButton) dialog1.findViewById(R.id.admin_yes);
        admin_no = (RadioButton) dialog1.findViewById(R.id.admin_no);
        assign_yes = (RadioButton) dialog1.findViewById(R.id.assign_yes);
        assign_no = (RadioButton) dialog1.findViewById(R.id.assign_no);

        update = (Button) dialog1.findViewById(R.id.update);
        delete = (Button) dialog1.findViewById(R.id.delete);

        Typeface bahu_thin = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_thin.ttf");
        Typeface bahu_bold = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_bold.ttf");

        empid.setTypeface(bahu_bold);
        empname.setTypeface(bahu_bold);
        empcontact.setTypeface(bahu_bold);
        empmail.setTypeface(bahu_bold);
        empph.setTypeface(bahu_bold);
        empdesig.setTypeface(bahu_bold);
        empadmin.setTypeface(bahu_bold);
        empassign.setTypeface(bahu_bold);

        employee_id.setTypeface(bahu_thin);
        employee_name.setTypeface(bahu_thin);
        employee_email.setTypeface(bahu_thin);
        employee_ph.setTypeface(bahu_thin);
        employee_designation.setTypeface(bahu_thin);
        is_admin.setTypeface(bahu_thin);
        can_assign.setTypeface(bahu_thin);
        admin_yes.setTypeface(bahu_thin);
        admin_no.setTypeface(bahu_thin);
        assign_yes.setTypeface(bahu_thin);
        assign_no.setTypeface(bahu_thin);
        update.setTypeface(bahu_thin);
        delete.setTypeface(bahu_thin);

        final EmployeeDetails emp = Employeedetails.get(i);

        id_value = emp.getEmployee_id();
        name_value = emp.getEmployee_name();
        email_value = emp.getEmployee_email();
        ph_value = emp.getEmployee_contact();
        designation_value = emp.getEmployee_designation();

        if (emp.getEmployee_is_admin().equalsIgnoreCase("1")) {
            admin_value = "YES";
            admin_yes.setChecked(true);
            admin_no.setChecked(false);
        } else {
            admin_value = "NO";
            admin_yes.setChecked(false);
            admin_no.setChecked(true);
        }

        if (emp.getEmployee_can_assign().equalsIgnoreCase("1")) {
            assign_value = "YES";
            assign_yes.setChecked(true);
            assign_no.setChecked(false);
        } else {
            assign_value = "NO";
            assign_yes.setChecked(false);
            assign_no.setChecked(true);
        }

        employee_id.setText(id_value);
        employee_name.setText(name_value);
        employee_email.setText(email_value);
        employee_ph.setText(ph_value);
        employee_designation.setText(designation_value);
        is_admin.setText(admin_value);
        can_assign.setText(assign_value);


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!edit) {
                    edit = true;

                    update.setText("Submit");
                    delete.setText("Cancel");

                    employee_name.setEnabled(true);
                    employee_email.setEnabled(true);
                    employee_ph.setEnabled(true);
                    employee_designation.setEnabled(true);

                    is_admin.setVisibility(View.GONE);
                    can_assign.setVisibility(View.GONE);
                    edit_can_assign.setVisibility(View.VISIBLE);
                    edit_is_admin.setVisibility(View.VISIBLE);


                } else {

                    if (Validation()) {
                        name_value = MakeSubTextToParse(name_value);
                        email_value = MakeSubTextToParse(email_value);
                        ph_value = MakeSubTextToParse(ph_value);
                        designation_value = MakeSubTextToParse(designation_value);

                        SendUpdatedData(i);
                    }

                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edit) {
                    edit = false;

                    update.setText("Update");
                    delete.setText("Delete");

                    employee_name.setEnabled(false);
                    employee_email.setEnabled(false);
                    employee_ph.setEnabled(false);
                    employee_designation.setEnabled(false);

                    is_admin.setVisibility(View.VISIBLE);
                    can_assign.setVisibility(View.VISIBLE);
                    edit_can_assign.setVisibility(View.GONE);
                    edit_is_admin.setVisibility(View.GONE);
                } else {
                    DeleteEmployee(i);
                }
            }
        });

        dialog1.show();
        Window window = dialog1.getWindow();
        window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
    }

    public void DeleteEmployee(final Integer i) {

        Snackbar snackbar = Snackbar.make(employee_layout, "Please confirm to delete", 3000);

        snackbar.setAction("Confirm", new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String JSON_URL_TOKEN_DELETE_EMPLOYEE = BASE_URL + "delete_employee.php?emp_id=" + Employeedetails.get(i).getEmployee_id();


                try {
                    Log.d(TAG, "onClick:   json " + JSON_URL_TOKEN_DELETE_EMPLOYEE);
                    StringRequest req2 = new StringRequest(JSON_URL_TOKEN_DELETE_EMPLOYEE,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("MainActivity", response.toString());

                                    try {
                                        if (response.length() > 0) {
                                            if (response.toString().equalsIgnoreCase("1")) {
                                                Toast.makeText(context, "Employee has been removed successfully", Toast.LENGTH_SHORT).show();
                                                Employeedetails.remove(i);
                                                dialog1.dismiss();
                                                notifyItemRemoved(i);

                                            } else {
                                                Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();

                                            }

                                        } else {
                                            Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();

                                        }
                                    } catch (Exception e) {
                                        Log.d("MainActivity", "onResponse: " + e);
                                        Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();

                                    }
                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("MainActivity", "Server Error: " + error.getMessage());
                            Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();


                        }
                    });

                    RequestQueue requestQueue = Volley.newRequestQueue(context);

                    int socketTimeout = 600000;//30 seconds - change to what you want
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    req2.setRetryPolicy(policy);

                    requestQueue.add(req2);

                } catch (Exception e) {
                    Log.d("MainActivity", "getCustomer: " + e);
                    Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();

                }
            }
        });

        snackbar.show();
    }

    //
    public boolean Validation() {
        boolean valid = true;

        name_value = employee_name.getText().toString().trim();
        email_value = employee_email.getText().toString().trim();
        ph_value = employee_ph.getText().toString().trim();
        designation_value = employee_designation.getText().toString().trim();

        if (admin_yes.isChecked()) {
            admin_value = "1";
        } else if (admin_no.isChecked()) {
            admin_value = "0";
        }

        if (assign_yes.isChecked()) {
            assign_value = "1";
        } else if (assign_no.isChecked()) {
            assign_value = "0";
        }

        if (name_value.isEmpty()) {
            valid = false;
            employee_name.setError("Name cant be empty");
        } else {
            employee_name.setError(null);
        }
        if (email_value.isEmpty()) {
            valid = false;
            employee_email.setError("Mail id cant be empty");
        } else {
            employee_email.setError(null);
        }
        if (ph_value.isEmpty()) {
            valid = false;
            employee_ph.setError("phone number cant be empty");
        } else {
            employee_ph.setError(null);
        }
        if (designation_value.isEmpty()) {
            valid = false;
            employee_designation.setError("Designation cant be empty");
        } else {
            employee_designation.setError(null);
        }

        return valid;
    }

    //
    public void SendUpdatedData(final Integer j) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setCancelable(false);
        dialog.setTitle("Please confirm to submit");

        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                String JSON_URL_TOKEN_EDIT_EMPLOYEE = BASE_URL + "edit_employee.php?user_id=" + user_id + "&employee_id=" + Employeedetails.get(j).getEmployee_id() + "&employee_name=" + name_value + "&employee_email=" + email_value + "&employee_ph=" + ph_value + "&designation=" + designation_value + "&is_admin=" + admin_value + "&can_assign=" + assign_value;

//                String JSON_URL_TOKEN_EDIT_EMPLOYEE = "https://tms.velarudh.com/test/json/edit_employee.php?user_id=" + user_id + "&employee_id=" + Employeedetails.get(j).getEmployee_id() + "&employee_name=" + name_value + "&employee_email=" + email_value + "&employee_ph=" + ph_value + "&designation=" + designation_value +"&is_admin=" + admin_value;

                try {
                    Log.d(TAG, "onClick:   json " + JSON_URL_TOKEN_EDIT_EMPLOYEE);
                    StringRequest req2 = new StringRequest(JSON_URL_TOKEN_EDIT_EMPLOYEE,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("ConfirmationAlert", response.toString());

                                    try {
                                        if (response.length() > 0) {
                                            if (response.toString().equalsIgnoreCase("1")) {
                                                Toast.makeText(context, "Employee details has been updated successfully", Toast.LENGTH_SHORT).show();

                                                edit = false;

                                                update.setText("Update");
                                                delete.setText("Delete");

                                                employee_name.setEnabled(false);
                                                employee_email.setEnabled(false);
                                                employee_ph.setEnabled(false);
                                                employee_designation.setEnabled(false);

                                                is_admin.setVisibility(View.VISIBLE);
                                                can_assign.setVisibility(View.VISIBLE);
                                                edit_can_assign.setVisibility(View.GONE);
                                                edit_is_admin.setVisibility(View.GONE);

                                                dialog1.dismiss();
                                                dialog2.dismiss();

                                                ((Employee_Details) context).ReloadData();
                                                notifyDataSetChanged();
                                            }

                                        } else {
                                            Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();

                                            edit = false;

                                            update.setText("Update");
                                            delete.setText("Delete");

                                            employee_name.setEnabled(false);
                                            employee_email.setEnabled(false);
                                            employee_ph.setEnabled(false);
                                            employee_designation.setEnabled(false);

                                            is_admin.setVisibility(View.VISIBLE);
                                            can_assign.setVisibility(View.VISIBLE);
                                            edit_can_assign.setVisibility(View.GONE);
                                            edit_is_admin.setVisibility(View.GONE);

                                            dialog1.dismiss();
                                            dialog2.dismiss();
                                        }
                                    } catch (Exception e) {
                                        Log.d("ConfirmationAlert", "onResponse: " + e);
                                        Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                        edit = false;

                                        update.setText("Update");
                                        delete.setText("Delete");

                                        employee_name.setEnabled(false);
                                        employee_email.setEnabled(false);
                                        employee_ph.setEnabled(false);
                                        employee_designation.setEnabled(false);

                                        is_admin.setVisibility(View.VISIBLE);
                                        can_assign.setVisibility(View.VISIBLE);
                                        edit_can_assign.setVisibility(View.GONE);
                                        edit_is_admin.setVisibility(View.GONE);

                                        dialog2.dismiss();
                                    }
                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("ConfirmationAlert", "Server Error: " + error.getMessage());
                            Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                            edit = false;

                            update.setText("Update");
                            delete.setText("Delete");

                            employee_name.setEnabled(false);
                            employee_email.setEnabled(false);
                            employee_ph.setEnabled(false);
                            employee_designation.setEnabled(false);

                            is_admin.setVisibility(View.VISIBLE);
                            can_assign.setVisibility(View.VISIBLE);
                            edit_can_assign.setVisibility(View.GONE);
                            edit_is_admin.setVisibility(View.GONE);

                            dialog2.dismiss();
                        }
                    });

                    RequestQueue requestQueue = Volley.newRequestQueue(context);

                    int socketTimeout = 600000;//30 seconds - change to what you want
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    req2.setRetryPolicy(policy);

                    requestQueue.add(req2);

                } catch (Exception e) {
                    Log.d("ConfirmationAlert", "getCustomer: " + e);
                    Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                    edit = false;

                    update.setText("Update");
                    delete.setText("Delete");

                    employee_name.setEnabled(false);
                    employee_email.setEnabled(false);
                    employee_ph.setEnabled(false);
                    employee_designation.setEnabled(false);

                    is_admin.setVisibility(View.VISIBLE);
                    can_assign.setVisibility(View.VISIBLE);
                    edit_can_assign.setVisibility(View.GONE);
                    edit_is_admin.setVisibility(View.GONE);

                    dialog2.dismiss();
                }
            }
        });

        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialog2.dismiss();
            }
        });


        dialog2 = dialog.show();
    }

    @Override
    public int getItemCount() {

        if (Employeedetails == null)
            return 0;
        else
            return Employeedetails.size();

    }

    public class VHolder extends RecyclerView.ViewHolder {

        RelativeLayout employee_details_layout;

        TextView emp_name;
        TextView emp_desig;


        private VHolder(final View itemView) {

            super(itemView);

            employee_details_layout = (RelativeLayout) itemView.findViewById(R.id.employee_details_layout);

            emp_name = (TextView) itemView.findViewById(R.id.emp_name);
            emp_desig = (TextView) itemView.findViewById(R.id.emp_desig);

            Typeface bahu_thin = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_thin.ttf");
            Typeface bahu_bold = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_bold.ttf");

            emp_name.setTypeface(bahu_bold);
            emp_desig.setTypeface(bahu_thin);

        }
    }

    public String MakeSubTextToParse(String name) {

        String[] name1 = name.split("\\s");

        name = "";
        for (String n2 : name1) {
            Log.d("n2", "" + n2);
            name += n2 + "%20";
        }
        return name;

    }

}
