package com.velarudh.tms.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.unnamed.b.atv.model.TreeNode;
import com.velarudh.tms.R;


/**
 * Created by Bogdan Melnychuk on 2/12/15.
 */
public class ProjectandParentTaskItemHolder extends TreeNode.BaseNodeViewHolder<ProjectandParentTaskItemHolder.IconTreeItem> {
    private TextView tvValue;
    private ImageView arrowView;

    public ProjectandParentTaskItemHolder(Context context) {
        super(context);
    }

    @Override
    public View createNodeView(final TreeNode node, IconTreeItem value) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_project_parent_node, null, false);
        tvValue = (TextView) view.findViewById(R.id.node_value);
        tvValue.setText(value.text + " ("+value.count+")");


        arrowView = (ImageView) view.findViewById(R.id.arrow_icon);


        return view;
    }

    @Override
    public void toggle(boolean active) {
        arrowView.setBackground(context.getResources().getDrawable(active ? R.drawable.ic_arrow_down : R.drawable.ic_arrow_down));
    }

    public static class IconTreeItem {
        public int icon;
        public int count;
        public String text;

        public IconTreeItem(String text , int count) {
//            this.icon = icon;
            this.text = text;
            this.count = count;
        }
    }
}
