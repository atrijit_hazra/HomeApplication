package com.velarudh.tms.adapter;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.velarudh.tms.R;
import com.velarudh.tms.task.NewTask_SubTask;
import com.velarudh.tms.task.Task_details;
import com.velarudh.tms.model.TaskDetails;
import com.velarudh.tms.util.CountDownTimerView;
import com.velarudh.tms.util.ExpandableTextViewTwo;
import com.velarudh.tms.util.Url;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * Created by Atrijit on 22-05-2017.
 */

public class Dashboard_task_project_adapter extends RecyclerView.Adapter<Dashboard_task_project_adapter.VHolder> {

    private Context context;
    private ArrayList<TaskDetails> task_details;
    String TAG = "Das_task_proj_adapter";

    String user_can_assign;
    private SparseBooleanArray mSelectedItems = new SparseBooleanArray();
    Activity activity;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
    Calendar cal = Calendar.getInstance();

    public Dashboard_task_project_adapter(Context context, ArrayList<TaskDetails> task_details, String user_can_assign) {
        this.task_details = task_details;
        this.context = context;
        activity = (Activity) context;
        this.user_can_assign = user_can_assign;
    }

    @Override
    public Dashboard_task_project_adapter.VHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dashboard_tsk_project_item, viewGroup, false);
        return new Dashboard_task_project_adapter.VHolder(view);
    }


    @Override
    public void onBindViewHolder(final Dashboard_task_project_adapter.VHolder viewHolder, final int i) {

        Log.d(TAG, "onBindViewHolder: " + task_details.get(i).getTask_assign_by_id());

        viewHolder.task_name.setText(task_details.get(i).getTask_name());
        viewHolder.project_name.setText(task_details.get(i).getProject_name());

        try {
            Date task_end = simpleDateFormat.parse(task_details.get(i).getTask_end_date());
            Date current = simpleDateFormat.parse(simpleDateFormat.format(cal.getTime()));

            Log.d(TAG, "onBindViewHolder: + date "+task_end+"  current "+current);

            if (current.after(task_end)) {
                viewHolder.assign_task_others.setVisibility(View.GONE);
            } else {
                viewHolder.assign_task_others.setVisibility(View.VISIBLE);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        viewHolder.view_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<TaskDetails> select_task = new ArrayList<TaskDetails>();
                select_task.add(task_details.get(i));

                Intent in = new Intent(context, Task_details.class);
                in.putParcelableArrayListExtra("select_task", select_task);
                context.startActivity(in);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            }
        });

        viewHolder.assign_task_others.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<TaskDetails> select_task = new ArrayList<TaskDetails>();
                select_task.add(task_details.get(i));

                Intent in = new Intent(context, NewTask_SubTask.class);
                in.putExtra("sub_task", "yes");
                in.putParcelableArrayListExtra("select_task", select_task);
                context.startActivity(in);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

            }
        });

    }

    @Override
    public int getItemCount() {

        if (task_details == null)
            return 0;
        else if (task_details.size() > 4)
            return 4;
        else
            return task_details.size();

    }

    public class VHolder extends RecyclerView.ViewHolder {

        RelativeLayout dashboard_layout;
        ExpandableTextViewTwo task_name;
        TextView project_name;

        ImageButton view_task;
        ImageButton assign_task_others;

        private VHolder(final View itemView) {

            super(itemView);

            dashboard_layout = (RelativeLayout) itemView.findViewById(R.id.dashboard_layout);

            task_name = (ExpandableTextViewTwo) itemView.findViewById(R.id.task_name);
            project_name = (TextView) itemView.findViewById(R.id.project_name);

            view_task = (ImageButton) itemView.findViewById(R.id.view_task);
            assign_task_others = (ImageButton) itemView.findViewById(R.id.assign_task_others);

            Typeface bahu_thin = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_thin.ttf");
            Typeface bahu_bold = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_bold.ttf");

            task_name.setTypeface(bahu_thin);
            project_name.setTypeface(bahu_bold);

            if (user_can_assign.equalsIgnoreCase("no"))
                assign_task_others.setVisibility(View.GONE);
            else
                assign_task_others.setVisibility(View.VISIBLE);

        }
    }

    public interface ClickListener {
        void onClick(int position);

        void onLongClick(int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private Dashboard_task_project_adapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final Dashboard_task_project_adapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }


    }

    public void setSelectedState(int position) {

        clearSelectedState();

        mSelectedItems.put(position, true);

        notifyItemChanged(position);
    }

    public void clearSelectedState() {
        List<Integer> selection = getSelectedItems();
        mSelectedItems.clear();
        for (Integer i : selection) {
            notifyItemChanged(i);
        }
    }

    public boolean isSelected(int position) {
        return getSelectedItems().contains(position);
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<>(mSelectedItems.size());
        for (int i = 0; i < mSelectedItems.size(); ++i) {
            items.add(mSelectedItems.keyAt(i));
        }
        return items;
    }

}
