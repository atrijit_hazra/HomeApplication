package com.velarudh.tms.adapter;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.velarudh.tms.R;
import com.velarudh.tms.main.Employee_Details;
import com.velarudh.tms.model.EmployeeDetails;
import com.velarudh.tms.util.Url;

import java.util.ArrayList;

/**
 * Created by Atrijit on 02-06-2017.
 */

public class Online_Users_adapter extends RecyclerView.Adapter<Online_Users_adapter.VHolder> {

    private Context context;
    private ArrayList<EmployeeDetails> Employeedetails;
    String TAG = "All_EmployeeDls_adapter";
    ContentResolver cv;

    boolean edit = false;

    String id_value;
    String name_value;
    String email_value;
    String ph_value;
    String designation_value;
    String assign_value;
    String admin_value;

    AlertDialog dialog2 = null;

    String user_id;

    String BASE_URL = Url.BASE_URL;


    public Online_Users_adapter(Context context, ArrayList<EmployeeDetails> Employeedetails, String user_id) {
        this.Employeedetails = Employeedetails;
        this.context = context;
        this.user_id = user_id;

    }

    @Override
    public Online_Users_adapter.VHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.online_users_details_layout, viewGroup, false);
        return new Online_Users_adapter.VHolder(view);
    }


    @Override
    public void onBindViewHolder(final Online_Users_adapter.VHolder viewHolder, final int i) {

        final EmployeeDetails emp = Employeedetails.get(i);

        id_value = emp.getEmployee_id();
        name_value = emp.getEmployee_name();

        viewHolder.user_name.setText(name_value);
        viewHolder.user_ic_active.setText("Online");
        viewHolder.user_ic_active.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_online, 0, 0, 0);
        viewHolder.user_message_count.setText("10");
    }


    @Override
    public int getItemCount() {

        if (Employeedetails == null)
            return 0;
        else
            return Employeedetails.size();

    }

    public class VHolder extends RecyclerView.ViewHolder {

        LinearLayout online_user_layout;

        TextView user_name;
        TextView user_ic_active;
        Button user_message_count;


        private VHolder(final View itemView) {

            super(itemView);

            online_user_layout = (LinearLayout) itemView.findViewById(R.id.online_user_layout);

            user_name = (TextView) itemView.findViewById(R.id.user_name);
            user_ic_active = (TextView) itemView.findViewById(R.id.user_ic_active);
            user_message_count = (Button) itemView.findViewById(R.id.user_message_count);

            Typeface bahu_thin = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_thin.ttf");
            Typeface bahu_bold = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_bold.ttf");


            user_name.setTypeface(bahu_thin);
            user_ic_active.setTypeface(bahu_thin);
            user_message_count.setTypeface(bahu_thin);

        }
    }

    public String MakeSubTextToParse(String name){

        String[] name1 = name.split("\\s");

        name = "";
        for (String n2 : name1) {
            Log.d("n2", "" + n2);
            name += n2 + "%20";
        }
        return name;

    }

    public interface ClickListener {
        void onClick(int position);

        void onLongClick(int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private Online_Users_adapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final Online_Users_adapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }


    }

}
