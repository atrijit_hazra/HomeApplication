package com.velarudh.tms.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.velarudh.tms.R;
import com.velarudh.tms.main.CreateEmployee;
import com.velarudh.tms.task.NewTask_SubTask;
import com.velarudh.tms.task.Task_details;
import com.velarudh.tms.model.TaskDetails;
import com.velarudh.tms.util.CountDownTimerView;
import com.velarudh.tms.util.ExpandableTextViewTwo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Atrijit on 22-05-2017.
 */

public class Dashboard_Alltask_adapter extends RecyclerView.Adapter<Dashboard_Alltask_adapter.VHolder> {

    private Context context;
    private ArrayList<TaskDetails> task_details;
    String TAG = "Dashboard_Alltask_adapter";
    private List<TaskDetails> searchList;
    String user_can_assign;
    Activity activity ;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
    Calendar cal = Calendar.getInstance();

    public Dashboard_Alltask_adapter(Context context, ArrayList<TaskDetails> task_details , String user_can_assign) {

        this.task_details = task_details;
        this.context = context;
        this.user_can_assign = user_can_assign;
        searchList = new ArrayList<>();

        if (task_details != null)
            searchList.addAll(task_details);

        activity = (Activity) context;

    }

    @Override
    public Dashboard_Alltask_adapter.VHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dashboard_tsk_project_item, viewGroup, false);
        return new Dashboard_Alltask_adapter.VHolder(view);
    }


    @Override
    public void onBindViewHolder(final Dashboard_Alltask_adapter.VHolder viewHolder, final int i) {

        viewHolder.task_name.setText(task_details.get(i).getTask_name());
        viewHolder.project_name.setText(task_details.get(i).getProject_name());

        try {
            Date task_end = simpleDateFormat.parse(task_details.get(i).getTask_end_date());
            Date current = simpleDateFormat.parse(simpleDateFormat.format(cal.getTime()));

            Log.d(TAG, "onBindViewHolder: + date "+task_end+"  current "+current);

            if (current.after(task_end)) {
                viewHolder.assign_task_others.setVisibility(View.GONE);
            } else {
                viewHolder.assign_task_others.setVisibility(View.VISIBLE);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        viewHolder.view_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<TaskDetails> select_task = new ArrayList<TaskDetails>();
                select_task.add(task_details.get(i));

                Intent in = new Intent(context,Task_details.class);
                in.putParcelableArrayListExtra("select_task",select_task);
                context.startActivity(in);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);

            }
        });

        viewHolder.assign_task_others.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<TaskDetails> select_task = new ArrayList<TaskDetails>();
                select_task.add(task_details.get(i));

                Intent in = new Intent(context,NewTask_SubTask.class);
                in.putExtra("sub_task","yes");
                in.putParcelableArrayListExtra("select_task",select_task);
                context.startActivity(in);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
            }
        });

    }

    @Override
    public int getItemCount() {

        if (task_details == null)
            return 0;
        else
            return task_details.size();

    }

    public class VHolder extends RecyclerView.ViewHolder {

        RelativeLayout dashboard_layout;
        ExpandableTextViewTwo task_name;
        TextView project_name;

        ImageButton view_task;
        ImageButton assign_task_others;

        private VHolder(final View itemView) {

            super(itemView);

            dashboard_layout = (RelativeLayout) itemView.findViewById(R.id.dashboard_layout);

            task_name = (ExpandableTextViewTwo) itemView.findViewById(R.id.task_name);
            project_name = (TextView) itemView.findViewById(R.id.project_name);

            view_task = (ImageButton) itemView.findViewById(R.id.view_task);
            assign_task_others = (ImageButton) itemView.findViewById(R.id.assign_task_others);
            Typeface bahu_thin = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_thin.ttf");
            Typeface bahu_bold = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_bold.ttf");

            task_name.setTypeface(bahu_thin);
            project_name.setTypeface(bahu_bold);

            if(user_can_assign.equalsIgnoreCase("no"))
                assign_task_others.setVisibility(View.GONE);

        }
    }

    public void filter(String charText) {

        try {
            Log.d("enter adapter filter", "" + charText);
            charText = charText.toLowerCase(Locale.getDefault());
            Log.d("enter adapter filter1", "" + charText);
            task_details.clear();
            if (charText.length() == 0) {
                task_details.addAll(searchList);
            } else {
                for (TaskDetails s : searchList) {
                    Log.d("value of s", "" + s);
                    if (s.getTask_name().toLowerCase(Locale.getDefault()).contains(charText)) {
                        task_details.add(s);
                        Log.d("value of friendList", "" + task_details);
                    }
                }
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
