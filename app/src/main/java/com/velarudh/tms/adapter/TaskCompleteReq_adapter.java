package com.velarudh.tms.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.velarudh.tms.R;
import com.velarudh.tms.model.TaskCompleted;
import com.velarudh.tms.model.TaskTypeStatusList;
import com.velarudh.tms.task.Task_Completed;
import com.velarudh.tms.util.CountDownTimerView;
import com.velarudh.tms.util.ExpandableTextView;
import com.velarudh.tms.util.ExpandableTextViewTwo;
import com.velarudh.tms.util.Url;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Atrijit on 07-06-2017.
 */

public class TaskCompleteReq_adapter extends RecyclerView.Adapter<TaskCompleteReq_adapter.VHolder> {

    private Context context;
    private ArrayList<TaskCompleted> taskCompleteds;
    String TAG = "TaskCompleteReq_adapter";
    boolean manage_edit = false;

    String JSON_URL_TOKEN_FINAL_TASK_SUBMIT;

    AlertDialog dialog2 = null;

    String user_id;
    String complete_value = "";
    String hour_taken_value = "0";
    String task_complexity_value;
    String task_quality_value;
    Dialog dialog;
    String BASE_URL = Url.BASE_URL;
    SimpleDateFormat myFormat = new SimpleDateFormat("yyyy/MM/dd");
    String Curr_date;

    Dialog dialog1;

    LinearLayout sub_task_layout;

    ExpandableTextViewTwo details_project_name;

    TextView subtask;
    TextView project;
    TextView taskassign;
    TextView taskcreate;
    TextView taskstart;
    TextView taskend;
    TextView taskremain;
    TextView taskprior;
    TextView tasktype;
    TextView taskdetail;
    TextView d_task_option;
    TextView hours;
    TextView task_complex;
    TextView task_quality;

    TextView details_task_name;
    TextView details_sub_task_name;
    TextView details_task_end_date;
    TextView details_task_assignd_to;
    TextView details_task_created_date;
    TextView details_task_start_date;
    TextView details_task_priority;
    TextView details_task_type;
    CountDownTimerView details_task_remain_time;
    ExpandableTextView details_task_desc;

    LinearLayout radio_layout;
    RadioGroup is_complete;
    RadioButton complete_yes;
    RadioButton complete_no;

    LinearLayout layout_complete;
    EditText hours_taken;
    Spinner select_task_complexity;
    Spinner select_task_quality;

    Button manage;
    Button cancel;

    Typeface bahu_thin;
    Typeface bahu_bold;

    public TaskCompleteReq_adapter(Context context, ArrayList<TaskCompleted> taskCompleteds, String user_id) {
        this.taskCompleteds = taskCompleteds;
        this.context = context;
        this.user_id = user_id;
        Curr_date = myFormat.format(Calendar.getInstance().getTime());
    }

    @Override
    public TaskCompleteReq_adapter.VHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.task_complete_req_details_layout, viewGroup, false);
        return new TaskCompleteReq_adapter.VHolder(view);
    }


    @Override
    public void onBindViewHolder(final TaskCompleteReq_adapter.VHolder viewHolder, final int i) {

        viewHolder.task_name.setText(taskCompleteds.get(i).getTask_name());
        viewHolder.task_assign_to.setText(taskCompleteds.get(i).getTask_assign_to());

        viewHolder.task_complete_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewCompleteReqDetails(i);
            }
        });
    }

    public void ViewCompleteReqDetails(final Integer i) {

        dialog1 = new Dialog(context);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.edit_task_complete_req_details_layout);
        dialog1.setCancelable(true);
        dialog1.setCanceledOnTouchOutside(true);

        sub_task_layout = (LinearLayout) dialog1.findViewById(R.id.sub_task_layout);

        subtask = (TextView) dialog1.findViewById(R.id.subtask);
        project = (TextView) dialog1.findViewById(R.id.project);
        taskassign = (TextView) dialog1.findViewById(R.id.taskassign);
        taskcreate = (TextView) dialog1.findViewById(R.id.taskcreate);
        taskstart = (TextView) dialog1.findViewById(R.id.taskstart);
        taskend = (TextView) dialog1.findViewById(R.id.taskend);
        taskremain = (TextView) dialog1.findViewById(R.id.taskremain);
        taskprior = (TextView) dialog1.findViewById(R.id.taskprior);
        tasktype = (TextView) dialog1.findViewById(R.id.tasktype);
        taskdetail = (TextView) dialog1.findViewById(R.id.taskdetail);
        d_task_option = (TextView) dialog1.findViewById(R.id.d_task_option);
        hours = (TextView) dialog1.findViewById(R.id.hours);
        task_complex = (TextView) dialog1.findViewById(R.id.task_complex);
        task_quality = (TextView) dialog1.findViewById(R.id.task_quality);

        details_project_name = (ExpandableTextViewTwo) dialog1.findViewById(R.id.details_project_name);
        details_task_name = (TextView) dialog1.findViewById(R.id.details_task_name);
        details_sub_task_name = (TextView) dialog1.findViewById(R.id.details_sub_task_name);
        details_task_end_date = (TextView) dialog1.findViewById(R.id.details_task_end_date);
        details_task_assignd_to = (TextView) dialog1.findViewById(R.id.details_task_assignd_to);
        details_task_created_date = (TextView) dialog1.findViewById(R.id.details_task_created_date);
        details_task_start_date = (TextView) dialog1.findViewById(R.id.details_task_start_date);
        details_task_priority = (TextView) dialog1.findViewById(R.id.details_task_priority);
        details_task_type = (TextView) dialog1.findViewById(R.id.details_task_type);
        details_task_desc = (ExpandableTextView) dialog1.findViewById(R.id.details_task_desc);
        details_task_remain_time = (CountDownTimerView) dialog1.findViewById(R.id.details_task_remain_time);

        radio_layout = (LinearLayout) dialog1.findViewById(R.id.radio_layout);
        is_complete = (RadioGroup) dialog1.findViewById(R.id.is_complete);
        complete_yes = (RadioButton) dialog1.findViewById(R.id.complete_yes);
        complete_no = (RadioButton) dialog1.findViewById(R.id.complete_no);

        layout_complete = (LinearLayout) dialog1.findViewById(R.id.layout_complete);
        hours_taken = (EditText) dialog1.findViewById(R.id.hours_taken);
        select_task_complexity = (Spinner) dialog1.findViewById(R.id.select_task_complexity);
        select_task_quality = (Spinner) dialog1.findViewById(R.id.select_task_quality);

        manage = (Button) dialog1.findViewById(R.id.manage);
        cancel = (Button) dialog1.findViewById(R.id.cancel);

        subtask.setTypeface(bahu_bold);
        project.setTypeface(bahu_bold);
        taskassign.setTypeface(bahu_bold);
        taskcreate.setTypeface(bahu_bold);
        taskstart.setTypeface(bahu_bold);
        taskend.setTypeface(bahu_bold);
        taskremain.setTypeface(bahu_bold);
        taskprior.setTypeface(bahu_bold);
        tasktype.setTypeface(bahu_bold);
        taskdetail.setTypeface(bahu_bold);
        d_task_option.setTypeface(bahu_bold);
        hours.setTypeface(bahu_bold);
        task_complex.setTypeface(bahu_bold);
        task_quality.setTypeface(bahu_bold);

        details_project_name.setTypeface(bahu_thin);
        details_task_name.setTypeface(bahu_bold);
        details_sub_task_name.setTypeface(bahu_thin);
        details_task_end_date.setTypeface(bahu_thin);
        details_task_assignd_to.setTypeface(bahu_thin);
        details_task_created_date.setTypeface(bahu_thin);
        details_task_start_date.setTypeface(bahu_thin);
        details_task_priority.setTypeface(bahu_thin);
        details_task_type.setTypeface(bahu_thin);
        details_task_desc.setTypeface(bahu_thin);
        details_task_remain_time.setTypeface(bahu_thin);

        complete_yes.setTypeface(bahu_thin);
        complete_no.setTypeface(bahu_thin);
        hours_taken.setTypeface(bahu_thin);
        manage.setTypeface(bahu_thin);
        cancel.setTypeface(bahu_thin);

        ArrayList<Integer> SpinnerValue = new ArrayList<>();
        for (int j = 1; j < 11; j++) {
            SpinnerValue.add(j);
        }
        final int color = 0xff2d7da6;

        ArrayAdapter<Integer> adapter_complex = new ArrayAdapter<Integer>(context, R.layout.spinner_item, SpinnerValue) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }
        };
        adapter_complex.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        select_task_complexity.setAdapter(adapter_complex);
        select_task_complexity.getBackground().setColorFilter(context.getResources().getColor(R.color.ItemColor), PorterDuff.Mode.SRC_ATOP);

        ArrayAdapter<Integer> adapter_quality = new ArrayAdapter<Integer>(context, R.layout.spinner_item, SpinnerValue) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }
        };
        adapter_quality.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        select_task_quality.setAdapter(adapter_quality);
        select_task_quality.getBackground().setColorFilter(context.getResources().getColor(R.color.ItemColor), PorterDuff.Mode.SRC_ATOP);

        details_project_name.setText(taskCompleteds.get(i).getProject_name());

        if (taskCompleteds.get(i).getTask_parent_id().equalsIgnoreCase("0")) {
            sub_task_layout.setVisibility(View.GONE);
        } else {
            sub_task_layout.setVisibility(View.VISIBLE);
            details_sub_task_name.setText(taskCompleteds.get(i).getSub_task_name());
        }

        String Start_date = taskCompleteds.get(i).getTask_start_date();
        Date StartDate = null;
        Date CurrentDate = null;
        try {
            StartDate = myFormat.parse(Start_date);
            CurrentDate = myFormat.parse(Curr_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "ViewCompleteReqDetails: create date " + taskCompleteds.get(i).getTask_create_date());

        details_task_name.setText(taskCompleteds.get(i).getTask_name());
        details_task_start_date.setText(taskCompleteds.get(i).getTask_start_date());
        details_task_end_date.setText(taskCompleteds.get(i).getTask_end_date());
        details_task_assignd_to.setText(taskCompleteds.get(i).getTask_assign_to());
        details_task_created_date.setText(OnlyDate(taskCompleteds.get(i).getTask_create_date()));
        if (!taskCompleteds.get(i).getTask_description().isEmpty())
            details_task_desc.setText(html2text(taskCompleteds.get(i).getTask_description()));
        else
            details_task_desc.setText("no details available");
        details_task_priority.setText(taskCompleteds.get(i).getTask_priority());
        details_task_type.setText(taskCompleteds.get(i).getTask_type());

        if (CurrentDate.before(StartDate)) {
            Log.d(TAG, "setData: time date " + StartDate + "  curr  " + CurrentDate);
            taskremain.setText("will start in");
            long diff = StartDate.getTime() - CurrentDate.getTime();
            Log.d(TAG, "setData: time diff " + diff);
            details_task_remain_time.setTime(diff);
        } else {
            details_task_remain_time.setTime(diffInMilliSeconds(taskCompleteds.get(i).getTask_end_date()));
        }
        details_task_remain_time.startCountDown();
        details_task_remain_time.setOnTimerListener(new CountDownTimerView.TimerListener() {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                details_task_remain_time.setText("Time up!");
            }
        });

        hour_taken_value = taskCompleteds.get(i).getCompleted_hour();
        hours_taken.setText(hour_taken_value);

        manage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!manage_edit) {
                    manage_edit = true;
                    manage.setText("Submit");
                    cancel.setVisibility(View.VISIBLE);
                    radio_layout.setVisibility(View.VISIBLE);
                    is_complete.setVisibility(View.VISIBLE);
                } else {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(sub_task_layout.getWindowToken(), 0);
                    if (Validation())
                        SendFinalStatus(i);
                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                manage_edit = false;
                manage.setText("Manage");
                complete_yes.setChecked(false);
                complete_no.setChecked(false);

                cancel.setVisibility(View.GONE);
                radio_layout.setVisibility(View.GONE);
                is_complete.setVisibility(View.GONE);
                layout_complete.setVisibility(View.GONE);


            }
        });

        complete_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (complete_yes.isChecked()) {

                    complete_yes.setChecked(true);
                    complete_no.setChecked(false);
                    complete_value = "Completed";
                    layout_complete.setVisibility(View.VISIBLE);

                }
            }
        });

        complete_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (complete_no.isChecked()) {
                    complete_yes.setChecked(false);
                    complete_no.setChecked(true);
                    layout_complete.setVisibility(View.GONE);
                    StatusSubmitAlert(i);
                }
            }
        });

        dialog1.show();
        Window window = dialog1.getWindow();
        window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
    }

    long diffInMilliSeconds(String end_date) {

        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);

        long diff = 0;
        Date end = null;
        Calendar cal = Calendar.getInstance();
        long curr_date = cal.getTimeInMillis();

        try {
            end = myFormat.parse(end_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long finish_date = end.getTime();

        diff = (finish_date - curr_date) + TimeUnit.DAYS.toMillis(1);

        return diff;
    }

    public static String html2text(String html) {
        return Html.fromHtml(html).toString();
    }

    public String OnlyDate(String date) {

        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss a", Locale.ENGLISH);

        Date date1 = null;
        try {
            date1 = myFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy/MM/dd");

        return newFormat.format(date1);

    }

    public void StatusSubmitAlert(final int i) {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.status_dialog_layout);

        final RadioButton pending = (RadioButton) dialog.findViewById(R.id.pending);

        final RadioButton going_on = (RadioButton) dialog.findViewById(R.id.going_on);

        final RadioButton stand_by = (RadioButton) dialog.findViewById(R.id.stand_by);

        final RadioButton new_open = (RadioButton) dialog.findViewById(R.id.new_open);

        final RadioButton completed = (RadioButton) dialog.findViewById(R.id.completed);

        completed.setVisibility(View.GONE);

        Button cancel = (Button) dialog.findViewById(R.id.cancel);
        Button send = (Button) dialog.findViewById(R.id.send);


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                complete_yes.setChecked(false);
                complete_no.setChecked(false);
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pending.isChecked()) {
                    complete_value = "Pending";
                } else if (going_on.isChecked()) {
                    complete_value = "Going On";
                } else if (stand_by.isChecked()) {
                    complete_value = "Stand By";
                } else if (new_open.isChecked()) {
                    complete_value = "Newly Open";
                }

                if (!complete_value.isEmpty()) {
                    SendFinalStatus(i);
                } else {
                    Toast.makeText(context, "Select one option", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.show();
    }

    public boolean Validation() {

        boolean valid = true;
        hour_taken_value = hours_taken.getText().toString().trim();
        task_complexity_value = select_task_complexity.getSelectedItem().toString().trim();
        task_quality_value = select_task_quality.getSelectedItem().toString().trim();

        if (task_complexity_value.equalsIgnoreCase("Select") || task_quality_value.equalsIgnoreCase("Select")) {
            valid = false;
            Toast.makeText(context, "Please select all the value", Toast.LENGTH_SHORT).show();
        }
        return valid;
    }

    public void SendFinalStatus(final int i) {

        final AlertDialog.Builder dialog_ = new AlertDialog.Builder(context);
        dialog_.setCancelable(false);
        dialog_.setTitle("Please confirm to submit");

        dialog_.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int j) {

                if (complete_value.equalsIgnoreCase("Completed")) {
                    JSON_URL_TOKEN_FINAL_TASK_SUBMIT = BASE_URL + "final_task_status.php?task_id=" + taskCompleteds.get(i).getTask_id() + "&final_status=" + complete_value + "&hour_taken=" + hour_taken_value + "&task_complexity=" + task_complexity_value + "&task_quality=" + task_quality_value;
                } else {
                    JSON_URL_TOKEN_FINAL_TASK_SUBMIT = BASE_URL + "final_task_status.php?task_id=" + taskCompleteds.get(i).getTask_id() + "&final_status=" + complete_value;
                }

                try {
                    Log.d(TAG, "onClick:   json " + JSON_URL_TOKEN_FINAL_TASK_SUBMIT);
                    StringRequest req2 = new StringRequest(JSON_URL_TOKEN_FINAL_TASK_SUBMIT,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("SendFinalStatus", response.toString());

                                    try {
                                        if (response.length() > 0) {
                                            if (response.toString().equalsIgnoreCase("1")) {

                                                Toast.makeText(context, "Status has been updated successfully", Toast.LENGTH_SHORT).show();

                                                manage.setText("Manage");
                                                complete_yes.setChecked(false);
                                                complete_no.setChecked(false);

                                                cancel.setVisibility(View.GONE);
                                                radio_layout.setVisibility(View.GONE);
                                                is_complete.setVisibility(View.GONE);
                                                layout_complete.setVisibility(View.GONE);

                                                dialog2.dismiss();
                                                dialog1.dismiss();
                                                if (dialog != null)
                                                    dialog.dismiss();
                                                ((Task_Completed) context).getAllTaskCompleteRequest();
                                            }

                                        } else {
                                            Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                            dialog2.dismiss();
                                        }
                                    } catch (Exception e) {
                                        Log.d("SendFinalStatus", "onResponse: " + e);
                                        Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                        dialog2.dismiss();
                                    }
                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("SendFinalStatus", "Server Error: " + error.getMessage());
                            Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                            dialog2.dismiss();
                        }
                    });

                    RequestQueue requestQueue = Volley.newRequestQueue(context);

                    int socketTimeout = 600000;//30 seconds - change to what you want
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    req2.setRetryPolicy(policy);

                    requestQueue.add(req2);

                } catch (Exception e) {
                    Log.d("SendFinalStatus", "getCustomer: " + e);
                    Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                    dialog2.dismiss();
                }
            }
        });

        dialog_.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialog2.dismiss();
            }
        });


        dialog2 = dialog_.show();
    }

    @Override
    public int getItemCount() {

        if (taskCompleteds == null)
            return 0;
        else
            return taskCompleteds.size();

    }

    public class VHolder extends RecyclerView.ViewHolder {


        RelativeLayout task_complete_layout;
        TextView task_name;
        TextView task_assign_to;

        private VHolder(final View itemView) {

            super(itemView);

            task_complete_layout = (RelativeLayout) itemView.findViewById(R.id.task_complete_layout);

            task_name = (TextView) itemView.findViewById(R.id.task_name);
            task_assign_to = (TextView) itemView.findViewById(R.id.task_assign_to);

            bahu_thin = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_thin.ttf");
            bahu_bold = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_bold.ttf");

            task_name.setTypeface(bahu_bold);
            task_assign_to.setTypeface(bahu_thin);

        }
    }

}
