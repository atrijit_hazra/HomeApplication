package com.velarudh.tms.adapter;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.velarudh.tms.R;
import com.velarudh.tms.model.ProjectDetails;
import com.velarudh.tms.model.ProjectType;
import com.velarudh.tms.project.ManageAllProject;
import com.velarudh.tms.util.ExpandableTextViewThree;
import com.velarudh.tms.util.Url;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Atrijit on 30-05-2017.
 */

public class Manage_AllProject_adapter extends RecyclerView.Adapter<Manage_AllProject_adapter.VHolder> {

    private Context context;
    private ArrayList<ProjectDetails> projectDetailse;
    String TAG = "Manage_Allt_adapter";
    ContentResolver cv;

    String user_id;
    String JSON_URL_TOKEN_DELETE_PROJECT;
    String JSON_URL_TOKEN_EDIT_PROJECT;

    Spinner select_project_type;
    EditText select_project_name;
    Spinner select_status;
    EditText details_edit;

    ArrayList<ProjectType> project_type_name;
    ArrayList<String> project_status_list;
    ArrayList<String> project_type_id_list;
    ArrayList<String> status_list;

    String project_type_value;
    String project_type_value_id;
    String project_name_value;
    String project_name_value_id;
    String status_select_value = "Select";

    AlertDialog dialog2 = null;

    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss a");
    String current_date_time;

    String project_desc;

    String BASE_URL = Url.BASE_URL;

    Typeface bahu_thin;
    Typeface bahu_bold;

    Dialog dialog3;
    RelativeLayout project_layout;

    TextView projectname;
    TextView projecttype;
    TextView createby;
    TextView projectstatus;
    TextView projectdesc;

    TextView project_name;
    TextView project_type;
    TextView created_by;
    TextView project_status;
    ExpandableTextViewThree details;

    Button edit_project;
    Button delete_project;

    public Manage_AllProject_adapter(Context context, ArrayList<ProjectDetails> projectDetailse, String user_id) {
        this.projectDetailse = projectDetailse;
        this.context = context;
        this.user_id = user_id;
        bahu_thin = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_thin.ttf");
        bahu_bold = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_bold.ttf");
    }

    @Override
    public Manage_AllProject_adapter.VHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.manage_all_project_details, viewGroup, false);
        return new Manage_AllProject_adapter.VHolder(view);
    }


    @Override
    public void onBindViewHolder(final Manage_AllProject_adapter.VHolder viewHolder, final int i) {


        viewHolder.project_name.setText(projectDetailse.get(i).getProject_name());
        viewHolder.project_created_by.setText(projectDetailse.get(i).getProject_created_by());

        viewHolder.project_details_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DisplayProjectDetails(i);
            }
        });
    }

    public void DisplayProjectDetails(final Integer i) {

        dialog3 = new Dialog(context);
        dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog3.setContentView(R.layout.edit_manage_all_project_details);
        dialog3.setCancelable(true);
        dialog3.setCanceledOnTouchOutside(true);

        project_layout = (RelativeLayout) dialog3.findViewById(R.id.project_layout);

        projectname = (TextView) dialog3.findViewById(R.id.projectname);
        projecttype = (TextView) dialog3.findViewById(R.id.projecttype);
        createby = (TextView) dialog3.findViewById(R.id.createby);
        projectstatus = (TextView) dialog3.findViewById(R.id.projectstatus);
        projectdesc = (TextView) dialog3.findViewById(R.id.projectdesc);

        project_name = (TextView) dialog3.findViewById(R.id.project_name);
        project_type = (TextView) dialog3.findViewById(R.id.project_type);
        created_by = (TextView) dialog3.findViewById(R.id.created_by);
        project_status = (TextView) dialog3.findViewById(R.id.project_status);
        details = (ExpandableTextViewThree) dialog3.findViewById(R.id.details);
        edit_project = (Button) dialog3.findViewById(R.id.edit_project);
        delete_project = (Button) dialog3.findViewById(R.id.delete_project);

        projectname.setTypeface(bahu_bold);
        projecttype.setTypeface(bahu_bold);
        createby.setTypeface(bahu_bold);
        projectstatus.setTypeface(bahu_bold);
        projectdesc.setTypeface(bahu_bold);

        project_name.setTypeface(bahu_thin);
        project_type.setTypeface(bahu_thin);
        created_by.setTypeface(bahu_thin);
        project_status.setTypeface(bahu_thin);
        details.setTypeface(bahu_thin);
        edit_project.setTypeface(bahu_thin);
        delete_project.setTypeface(bahu_thin);

        project_name.setText(projectDetailse.get(i).getProject_name());
        project_type.setText(projectDetailse.get(i).getProject_type());
        created_by.setText(projectDetailse.get(i).getProject_created_by());

        if (projectDetailse.get(i).getProject_status().equalsIgnoreCase("0")) {
            project_status.setText("Incomplete");
        }

        if (!projectDetailse.get(i).getProject_description().isEmpty())
            details.setText(html2text(projectDetailse.get(i).getProject_description()));
        else
            details.setText("no details available");

        if (projectDetailse.get(i).getProject_created_by_id().equalsIgnoreCase(user_id)) {
            edit_project.setVisibility(View.VISIBLE);
            delete_project.setVisibility(View.VISIBLE);
        } else {
            edit_project.setVisibility(View.GONE);
            delete_project.setVisibility(View.GONE);
        }

        edit_project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EditProject(i);
            }
        });

        delete_project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DeleteProject(i);
            }
        });

        dialog3.show();
        Window window = dialog3.getWindow();
        window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
    }

    public static String html2text(String html) {
        return Html.fromHtml(html).toString();
    }

    @Override
    public int getItemCount() {

        if (projectDetailse == null)
            return 0;
        else
            return projectDetailse.size();

    }

    public void DeleteProject(final Integer i) {

        Snackbar snackbar;
        snackbar = Snackbar.make(project_layout, "Please confirm to delete", 3000);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(context.getResources().getColor(R.color.ButtonBackGround));
        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);

        snackbar.setAction("Confirm", new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                JSON_URL_TOKEN_DELETE_PROJECT = BASE_URL + "delete_project.php?project_id=" + projectDetailse.get(i).getProject_id();

                try {
                    Log.d(TAG, "onClick:   json " + JSON_URL_TOKEN_DELETE_PROJECT);
                    StringRequest req2 = new StringRequest(JSON_URL_TOKEN_DELETE_PROJECT,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        if (response.length() > 0) {
                                            if (response.toString().equalsIgnoreCase("1")) {
                                                Toast.makeText(context, "Project has been deleted successfully", Toast.LENGTH_SHORT).show();
                                                projectDetailse.remove(i);
                                                dialog3.dismiss();
                                                notifyItemRemoved(i);
                                            } else {
                                                Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                            }

                                        } else {
                                            Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();

                                        }
                                    } catch (Exception e) {
                                        Log.d("MainActivity", "onResponse: " + e);
                                        Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();

                                    }
                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("MainActivity", "Server Error: " + error.getMessage());
                            Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();

                        }
                    });

                    RequestQueue requestQueue = Volley.newRequestQueue(context);
                    int socketTimeout = 600000;//30 seconds - change to what you want
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    req2.setRetryPolicy(policy);
                    requestQueue.add(req2);

                } catch (Exception e) {
                    Log.d("MainActivity", "getCustomer: " + e);
                    Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();

                }
            }
        });

        snackbar.show();
    }

    public void EditProject(final Integer i) {

        final Dialog dialog1 = new Dialog(context);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.edit_project_dialog_layout);
        dialog1.setCancelable(false);

        Button ok;
        Button cancel;

        TextView projettyped;
        TextView projetnamed;
        TextView projetstatusd;
        TextView projetdescd;

        projettyped = (TextView) dialog1.findViewById(R.id.projettyped);
        projetnamed = (TextView) dialog1.findViewById(R.id.projetnamed);
        projetstatusd = (TextView) dialog1.findViewById(R.id.projetstatusd);
        projetdescd = (TextView) dialog1.findViewById(R.id.projetdescd);

        select_project_type = (Spinner) dialog1.findViewById(R.id.select_project_type);
        select_project_name = (EditText) dialog1.findViewById(R.id.select_project_name);
        select_status = (Spinner) dialog1.findViewById(R.id.select_status);
        details_edit = (EditText) dialog1.findViewById(R.id.details_edit);

        ok = (Button) dialog1.findViewById(R.id.submit_task);
        cancel = (Button) dialog1.findViewById(R.id.cancel_edit);

        projettyped.setTypeface(bahu_bold);
        projetnamed.setTypeface(bahu_bold);
        projetstatusd.setTypeface(bahu_bold);
        projetdescd.setTypeface(bahu_bold);

        select_project_name.setTypeface(bahu_thin);
        details_edit.setTypeface(bahu_thin);
        ok.setTypeface(bahu_thin);
        cancel.setTypeface(bahu_thin);

        project_name_value = projectDetailse.get(i).getProject_name();
        project_name_value_id = projectDetailse.get(i).getProject_id();
        project_desc = projectDetailse.get(i).getProject_description();

        select_project_name.setText(project_name_value);
        details_edit.setText(html2text(project_desc));

        SetSpinnerValue(i);


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                project_desc = details_edit.getText().toString().trim();
                project_name_value = select_project_name.getText().toString().trim();

                if (project_desc.length() < 1) {
                    details_edit.setError("Please enter desription");
                } else if (project_name_value.length() < 1) {
                    select_project_name.setError("Please enter project name");
                } else {
                    select_project_name.setError(null);
                    details_edit.setError(null);

                    project_desc = MakeSubTextToParse(project_desc);
                    project_name_value = MakeSubTextToParse(project_name_value);
                    ConfirmationAlert(dialog1);
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog1.dismiss();
            }
        });

        dialog1.show();
        Window window = dialog1.getWindow();
        window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
    }

    public void SetSpinnerValue(final Integer i) {
        final int color = 0xff2d7da6;
        project_type_name = new ArrayList<>();
        project_status_list = new ArrayList<>();
        project_type_id_list = new ArrayList<>();

        for (ProjectDetails project : projectDetailse) {

            String type = project.getProject_type();
            String name = project.getProject_name();
            String type_id = project.getProject_type_id();
            String name_id = project.getProject_id();

            ProjectType projectType = new ProjectType();

            if (project_status_list.isEmpty()) {
                project_status_list.add(type);
                project_type_id_list.add(type_id);
            } else {
                if (!project_status_list.contains(type)) {
                    project_status_list.add(type);
                    project_type_id_list.add(type_id);
                }
            }
            projectType.project_type = type;
            projectType.project_name = name;
            projectType.project_type_id = type_id;
            projectType.project_name_id = name_id;

            project_type_name.add(projectType);

        }

        ArrayAdapter<String> adapter_project_type = new ArrayAdapter<String>(context, R.layout.spinner_item, project_status_list) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }
        };
        adapter_project_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        select_project_type.setAdapter(adapter_project_type);
        select_project_type.getBackground().setColorFilter(context.getResources().getColor(R.color.ItemColor), PorterDuff.Mode.SRC_ATOP);

        select_project_type.setSelection(project_status_list.indexOf(projectDetailse.get(i).getProject_type()));
        project_type_value = projectDetailse.get(i).getProject_type();
        project_type_value_id = projectDetailse.get(i).getProject_type_id();

        select_project_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                project_type_value = project_status_list.get(i);
                project_type_value_id = project_type_id_list.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        status_list = new ArrayList<>();
        status_list.add("INCOMPLETE");
        status_list.add("COMPLETE");

        ArrayAdapter<String> adapter_status = new ArrayAdapter<String>(context, R.layout.spinner_item, status_list) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }
        };
        adapter_status.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        select_status.setAdapter(adapter_status);
        select_status.setSelection(status_list.indexOf(projectDetailse.get(i).getProject_status().toUpperCase()));
        status_select_value = projectDetailse.get(i).getProject_status();
        select_status.getBackground().setColorFilter(context.getResources().getColor(R.color.ItemColor), PorterDuff.Mode.SRC_ATOP);

        select_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                status_select_value = status_list.get(i);
                if (status_select_value.equalsIgnoreCase("COMPLETE")) {
                    status_select_value = "1";
                } else {
                    status_select_value = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void ConfirmationAlert(final Dialog dialog1) {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setCancelable(false);
        dialog.setTitle("Please confirm to submit");

        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                current_date_time = simpleDateFormat.format(calendar.getTime());
                JSON_URL_TOKEN_EDIT_PROJECT = BASE_URL + "edit_project.php?user_id=" + user_id + "&project_type_id=" + project_type_value_id + "&project_id=" + project_name_value_id + "&project_name=" + project_name_value + "&status=" + status_select_value + "&desc=" + project_desc + "&date=" + current_date_time;
                try {
                    Log.d(TAG, "onClick:   json " + JSON_URL_TOKEN_EDIT_PROJECT);
                    StringRequest req2 = new StringRequest(JSON_URL_TOKEN_EDIT_PROJECT,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("ConfirmationAlert", response.toString());
                                    try {
                                        if (response.length() > 0) {
                                            if (response.toString().equalsIgnoreCase("1")) {
                                                Toast.makeText(context, "Project has been updated successfully", Toast.LENGTH_SHORT).show();
                                                dialog1.dismiss();
                                                dialog2.dismiss();
                                                dialog3.dismiss();
                                                ((ManageAllProject) context).ReloadData();
                                                notifyDataSetChanged();
                                            }
                                        } else {
                                            Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                            dialog1.dismiss();
                                            dialog2.dismiss();
                                        }
                                    } catch (Exception e) {
                                        Log.d("ConfirmationAlert", "onResponse: " + e);
                                        Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                        dialog1.dismiss();
                                        dialog2.dismiss();
                                    }
                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("ConfirmationAlert", "Server Error: " + error.getMessage());
                            Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                            dialog1.dismiss();
                            dialog2.dismiss();
                        }
                    });

                    RequestQueue requestQueue = Volley.newRequestQueue(context);

                    int socketTimeout = 600000;//30 seconds - change to what you want
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    req2.setRetryPolicy(policy);

                    requestQueue.add(req2);

                } catch (Exception e) {
                    Log.d("ConfirmationAlert", "getCustomer: " + e);
                    Toast.makeText(context, "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                    dialog1.dismiss();
                    dialog2.dismiss();
                }
            }
        });

        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialog2.dismiss();
            }
        });
        dialog2 = dialog.show();
    }

    public String MakeSubTextToParse(String name) {
        String[] name1 = name.split("\\s");
        name = "";
        for (String n2 : name1) {
            Log.d("n2", "" + n2);
            name += n2 + "%20";
        }
        return name;
    }


    public class VHolder extends RecyclerView.ViewHolder {
        RelativeLayout project_details_layout;
        TextView project_name;
        TextView project_created_by;

        private VHolder(final View itemView) {
            super(itemView);
            project_details_layout = (RelativeLayout) itemView.findViewById(R.id.project_details_layout);
            project_name = (TextView) itemView.findViewById(R.id.project_name);
            project_created_by = (TextView) itemView.findViewById(R.id.project_created_by);

            project_name.setTypeface(bahu_bold);
            project_created_by.setTypeface(bahu_thin);
        }
    }
}
