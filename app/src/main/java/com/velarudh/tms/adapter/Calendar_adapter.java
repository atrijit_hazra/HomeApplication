package com.velarudh.tms.adapter;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.velarudh.tms.R;
import com.velarudh.tms.model.DateExtendDetails;
import com.velarudh.tms.model.TaskDetails;
import com.velarudh.tms.task.Task_details;
import com.velarudh.tms.util.CircularTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Atrijit on 09-06-2017.
 */

public class Calendar_adapter extends RecyclerView.Adapter<Calendar_adapter.VHolder> {

    private Context context;
    private ArrayList<TaskDetails> taskDetailses;
    private ArrayList<Integer> color_list;
    String TAG = "Calendar_adapter";
    String onlyClassName;

    public Calendar_adapter(Context context, ArrayList<TaskDetails> taskDetailses, ArrayList<Integer> color_list , String onlyClassName) {
        this.taskDetailses = taskDetailses;
        this.context = context;
        this.color_list = color_list;
        this.onlyClassName = onlyClassName;
    }

    @Override
    public Calendar_adapter.VHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.calendar_list_details, viewGroup, false);
        return new Calendar_adapter.VHolder(view);
    }


    @Override
    public void onBindViewHolder(final Calendar_adapter.VHolder viewHolder, final int i) {

        TaskDetails td = taskDetailses.get(i);

        String colorCode = String.format("#%06X", (0xFFFFFF & color_list.get(i)));

        Log.d(TAG, "onBindViewHolder: "+(color_list.get(i)+"") +"    "+colorCode);
        viewHolder.task_name.setText(td.getTask_name());
        viewHolder.color_indicator.setSolidColor(colorCode);

        viewHolder.task_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<TaskDetails> select_task = new ArrayList<TaskDetails>();
                select_task.add(taskDetailses.get(i));

                Intent in = new Intent(context,Task_details.class);
                in.putExtra("source",onlyClassName);
                in.putParcelableArrayListExtra("select_task",select_task);
                in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(in);


            }
        });

    }


    @Override
    public int getItemCount() {

        if (taskDetailses == null)
            return 0;
        else
            return taskDetailses.size();

    }

    public class VHolder extends RecyclerView.ViewHolder {

        RelativeLayout date_extension;

        TextView task_name;
        CircularTextView color_indicator;

        private VHolder(final View itemView) {

            super(itemView);

            date_extension = (RelativeLayout) itemView.findViewById(R.id.date_extension);

            task_name = (TextView) itemView.findViewById(R.id.task_name);
            color_indicator = (CircularTextView) itemView.findViewById(R.id.color_indicator);

            Typeface bahu_thin = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_thin.ttf");
            task_name.setTypeface(bahu_thin);
        }
    }


}
