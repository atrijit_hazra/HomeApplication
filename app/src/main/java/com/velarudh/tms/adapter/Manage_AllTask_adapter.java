package com.velarudh.tms.adapter;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.velarudh.tms.R;
import com.velarudh.tms.model.TaskDetails;
import com.velarudh.tms.task.ManageAllTask;
import com.velarudh.tms.util.ExpandableTextViewTwo;
import java.util.ArrayList;

public class Manage_AllTask_adapter extends RecyclerView.Adapter<Manage_AllTask_adapter.VHolder> {

    private Context context;
    private ArrayList<TaskDetails> task_details;
    String TAG = "Manage_AllTask_adapter";
    ContentResolver cv;
    String user_id;


    public Manage_AllTask_adapter(Context context, ArrayList<TaskDetails> task_details, String user_id) {
        this.task_details = task_details;
        this.context = context;
        this.user_id = user_id;

    }

    @Override
    public Manage_AllTask_adapter.VHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.manage_alltasks_details, viewGroup, false);
        return new Manage_AllTask_adapter.VHolder(view);
    }


    @Override
    public void onBindViewHolder(final Manage_AllTask_adapter.VHolder viewHolder, final int i) {

        viewHolder.manage_project_name.setText(task_details.get(i).getProject_name());
        viewHolder.manage_task_name.setText(task_details.get(i).getTask_name());

        if (task_details.get(i).getTask_assign_by_id().equalsIgnoreCase(user_id)) {
            viewHolder.assign_task.setVisibility(View.VISIBLE);
        }
        else
            viewHolder.assign_task.setVisibility(View.GONE);

        viewHolder.manage_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.manage_task_options);
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);

                final RadioGroup option_choose = (RadioGroup) dialog.findViewById(R.id.option_choose);
                RadioButton only_view = (RadioButton) dialog.findViewById(R.id.only_view);
                RadioButton only_edit = (RadioButton) dialog.findViewById(R.id.only_edit);
                RadioButton delete = (RadioButton) dialog.findViewById(R.id.delete);
                Button cancel = (Button) dialog.findViewById(R.id.cancel);

                if (!task_details.get(i).getTask_assign_by_id().equalsIgnoreCase(user_id)) {
                    only_edit.setVisibility(View.GONE);
                    delete.setVisibility(View.GONE);
                }
                option_choose.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup, int j) {

                        int radioButtonID = radioGroup.getCheckedRadioButtonId();
                        Log.d(TAG, "onClick: "+radioButtonID);
                        RadioButton radioButton = (RadioButton)radioGroup.findViewById(radioButtonID);
                        String selectedtext = (String) radioButton.getText();

                        Log.d(TAG, "onCheckedChanged: "+selectedtext);

                        switch (selectedtext)
                        {
                            case "View":
                                dialog.dismiss();
                                ((ManageAllTask) context).EditAssignTask(i,selectedtext);
                                break;

                            case "Edit":
                                dialog.dismiss();
                                ((ManageAllTask) context).EditAssignTask(i,selectedtext);
                                break;

                            case "Delete":
                                dialog.dismiss();
                                ((ManageAllTask) context).DeleteTask(i);
                                break;
                        }

                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }

        });
    }

    @Override
    public int getItemCount() {
        if (task_details == null)
            return 0;
        else
            return task_details.size();
    }

    public class VHolder extends RecyclerView.ViewHolder {
        RelativeLayout manage_layout;
        RelativeLayout sub_task_layout;
        TextView manage_project_name;
        ExpandableTextViewTwo manage_task_name;
        ImageView assign_task;

        private VHolder(final View itemView) {

            super(itemView);

            manage_layout = (RelativeLayout) itemView.findViewById(R.id.layout);

            manage_project_name = (TextView) itemView.findViewById(R.id.manage_project_name);
            manage_task_name = (ExpandableTextViewTwo) itemView.findViewById(R.id.manage_task_name);
            assign_task = (ImageView) itemView.findViewById(R.id.assign_task);
            Typeface bahu_thin = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_thin.ttf");
            Typeface bahu_bold = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_bold.ttf");

            manage_project_name.setTypeface(bahu_bold);
            manage_task_name.setTypeface(bahu_thin);

        }
    }

}
