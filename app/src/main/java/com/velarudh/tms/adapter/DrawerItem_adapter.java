package com.velarudh.tms.adapter;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.velarudh.tms.model.DrawerItem;
import com.velarudh.tms.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Atrijit on 19-04-2017.
 */

public class DrawerItem_adapter extends RecyclerView.Adapter<DrawerItem_adapter.VHolder>{

    private Context context;
    private ArrayList<DrawerItem> ImageList;
    String TAG = "DrawerItem_adapter";
    ContentResolver cv;

    public DrawerItem_adapter(Context context, ArrayList<DrawerItem> ImageList) {
        this.ImageList = ImageList;
        this.context = context;

    }

    @Override
    public DrawerItem_adapter.VHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.drawer_list_item, viewGroup, false);
        return new DrawerItem_adapter.VHolder(view);
    }


    @Override
    public void onBindViewHolder(final DrawerItem_adapter.VHolder viewHolder, final int i) {

        DrawerItem di = ImageList.get(i);

        viewHolder.draweritem_icon.setImageResource(di.getIcon());
        viewHolder.draweritem_name.setText(di.getTitle());

        if(di.getCount() == 0)
        {
            viewHolder.count_display.setVisibility(View.GONE);
        }
        else
        {
            viewHolder.count_display.setVisibility(View.VISIBLE);
            viewHolder.count_display.setText(di.getCount()+"");
        }

//        if(isSelected(i))
//        {
//            viewHolder.item_layout.setBackgroundResource(R.drawable.nevigation_itemclick);
//        }
//        else
//        {
//            viewHolder.item_layout.setBackgroundResource(R.drawable.navigation_itemrelease);
//        }



    }

    @Override
    public int getItemCount() {

        if(ImageList == null)
            return 0;
        else
            return  ImageList.size();

    }

    public class VHolder extends RecyclerView.ViewHolder{

        RelativeLayout item_layout;
        ImageView draweritem_icon;
        TextView draweritem_name;
        Button count_display;

        private VHolder(final View itemView) {

            super(itemView);

            item_layout = (RelativeLayout) itemView.findViewById(R.id.item_layout);
            draweritem_icon = (ImageView) itemView.findViewById(R.id.draweritem_icon);
            draweritem_name = (TextView) itemView.findViewById(R.id.draweritem_name);
            count_display = (Button) itemView.findViewById(R.id.count_display);

            Typeface bahu_bold = Typeface.createFromAsset(context.getAssets(), "fonts/bauhaus_thin.ttf");
            draweritem_name.setTypeface(bahu_bold);
        }
    }

    public interface ClickListener {
        void onClick(int position);

        void onLongClick(int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private DrawerItem_adapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final DrawerItem_adapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }


    }

}
