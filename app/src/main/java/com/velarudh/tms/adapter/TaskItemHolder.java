package com.velarudh.tms.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.unnamed.b.atv.model.TreeNode;
import com.velarudh.tms.R;
import com.velarudh.tms.model.TaskDetails;
import com.velarudh.tms.task.NewTask_SubTask;
import com.velarudh.tms.task.Task_details;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by Bogdan Melnychuk on 2/12/15.
 */
public class TaskItemHolder extends TreeNode.BaseNodeViewHolder<TaskItemHolder.IconTreeItem> {
    private TextView tvValue;
    private ImageButton view_task;
    private ImageButton assign_task_others;
    TaskDetails taskDetails;
    Activity activity;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
    Calendar cal = Calendar.getInstance();

    public TaskItemHolder(Context context, TaskDetails taskDetails, Activity activity) {
        super(context);
        this.taskDetails = taskDetails;
        this.activity = activity;
    }

    @Override
    public View createNodeView(final TreeNode node, IconTreeItem value) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_task_node, null, false);
        view_task = (ImageButton) view.findViewById(R.id.view_task);
        assign_task_others = (ImageButton) view.findViewById(R.id.assign_task_others);
        tvValue = (TextView) view.findViewById(R.id.task_name);
        if (!taskDetails.getTask_parent_id().equalsIgnoreCase("0"))
            tvValue.setText(taskDetails.getTask_name() + "(sub)");

        try {
            Date task_end = simpleDateFormat.parse(taskDetails.getTask_end_date());
            Date current = simpleDateFormat.parse(simpleDateFormat.format(cal.getTime()));

            if (current.after(task_end)) {
                assign_task_others.setVisibility(View.GONE);
            } else {
                assign_task_others.setVisibility(View.VISIBLE);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        view_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<TaskDetails> select_task = new ArrayList<TaskDetails>();
                select_task.add(taskDetails);

                Intent in = new Intent(context, Task_details.class);
                in.putParcelableArrayListExtra("select_task", select_task);
                context.startActivity(in);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

            }
        });

        assign_task_others.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<TaskDetails> select_task = new ArrayList<TaskDetails>();
                select_task.add(taskDetails);

                Intent in = new Intent(context, NewTask_SubTask.class);
                in.putExtra("sub_task", "yes");
                in.putParcelableArrayListExtra("select_task", select_task);
                context.startActivity(in);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        return view;
    }

    @Override
    public void toggle(boolean active) {
//        arrowView.setBackground(context.getResources().getDrawable(active ? R.drawable.ic_arrow_down : R.drawable.ic_arrow_down));
    }

    public static class IconTreeItem {
        public int icon;
        public String text;

        public IconTreeItem(String text) {
//            this.icon = icon;
            this.text = text;
        }
    }
}
