package com.velarudh.tms.calendar;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;
import com.velarudh.tms.R;
import com.velarudh.tms.adapter.Calendar_adapter;
import com.velarudh.tms.main.Dashboard;
import com.velarudh.tms.model.CalendarDate;
import com.velarudh.tms.model.TaskDetails;
import com.velarudh.tms.task.Task_details;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;

public class Task_calendar extends AppCompatActivity {

    String TAG = "Task_calendar";
    Toolbar toolbar;
    FloatingActionButton fab;

    ArrayList<TaskDetails> task_detailses;

    MaterialCalendarView calendarView;

    Calendar cal = Calendar.getInstance();

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");

    int color ;

    Collection<CalendarDay> day_decorate_list;

    CalendarDay decorate_day;

    Date start_date = null;
    Date end_date = null;

    ArrayList<Integer> color_list ;

    ArrayList<CalendarDate> cd_list;

    ArrayList<Integer> position_list;

    ArrayList<TaskDetails> td_dateclick;
    ArrayList<Integer> color_dateclick;

    RecyclerView date_click_recycler;
    TextView no_task;

    String onlyClassName;
    Typeface Bahu_thin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_calendar);

        init();

        DecorateCalendar();

        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {

                position_list = new ArrayList<Integer>();
                td_dateclick = new ArrayList<TaskDetails>();
                color_dateclick = new ArrayList<Integer>();

                for(int i = 0 ; i<cd_list.size() ; i++)
                {
                    CalendarDate cd = cd_list.get(i);
                    Date s_date = cd.getStart_date();
                    Date e_date = cd.getEnd_date();

                    Calendar start = Calendar.getInstance();
                    start.setTime(s_date);

                    Calendar end = Calendar.getInstance();
                    end.setTime(e_date);


                    while(!start.after(end))
                    {
                        if(date.equals(CalendarDay.from(start)))
                        {
                            position_list.add(cd.getPosition());
                            color_dateclick.add(cd.getColor());
                            Log.d(TAG, "onDateSelected: "+position_list);
                        }
                        start.add(start.DATE, 1);

                    }

                }

                if(position_list.isEmpty())
                {
                    date_click_recycler.setVisibility(View.GONE);
                    no_task.setVisibility(View.VISIBLE);
                }
                else {

                    date_click_recycler.setVisibility(View.VISIBLE);
                    no_task.setVisibility(View.GONE);

                    for (int j = 0; j < position_list.size(); j++) {

                        TaskDetails td = task_detailses.get(position_list.get(j));
                        td_dateclick.add(td);

                        Log.d(TAG, "onDateSelected: "+color_dateclick);

                        Calendar_adapter adapter = new Calendar_adapter(getApplicationContext(), td_dateclick, color_dateclick , onlyClassName);
                        date_click_recycler.setAdapter(adapter);

                        LinearLayoutManager lm = new LinearLayoutManager(getApplicationContext());
                        date_click_recycler.setLayoutManager(lm);

                    }
                }

            }
        });
    }

    public void init() {

        onlyClassName = this.getClass().getName();

        task_detailses = getIntent().getParcelableArrayListExtra("task_details");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.title_activity_task_calendar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab_open);

        calendarView = (MaterialCalendarView) findViewById(R.id.simpleCalendarView);

        calendarView.state().edit()
                .setFirstDayOfWeek(Calendar.MONDAY)
                .setCalendarDisplayMode(CalendarMode.MONTHS)
                .commit();

        calendarView.setSelectionColor(Color.TRANSPARENT);

        color_list = new ArrayList<>();

        color_list.add(0xFF000000);
        color_list.add(0xFFAD1457);
        color_list.add(0xFFE040FB);
        color_list.add(0xFFCDDC39);
        color_list.add(0xFFFFFFFF);

        day_decorate_list = new HashSet<>();
        day_decorate_list.add( CalendarDay.from(cal.getTime()));
        calendarView.addDecorator(new EventDecorator(0xFFF44336, day_decorate_list , 20));

        date_click_recycler = (RecyclerView) findViewById(R.id.date_click_recycler);
        no_task = (TextView) findViewById(R.id.no_task);
        Bahu_thin = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_thin.ttf");
        no_task.setTypeface(Bahu_thin);

    }

    public void DecorateCalendar(){

        cd_list = new ArrayList<>();

        for(int i =0 ; i<task_detailses.size() ; i++) {

            day_decorate_list = new HashSet<>();

            try {
                start_date = simpleDateFormat.parse(task_detailses.get(i).getTask_start_date());
                end_date = simpleDateFormat.parse(task_detailses.get(i).getTask_end_date());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Calendar start_cal = Calendar.getInstance();
            start_cal.setTime(start_date);

            Calendar end_cal = Calendar.getInstance();
            end_cal.setTime(end_date);

            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

            Log.d(TAG, "DecorateCalendar:  color bfr"+color);
            CalendarDate cd = new CalendarDate();
            cd.setPosition(i);
            cd.setStart_date(start_date);
            cd.setEnd_date(end_date);
            cd.setColor(color);

            Log.d(TAG, "DecorateCalendar: color"+cd.getColor());
            cd_list.add(cd);

            while (!start_cal.after(end_cal)) {
                decorate_day = CalendarDay.from(start_cal.getTime());
                day_decorate_list.add(decorate_day);
                calendarView.addDecorator(new EventDecorator(color, day_decorate_list , 10));

                start_cal.add(start_cal.DATE, 1);

                Log.d(TAG, "init: " + start_cal.getTime());
            }
        }

    }


    public class EventDecorator implements DayViewDecorator {


        private final int color;
        private final int span_size;
        private final HashSet<CalendarDay> dates;

        public EventDecorator(int color, Collection<CalendarDay> dates , int span_size) {
            this.color = color;
            this.dates = new HashSet<>(dates);
            this.span_size = span_size;
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return dates.contains(day);
        }

        @Override
        public void decorate(DayViewFacade view) {

            view.addSpan(new DotSpan(span_size, color));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(this, Dashboard.class);
        startActivity(in);
    }
}
