package com.velarudh.tms.firebase;

import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Config;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.velarudh.tms.home.MainActivity;
import com.velarudh.tms.main.Dashboard;

import org.json.JSONException;
import org.json.JSONObject;

import static android.R.id.message;

/**
 * Created by Atrijit on 14-09-2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";

    String TaskAlertMessage = "";
    String TaskName = "";
    String AssignedBy = "";
    String TaskMessageBody;

    String ChatMessage = "";
    String SendBy = "";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        super.onMessageReceived(remoteMessage);

        Log.d(TAG, "onMessageReceived: ");

        if (NotificationUtils.isAppIsInBackground(this)) {

            TaskAlertMessage = remoteMessage.getData().toString();
            Log.d(TAG, "From: " + remoteMessage.getFrom());
            Log.d(TAG, "Notification Message Body: " + TaskAlertMessage.toString());

            try {
                JSONObject jsonObject = new JSONObject(TaskAlertMessage.toString());
                TaskName = jsonObject.getJSONObject("data").getString("title");
                AssignedBy = jsonObject.getJSONObject("data").getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            TaskMessageBody = "Assigned by: " + AssignedBy;

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Intent resultIntent = new Intent(getApplicationContext(), Dashboard.class);
            resultIntent.putExtra("message", message);
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.showNotificationMessage(TaskName, TaskMessageBody, resultIntent, defaultSoundUri);
        }
        else
        {
            Log.e(TAG, "Notification Body: chat " + remoteMessage.getNotification().getBody());
            Log.d(TAG, "From: chat " + remoteMessage.getFrom());
            ChatMessage = remoteMessage.getNotification().getBody();
            SendBy = remoteMessage.getFrom();
            handleNotification(remoteMessage.getNotification().getBody());
        }


    }

    private void handleNotification(String message) {

    }


}
