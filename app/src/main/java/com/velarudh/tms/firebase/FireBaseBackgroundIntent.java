package com.velarudh.tms.firebase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Atrijit on 18-09-2017.
 */

public class FireBaseBackgroundIntent extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

         if (intent.getAction().equals("pushNotification")) {
            String message = intent.getStringExtra("message");

             Log.d("FireBaseBaoundIntent", "onReceive: "+message);
        }
    }
}
