package com.velarudh.tms.firebase;

import android.os.StrictMode;
import android.util.Log;

import com.velarudh.tms.util.Url;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * Created by Atrijit on 13-07-2017.
 */

public class SendMessageToDevice {

    static InputStream is;
    static JSONObject jObj;
    static String json = "";
    String request_id;

    String URL = Url.BASE_URL+"chat_send.php";

    public SendMessageToDevice() {

    }

    DefaultHttpClient httpClient;
    HttpPost httpPost;

    public String SendMessage(String message , String sender_id , String receiver_id){

        try {

            Log.d("", "SendMessage: "+URL);
            StrictMode.ThreadPolicy policyy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

            StrictMode.setThreadPolicy(policyy);

            httpClient = new DefaultHttpClient();
            httpPost = new HttpPost(URL);

            JSONObject value = new JSONObject();
            try {

                value.put("sender_id",sender_id);
                value.put("receiver_id",receiver_id);
                value.put("msg",message);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.d("", "SendMessage: "+value.toString());
            StringEntity se = new StringEntity(value.toString());
            httpPost.setEntity(se);
            httpPost.setHeader("Content-Type", "application/json");
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();

            json = sb.toString();
            Log.e("JSONStr fare request", json);
        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        return request_id;
    }
}
