package com.velarudh.tms.firebase;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by Atrijit on 18-04-2017.
 */

public class FireBaseSessionManager {

    SharedPreferences pref;
    // Editor reference for Shared preferences
    SharedPreferences.Editor editor;


    Context _context;


    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREFER_NAME = "FireBase";

    private static final String USER_TOKEN = "user_token";


    // Constructor
    public FireBaseSessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

//
    public void setUserToken(String url)
    {
        editor.putString(USER_TOKEN, url);
        editor.commit();
    }
    public void clearSession() {

        // Clearing all user data from Shared Preferences
        editor.clear();
        editor.commit();
    }

    public String getUserToken()
    {
        return pref.getString(USER_TOKEN,null);
    }


}
