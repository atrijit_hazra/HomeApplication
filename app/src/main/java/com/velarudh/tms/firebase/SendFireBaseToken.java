package com.velarudh.tms.firebase;

import android.content.Context;
import android.os.StrictMode;
import android.util.Log;

import com.velarudh.tms.util.Url;
import com.velarudh.tms.util.UserSessionManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * Created by Atrijit on 13-07-2017.
 */

public class SendFireBaseToken {

    static InputStream is;
    static JSONObject jObj;
    static String json = "";
    String UserId;
    String FireBaseToken;
    Context context;
    String JSON_URL_SEND_FIREBASE_TOKEN = Url.BASE_URL+"send_firebase_token.php?user_id=";

    UserSessionManager usm;
    FireBaseSessionManager fcm;

    public SendFireBaseToken(Context context) {
        this.context = context;
        usm = new UserSessionManager(context);
        fcm = new FireBaseSessionManager(context);
        UserId = usm.getUserDetails().get("user_id");
        FireBaseToken = fcm.getUserToken();

        JSON_URL_SEND_FIREBASE_TOKEN = JSON_URL_SEND_FIREBASE_TOKEN+UserId+"&user_token="+FireBaseToken;
    }

    DefaultHttpClient httpClient;
    HttpGet httpGet;

    public boolean SendFireBaseToken() {

        boolean valid = false;
        try {

            StrictMode.ThreadPolicy policyy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policyy);
            httpClient = new DefaultHttpClient();
            httpGet = new HttpGet(JSON_URL_SEND_FIREBASE_TOKEN);

            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();

            json = sb.toString();
            Log.e("JSONStr fare estimate", json);
        } catch (Exception e) {
            e.getMessage();
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        if(json.equalsIgnoreCase("1"))
            valid = true;

        return valid;
    }
}
