package com.velarudh.tms.firebase;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.velarudh.tms.R;
import com.velarudh.tms.adapter.All_EmployeeDetails_adapter;
import com.velarudh.tms.adapter.Online_Users_adapter;
import com.velarudh.tms.json.EmployeeDetailsReturnJson;
import com.velarudh.tms.main.Dashboard;
import com.velarudh.tms.model.EmployeeDetails;
import com.velarudh.tms.util.ConnectivityReceiver;
import com.velarudh.tms.util.NetErrorActivity;
import com.velarudh.tms.util.Url;
import com.velarudh.tms.util.UserSessionManager;

import org.json.JSONArray;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class OnlineUsersAvailable extends AppCompatActivity {

    String TAG = "OnlineUsersAvailable";
    Toolbar toolbar;
    SwipeRefreshLayout swipe_layout;
    RecyclerView user_recycler;
    ArrayList<EmployeeDetails> employee_detail;

    UserSessionManager usm;
    HashMap<String, String> user_details;
    String U_EMP_ID = "user_id";
    String user_emp_id;

    String BASE_URL = Url.BASE_URL;
    ConnectivityReceiver myReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_users_available);

        Init();
        if (checkConnection()) {
            getAllUserDetails();
        } else {
            Intent in = new Intent(this, NetErrorActivity.class);
            in.putExtra("layout_flag", "1");
            startActivity(in);
        }

        swipe_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getAllUserDetails();
            }
        });
    }

    public void Init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.title_activity_online_users_available);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        user_recycler = (RecyclerView) findViewById(R.id.user_recycler);
        swipe_layout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);

        usm = new UserSessionManager(this);

        user_details = usm.getUserDetails();
        user_emp_id = user_details.get(U_EMP_ID);
    }

    public void getAllUserDetails() {

        String JSON_URL_TOKEN_EMPLOYEE_DETAILS = BASE_URL + "all_employees.php?";


        Log.d(TAG, "getAllUserDetails: " + JSON_URL_TOKEN_EMPLOYEE_DETAILS);

        try {
            // Volley's json array request object
            JsonArrayRequest req2 = new JsonArrayRequest(JSON_URL_TOKEN_EMPLOYEE_DETAILS,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d("getTaskDetailsData", response.toString());

                            try {
                                if (response.length() > 0) {

                                    EmployeeDetailsReturnJson uds = new EmployeeDetailsReturnJson(response.toString(), getApplicationContext());
                                    employee_detail = uds.EmployeeDetailsReturnJson();

                                    setData();

                                    swipe_layout.setRefreshing(false);

                                } else {

                                }
                            } catch (Exception e) {
                                Log.d("getAllUserDetails", "onResponse: " + e);
                                swipe_layout.setRefreshing(false);

                            }
                        }

                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Log.e("getAllUserDetails", "Server Error: " + error.getMessage());
                            swipe_layout.setRefreshing(false);

                        }
                    });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            int socketTimeout = 600000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req2.setRetryPolicy(policy);

            requestQueue.add(req2);


        } catch (Exception e) {
            Log.d("getTaskDetailsData", "exception: " + e);
            swipe_layout.setRefreshing(false);
        }
    }

    public void setData() {

        Online_Users_adapter adapter = new Online_Users_adapter(this, employee_detail, user_emp_id);
        user_recycler.setAdapter(adapter);

        LinearLayoutManager lm = new LinearLayoutManager(this);
        user_recycler.setLayoutManager(lm);

        user_recycler.addOnItemTouchListener(new Online_Users_adapter.RecyclerTouchListener(this, user_recycler, new Online_Users_adapter.ClickListener() {
            @Override
            public void onClick(int position) {
                EmployeeDetails employeeDetails = employee_detail.get(position);
                ArrayList<EmployeeDetails> ToForward = new ArrayList<EmployeeDetails>();
                ToForward.add(employeeDetails);
                Intent in = new Intent(OnlineUsersAvailable.this,ChatActivity.class);
                in.putParcelableArrayListExtra("to_forward",ToForward);
                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(in);
            }

            @Override
            public void onLongClick(int position) {

            }
        }));
    }

    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected(getApplicationContext());
        return isConnected;
    }

    @Override
    protected void onResume() {
        super.onResume();
        myReceiver = new ConnectivityReceiver();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        if (checkConnection()) {
            getAllUserDetails();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(myReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
