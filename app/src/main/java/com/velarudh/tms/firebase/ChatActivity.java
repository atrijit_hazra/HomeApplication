package com.velarudh.tms.firebase;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.velarudh.tms.R;
import com.velarudh.tms.model.EmployeeDetails;
import com.velarudh.tms.util.UserSessionManager;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ChatActivity extends AppCompatActivity {

    String TAG = "ChatActivity";
    Toolbar toolbar;
    SwipeRefreshLayout swipe_layout;
    RecyclerView chat_recycler;
    EditText message;

    ArrayList<EmployeeDetails> employee_details;
    EmployeeDetails employeeDetails;

    UserSessionManager usm;
    HashMap<String, String> user_details;
    String U_EMP_ID = "user_id";
    String user_emp_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat);

        init();


    }

    public void init() {

        employee_details = getIntent().getParcelableArrayListExtra("to_forward");
        employeeDetails = employee_details.get(0);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(employeeDetails.getEmployee_name());
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        swipe_layout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        chat_recycler = (RecyclerView) findViewById(R.id.chat_recycler);

        message = (EditText) findViewById(R.id.message);
        Typeface bahu_thin = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_thin.ttf");
        message.setTypeface(bahu_thin);

        usm = new UserSessionManager(this);

        user_details = usm.getUserDetails();
        user_emp_id = user_details.get(U_EMP_ID);
    }

    public void SendMessage(View v) {

        String msg = message.getText().toString().trim();

        SendMessageToDevice messageToDevice = new SendMessageToDevice();
        String reply = messageToDevice.SendMessage(msg,employeeDetails.getEmployee_id(),user_emp_id);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
