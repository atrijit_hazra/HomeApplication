package com.velarudh.tms.firebase;

import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.velarudh.tms.util.UserSessionManager;

import java.io.IOException;

/**
 * Created by Atrijit on 14-09-2017.
 */

public class FirebaseIDService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIDService";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

//        sendRegistrationToServer(refreshedToken);
    }

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        UserSessionManager usm = new UserSessionManager(getApplicationContext());
        String UserId = usm.getUserDetails().get("user_id");

        if (UserId != null) {
            new FireBaseSessionManager(getApplicationContext()).setUserToken(token);
            Log.d(TAG, "sendRegistrationToServer: " + new FireBaseSessionManager(getApplicationContext()).getUserToken());
            SendFireBaseToken sendFireBaseToken = new SendFireBaseToken(this);
            if (!sendFireBaseToken.SendFireBaseToken())
                sendRegistrationToServer(token);
        }
        else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        FirebaseInstanceId.getInstance().deleteInstanceId();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }
}
