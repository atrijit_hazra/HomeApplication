package com.velarudh.tms.model;

/**
 * Created by Atrijit on 14-02-2018.
 */

public class ParentAndSubTask {

    public Boolean isParentTask;
    public Boolean isTask;
    public String parentTaskName;
    public String taskName;
    public String taskId;
    public TaskDetails taskDetails;

    public ParentAndSubTask(){

    }

    public Boolean getParentTask() {
        return isParentTask;
    }

    public void setParentTask(Boolean parentTask) {
        isParentTask = parentTask;
    }

    public Boolean getTask() {
        return isTask;
    }

    public void setTask(Boolean task) {
        isTask = task;
    }

    public String getParentTaskName() {
        return parentTaskName;
    }

    public void setParentTaskName(String parentTaskName) {
        this.parentTaskName = parentTaskName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public TaskDetails getTaskDetails() {
        return taskDetails;
    }

    public void setTaskDetails(TaskDetails taskDetails) {
        this.taskDetails = taskDetails;
    }
}
