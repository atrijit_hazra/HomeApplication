package com.velarudh.tms.model;

/**
 * Created by Atrijit on 25-05-2017.
 */

public class ProjectType {

    public String project_type;
    public String project_name;
    public String project_type_id;
    public String project_name_id;

    public String getProject_type() {
        return project_type;
    }

    public void setProject_type(String project_type) {
        this.project_type = project_type;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getProject_type_id() {
        return project_type_id;
    }

    public void setProject_type_id(String project_type_id) {
        this.project_type_id = project_type_id;
    }

    public String getProject_name_id() {
        return project_name_id;
    }

    public void setProject_name_id(String project_name_id) {
        this.project_name_id = project_name_id;
    }
}
