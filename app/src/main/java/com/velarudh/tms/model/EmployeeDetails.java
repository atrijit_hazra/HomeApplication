package com.velarudh.tms.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Atrijit on 25-05-2017.
 */

public class EmployeeDetails implements Parcelable{

    public String employee_id;
    public String employee_name;
    public String employee_email;
    public String employee_contact;
    public String employee_designation;
    public String employee_is_admin;
    public String employee_can_assign;

    public EmployeeDetails(){

    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getEmployee_email() {
        return employee_email;
    }

    public void setEmployee_email(String employee_email) {
        this.employee_email = employee_email;
    }

    public String getEmployee_contact() {
        return employee_contact;
    }

    public void setEmployee_contact(String employee_contact) {
        this.employee_contact = employee_contact;
    }

    public String getEmployee_designation() {
        return employee_designation;
    }

    public void setEmployee_designation(String employee_designation) {
        this.employee_designation = employee_designation;
    }

    public String getEmployee_is_admin() {
        return employee_is_admin;
    }

    public void setEmployee_is_admin(String employee_is_admin) {
        this.employee_is_admin = employee_is_admin;
    }

    public String getEmployee_can_assign() {
        return employee_can_assign;
    }

    public void setEmployee_can_assign(String employee_can_assign) {
        this.employee_can_assign = employee_can_assign;
    }

    protected EmployeeDetails(Parcel in) {
        employee_id = in.readString();
        employee_name = in.readString();
        employee_email = in.readString();
        employee_contact = in.readString();
        employee_designation = in.readString();
        employee_is_admin = in.readString();
        employee_can_assign = in.readString();
    }

    public static final Creator<EmployeeDetails> CREATOR = new Creator<EmployeeDetails>() {
        @Override
        public EmployeeDetails createFromParcel(Parcel in) {
            return new EmployeeDetails(in);
        }

        @Override
        public EmployeeDetails[] newArray(int size) {
            return new EmployeeDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(employee_id);
        parcel.writeString(employee_name);
        parcel.writeString(employee_email);
        parcel.writeString(employee_contact);
        parcel.writeString(employee_designation);
        parcel.writeString(employee_is_admin);
        parcel.writeString(employee_can_assign);
    }
}
