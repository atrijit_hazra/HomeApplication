package com.velarudh.tms.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Atrijit on 25-05-2017.
 */

public class ProjectDetails implements Parcelable {

    public String project_id;
    public String project_type_id;
    public String project_name;
    public String project_type;
    public String project_created_by;
    public String project_created_by_id;
    public String project_created_date;
    public String project_created_time;
    public String project_status;
    public String project_description;
    public String project_file;

    public ProjectDetails(){

    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getProject_type_id() {
        return project_type_id;
    }

    public void setProject_type_id(String project_type_id) {
        this.project_type_id = project_type_id;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getProject_type() {
        return project_type;
    }

    public void setProject_type(String project_type) {
        this.project_type = project_type;
    }

    public String getProject_created_by() {
        return project_created_by;
    }

    public void setProject_created_by(String project_created_by) {
        this.project_created_by = project_created_by;
    }

    public String getProject_created_by_id() {
        return project_created_by_id;
    }

    public void setProject_created_by_id(String project_created_by_id) {
        this.project_created_by_id = project_created_by_id;
    }

    public String getProject_created_date() {
        return project_created_date;
    }

    public void setProject_created_date(String project_created_date) {
        this.project_created_date = project_created_date;
    }

    public String getProject_created_time() {
        return project_created_time;
    }

    public void setProject_created_time(String project_created_time) {
        this.project_created_time = project_created_time;
    }

    public String getProject_status() {
        return project_status;
    }

    public void setProject_status(String project_status) {
        this.project_status = project_status;
    }

    public String getProject_description() {
        return project_description;
    }

    public void setProject_description(String project_description) {
        this.project_description = project_description;
    }

    public String getProject_file() {
        return project_file;
    }

    public void setProject_file(String project_file) {
        this.project_file = project_file;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    protected ProjectDetails(Parcel in) {
        project_id = in.readString();
        project_type_id = in.readString();
        project_name = in.readString();
        project_type = in.readString();
        project_created_by = in.readString();
        project_created_by_id = in.readString();
        project_created_date = in.readString();
        project_created_time = in.readString();
        project_status = in.readString();
        project_description = in.readString();
        project_file = in.readString();
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(project_id);
        parcel.writeString(project_type_id);
        parcel.writeString(project_name);
        parcel.writeString(project_type);
        parcel.writeString(project_created_by);
        parcel.writeString(project_created_by_id);
        parcel.writeString(project_created_date);
        parcel.writeString(project_created_time);
        parcel.writeString(project_status);
        parcel.writeString(project_description);
        parcel.writeString(project_file);
    }

    public static final Creator<ProjectDetails> CREATOR = new Creator<ProjectDetails>() {
        @Override
        public ProjectDetails createFromParcel(Parcel in) {
            return new ProjectDetails(in);
        }

        @Override
        public ProjectDetails[] newArray(int size) {
            return new ProjectDetails[size];
        }
    };
}
