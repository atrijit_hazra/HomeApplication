package com.velarudh.tms.model;

import java.util.ArrayList;

/**
 * Created by Atrijit on 11-09-2017.
 */

public class TaskTypeStatusList {

    ArrayList<String> task_priority_list;
    ArrayList<String> task_type_list;
    ArrayList<String> task_status_list;

    public ArrayList<String> getTask_priority_list() {

        task_priority_list = new ArrayList<>();

        task_priority_list.add("Select");
        task_priority_list.add("High");
        task_priority_list.add("Normal");
        task_priority_list.add("Medium");
        task_priority_list.add("Low");
        task_priority_list.add("Urgent");
        return task_priority_list;
    }

    public ArrayList<String> getTask_status_list() {

        task_status_list = new ArrayList<>();

        task_status_list.add("Select");
        task_status_list.add("Pending");
        task_status_list.add("Going On");
        task_status_list.add("Standing By");
        task_status_list.add("Newly Open");
        task_status_list.add("Completed");
        return task_status_list;
    }

    public ArrayList<String> getTask_type_list() {

        task_type_list = new ArrayList<>();

        task_type_list.add("Select");
        task_type_list.add("New Task");
        task_type_list.add("One Time Task");
        task_type_list.add("Repeat Task");
        task_type_list.add("Error");
        task_type_list.add("Course");
        return task_type_list;
    }
}
