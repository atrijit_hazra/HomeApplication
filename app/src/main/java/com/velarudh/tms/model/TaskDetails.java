package com.velarudh.tms.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Atrijit on 22-05-2017.
 */

public class TaskDetails implements Parcelable{

    public  String task_id ;
    public  String task_name ;
    public  String sub_task_name;
    public  String task_description ;
    public  String task_start_date;
    public  String task_end_date ;
    public  String task_assign_by ;
    public  String task_assign_to ;
    public  String task_assign_by_id ;
    public  String task_assign_to_id ;
    public  String task_priority;

    public  String task_type ;
    public  String task_status;
    public  String task_create_date;

    public  String project_id ;
    public  String project_type_id ;

    public  String project_name ;
    public  String project_description ;
    public  String project_created_by;
    public  String project_created_date;
    public  String project_status;
    public  String project_type ;
    public  String task_parent_id;

    public TaskDetails(){

    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getTask_name() {
        return task_name;
    }

    public String getSub_task_name() {
        return sub_task_name;
    }

    public void setSub_task_name(String sub_task_name) {
        this.sub_task_name = sub_task_name;
    }

    public void setTask_name(String task_name) {
        this.task_name = task_name;
    }

    public String getTask_description() {
        return task_description;
    }

    public void setTask_description(String task_description) {
        this.task_description = task_description;
    }

    public String getTask_start_date() {
        return task_start_date;
    }

    public void setTask_start_date(String task_start_date) {
        this.task_start_date = task_start_date;
    }

    public String getTask_end_date() {
        return task_end_date;
    }

    public void setTask_end_date(String task_end_date) {
        this.task_end_date = task_end_date;
    }

    public String getTask_assign_by() {
        return task_assign_by;
    }

    public void setTask_assign_by(String task_assign_by) {
        this.task_assign_by = task_assign_by;
    }

    public String getTask_assign_to() {
        return task_assign_to;
    }

    public void setTask_assign_to(String task_assign_to) {
        this.task_assign_to = task_assign_to;
    }

    public String getTask_assign_by_id() {
        return task_assign_by_id;
    }

    public void setTask_assign_by_id(String task_assign_by_id) {
        this.task_assign_by_id = task_assign_by_id;
    }

    public String getTask_assign_to_id() {
        return task_assign_to_id;
    }

    public void setTask_assign_to_id(String task_assign_to_id) {
        this.task_assign_to_id = task_assign_to_id;
    }

    public String getTask_type() {
        return task_type;
    }

    public void setTask_type(String task_type) {
        this.task_type = task_type;
    }

    public String getTask_status() {
        return task_status;
    }

    public void setTask_status(String task_status) {
        this.task_status = task_status;
    }

    public String getTask_create_date() {
        return task_create_date;
    }

    public void setTask_create_date(String task_create_date) {
        this.task_create_date = task_create_date;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getProject_type_id() {
        return project_type_id;
    }

    public void setProject_type_id(String project_type_id) {
        this.project_type_id = project_type_id;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getProject_description() {
        return project_description;
    }

    public void setProject_description(String project_description) {
        this.project_description = project_description;
    }

    public String getProject_created_by() {
        return project_created_by;
    }

    public void setProject_created_by(String project_created_by) {
        this.project_created_by = project_created_by;
    }

    public String getProject_status() {
        return project_status;
    }

    public void setProject_status(String project_status) {
        this.project_status = project_status;
    }

    public String getProject_type() {
        return project_type;
    }

    public void setProject_type(String project_type) {
        this.project_type = project_type;
    }

    public String getProject_created_date() {
        return project_created_date;
    }

    public void setProject_created_date(String project_created_date) {
        this.project_created_date = project_created_date;
    }

    public String getTask_parent_id() {
        return task_parent_id;
    }

    public void setTask_parent_id(String task_parent_id) {
        this.task_parent_id = task_parent_id;
    }

    public String getTask_priority() {
        return task_priority;
    }

    public void setTask_priority(String task_priority) {
        this.task_priority = task_priority;
    }

    protected TaskDetails(Parcel in) {
        task_id = in.readString();
        task_name = in.readString();
        task_description = in.readString();
        task_start_date = in.readString();
        task_end_date = in.readString();
        task_assign_by = in.readString();
        task_assign_to = in.readString();
        task_type = in.readString();
        task_status = in.readString();
        task_create_date = in.readString();
        project_name = in.readString();
        project_description = in.readString();
        project_created_by = in.readString();
        project_status = in.readString();
        project_type = in.readString();
        project_created_date = in.readString();
        task_parent_id = in.readString();
        task_priority = in.readString();
        sub_task_name = in.readString();
        project_id = in.readString();
        project_type_id= in.readString();
        task_assign_by_id= in.readString();
        task_assign_to_id= in.readString();
    }

    public static final Creator<TaskDetails> CREATOR = new Creator<TaskDetails>() {
        @Override
        public TaskDetails createFromParcel(Parcel in) {
            return new TaskDetails(in);
        }

        @Override
        public TaskDetails[] newArray(int size) {
            return new TaskDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(task_id);
        parcel.writeString(task_name);
        parcel.writeString(task_description);
        parcel.writeString(task_start_date);
        parcel.writeString(task_end_date);
        parcel.writeString(task_assign_by);
        parcel.writeString(task_assign_to);
        parcel.writeString(task_type);
        parcel.writeString(task_status);
        parcel.writeString(task_create_date);
        parcel.writeString(project_name);
        parcel.writeString(project_description);
        parcel.writeString(project_created_by);
        parcel.writeString(project_status);
        parcel.writeString(project_type);
        parcel.writeString(project_created_date);
        parcel.writeString(task_parent_id);
        parcel.writeString(task_priority);
        parcel.writeString(sub_task_name);
        parcel.writeString(project_id);
        parcel.writeString(project_type_id);
        parcel.writeString(task_assign_by_id);
        parcel.writeString(task_assign_to_id);
    }
}
