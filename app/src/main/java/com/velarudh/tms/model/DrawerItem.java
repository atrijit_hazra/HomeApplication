package com.velarudh.tms.model;

/**
 * Created by Atrijit on 19-05-2017.
 */

public class DrawerItem {

    private int icon;
    private String title;
    private int count;

    public DrawerItem()
    {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
