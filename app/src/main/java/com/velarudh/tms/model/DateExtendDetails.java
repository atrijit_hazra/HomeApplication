package com.velarudh.tms.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Atrijit on 01-06-2017.
 */

public class DateExtendDetails implements Parcelable{

    public String task_id;
    public String task_name;
    public String requested_by;
    public String end_date;
    public String requested_date;

    public DateExtendDetails(){

    }



    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getTask_name() {
        return task_name;
    }

    public void setTask_name(String task_name) {
        this.task_name = task_name;
    }

    public String getRequested_by() {
        return requested_by;
    }

    public void setRequested_by(String requested_by) {
        this.requested_by = requested_by;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getRequested_date() {
        return requested_date;
    }

    public void setRequested_date(String requested_date) {
        this.requested_date = requested_date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(task_id);
        parcel.writeString(task_name);
        parcel.writeString(requested_by);
        parcel.writeString(end_date);
        parcel.writeString(requested_date);
    }

    protected DateExtendDetails(Parcel in) {
        task_id = in.readString();
        task_name = in.readString();
        requested_by = in.readString();
        end_date = in.readString();
        requested_date = in.readString();
    }

    public static final Creator<DateExtendDetails> CREATOR = new Creator<DateExtendDetails>() {
        @Override
        public DateExtendDetails createFromParcel(Parcel in) {
            return new DateExtendDetails(in);
        }

        @Override
        public DateExtendDetails[] newArray(int size) {
            return new DateExtendDetails[size];
        }
    };
}
