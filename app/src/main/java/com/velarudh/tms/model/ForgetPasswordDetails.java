package com.velarudh.tms.model;

/**
 * Created by Atrijit on 08-02-2017.
 */

public class ForgetPasswordDetails {

    String status;
    String otp;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
