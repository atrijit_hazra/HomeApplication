package com.velarudh.tms.model;

/**
 * Created by Atrijit on 22-05-2017.
 */

public class TakProject_count {

    private String total_employee;
    private String total_project;
    private String total_incomplete_project;
    private String total_complete_project;
    private String total_task;
    private String total_incomplete_task;
    private String total_complete_task;
    private String my_total_task;
    private String my_total_complete_task;
    private String my_total_incomplete_task;
    private String my_project;
    private String total_date_request;
    private String total_task_complete_request;

    public TakProject_count(){


    }

    public String getTotal_employee() {
        return total_employee;
    }

    public void setTotal_employee(String total_employee) {
        this.total_employee = total_employee;
    }

    public String getTotal_project() {
        return total_project;
    }

    public void setTotal_project(String total_project) {
        this.total_project = total_project;
    }

    public String getTotal_incomplete_project() {
        return total_incomplete_project;
    }

    public void setTotal_incomplete_project(String total_incomplete_project) {
        this.total_incomplete_project = total_incomplete_project;
    }

    public String getTotal_complete_project() {
        return total_complete_project;
    }

    public void setTotal_complete_project(String total_complete_project) {
        this.total_complete_project = total_complete_project;
    }

    public String getTotal_task() {
        return total_task;
    }

    public void setTotal_task(String total_task) {
        this.total_task = total_task;
    }

    public String getTotal_incomplete_task() {
        return total_incomplete_task;
    }

    public void setTotal_incomplete_task(String total_incomplete_task) {
        this.total_incomplete_task = total_incomplete_task;
    }

    public String getTotal_complete_task() {
        return total_complete_task;
    }

    public void setTotal_complete_task(String total_complete_task) {
        this.total_complete_task = total_complete_task;
    }

    public String getMy_total_task() {
        return my_total_task;
    }

    public void setMy_total_task(String my_total_task) {
        this.my_total_task = my_total_task;
    }

    public String getMy_total_complete_task() {
        return my_total_complete_task;
    }

    public void setMy_total_complete_task(String my_total_complete_task) {
        this.my_total_complete_task = my_total_complete_task;
    }

    public String getMy_total_incomplete_task() {
        return my_total_incomplete_task;
    }

    public void setMy_total_incomplete_task(String my_total_incomplete_task) {
        this.my_total_incomplete_task = my_total_incomplete_task;
    }

    public String getMy_project() {
        return my_project;
    }

    public void setMy_project(String my_project) {
        this.my_project = my_project;
    }

    public String getTotal_date_request() {
        return total_date_request;
    }

    public void setTotal_date_request(String total_date_request) {
        this.total_date_request = total_date_request;
    }

    public String getTotal_task_complete_request() {
        return total_task_complete_request;
    }

    public void setTotal_task_complete_request(String total_task_complete_request) {
        this.total_task_complete_request = total_task_complete_request;
    }
}
