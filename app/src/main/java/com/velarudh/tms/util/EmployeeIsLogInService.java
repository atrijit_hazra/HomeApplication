package com.velarudh.tms.util;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by Atrijit on 21-07-2017.
 */

public class EmployeeIsLogInService extends Service {

    String TAG = "UberTrackingService";
    Timer timer;
    HttpGet httpGet;
    HttpClient httpClient;
    String URL = Url.BASE_URL + "is_user_logged_in.php?user_id=";

    UserSessionManager usm;
    HashMap<String, String> user_details;
    String U_EMP_ID = "user_id";
    String user_emp_id;

    @Override
    public void onCreate() {
        super.onCreate();

        usm = new UserSessionManager(this);

        user_details = usm.getUserDetails();
        user_emp_id = user_details.get(U_EMP_ID);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        URL = URL+user_emp_id;
        ComPareTime();
        return START_STICKY;
    }

    public void ComPareTime() {

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                IsUserLoggedIn();
            }
        };
        timer = new Timer();
        timer.schedule(timerTask, 2000, 10000);


    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void IsUserLoggedIn() {

        try {

            Log.d("", "IsUserLoggedIn: " + URL);
            StrictMode.ThreadPolicy policyy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

            StrictMode.setThreadPolicy(policyy);

            httpClient = new DefaultHttpClient();
            httpGet = new HttpGet(URL);
            httpGet.setHeader("Content-Type", "application/json");
            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            InputStream is = httpEntity.getContent();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
