package com.velarudh.tms.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.velarudh.tms.R;
import com.velarudh.tms.home.MainActivity;
import com.velarudh.tms.task.SeeYourAllTasks;

import static com.android.volley.VolleyLog.TAG;
import static com.velarudh.tms.R.id.swipe_layout;
import static com.velarudh.tms.R.id.view;

/**
 * Created by Atrijit on 09-06-2017.
 */

public class NetErrorActivity extends Activity {

    ConnectivityReceiver myReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_neterror);
        Button retry = (Button)findViewById(R.id.retry);

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d(TAG, "onClick: ");
                if(checkConnection())
                {
                    if(getIntent().getStringExtra("layout_flag").equalsIgnoreCase("0"))
                    {
                        Intent in = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(in);

                    }
                    else {
                        finish();
                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"No intenet",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected(getApplicationContext());
        return isConnected;
    }
}

