package com.velarudh.tms.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Set;

/**
 * Created by Atrijit on 18-04-2017.
 */

public class UserSessionManager {

    SharedPreferences pref;
    // Editor reference for Shared preferences
    SharedPreferences.Editor editor;


    Context _context;


    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREFER_NAME = "TMS_USERDETAILS";

    // All Shared Preferences Keys

    private static final String URL = "url";

    private static final String LOGIN_ID = "login_id";
    private static final String USER_EMAIL = "user_email";
    private static final String USER_NAME = "user_name";
    private static final String USER_PHNO = "user_phno";
    private static final String USER_DESIGNATION = "user_designation";

    private static final String USER_ID = "user_id";
    private static final String USER_IS_ADMIN = "user_is_admin";
    private static final String USER_CAN_ASSIGN = "user_can_assign";

    private static final String USER_PASSWORD= "user_password";
    private static final String USER_LOG_IN = "user_log_in";


    // Constructor
    public UserSessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

//
    public void setBaseUrl(String url)
    {
        editor.putString(URL, url);
        editor.commit();
    }
    public void setUserDetails(String login_id , String user_email , String user_password , String user_name , String user_phno , String user_designation , String user_id , String user_is_admin , String user_can_assign)
    {
        editor.putString(LOGIN_ID, login_id);
        editor.putString(USER_EMAIL, user_email);
        editor.putString(USER_NAME,user_name);
        editor.putString(USER_PASSWORD, user_password);
        editor.putString(USER_PHNO, user_phno);

        editor.putString(USER_DESIGNATION, user_designation);
        editor.putString(USER_ID, user_id);
        editor.putString(USER_IS_ADMIN, user_is_admin);
        editor.putString(USER_CAN_ASSIGN, user_can_assign);

        editor.commit();
    }

    public void setUserLogIn(boolean user_log_in){

        editor.putBoolean(USER_LOG_IN, user_log_in);
        editor.commit();
    }

    public void clearSession() {

        // Clearing all user data from Shared Preferences
        editor.clear();
        editor.commit();

    }

    public String getBaseUrl()
    {
        return pref.getString(URL,null);
    }

    public boolean isLogIn() {
        return pref.getBoolean(USER_LOG_IN, false);
    }

    public HashMap<String , String> getUserDetails()
    {
        HashMap<String , String> some = new HashMap<>();
//        return pref.getString(key, null);

        some.put(LOGIN_ID , pref.getString(LOGIN_ID,null));
        some.put(USER_EMAIL , pref.getString(USER_EMAIL,null));
        some.put(USER_NAME, pref.getString(USER_NAME,null));
        some.put(USER_PASSWORD , pref.getString(USER_PASSWORD,null));
        some.put(USER_PHNO , pref.getString(USER_PHNO,null));

        some.put(USER_DESIGNATION , pref.getString(USER_DESIGNATION,null));
        some.put(USER_ID , pref.getString(USER_ID,null));
        some.put(USER_IS_ADMIN , pref.getString(USER_IS_ADMIN,null));
        some.put(USER_CAN_ASSIGN , pref.getString(USER_CAN_ASSIGN,null));

        return  some;
    }


}
