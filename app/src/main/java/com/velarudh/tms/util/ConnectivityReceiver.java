package com.velarudh.tms.util;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.velarudh.tms.R;
import com.velarudh.tms.task.SeeYourAllTasks;

/**
 * Created by Atrijit on 09-06-2017.
 */


public class ConnectivityReceiver extends BroadcastReceiver {

    public ConnectivityReceiver() {
        super();
    }

    String TAG = "ConnectivityReceiver";
    
    @Override
    public void onReceive(final Context context, Intent arg1) {

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        final boolean isConnected = activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();

        Log.d(TAG, "onReceive: "+isConnected);
        if (!isConnected) {
            Log.d(TAG, "onReceive:    enter");
            Intent in = new Intent(context, NetErrorActivity.class);
            in.putExtra("layout_flag","1");
            in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(in);
        }
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager
                cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
    }

}