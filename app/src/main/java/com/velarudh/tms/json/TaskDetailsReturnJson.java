package com.velarudh.tms.json;

import android.util.Log;

import com.velarudh.tms.model.TakProject_count;
import com.velarudh.tms.model.TaskDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Atrijit on 23-05-2017.
 */

public class TaskDetailsReturnJson {

    public static final String TASK_ID = "task_id";
    public static final String TASK_NAME = "task_name";
    public static final String SUBTASK_PARENT_ID = "subtask_parent_id";
    public static final String SUBTASK_PARENT_NAME = "subtask_parent_name";
    public static final String TASK_DESCRIPTION = "task_description";
    public static final String TASK_START_DATE = "task_start_date";
    public static final String TASK_END_DATE = "task_end_date";
    public static final String ASSIGN_BY = "assign_by";
    public static final String ASSIGN_TO = "assign_to";
    public static final String ASSIGN_BY_ID = "assign_by_id";
    public static final String ASSIGN_TO_ID = "assign_to_id";
    public static final String TASK_PRIORITY = "task_priority";
    public static final String TASK_TYPE = "task_type";
    public static final String TASK_STATUS = "task_status";
    public static final String TASK_CREATE_DATE = "task_create_date";

    public static final String PROJECT_NAME = "project_name";
    public static final String PROJECT_DESCRIPTION = "project_description";
    public static final String PROJECT_CREATED_BY = "project_created_by";
    public static final String PROJECT_CREATED_DATE = "project_created_date";
    public static final String PROJECT_STATUS = "project_status";
    public static final String PROJECT_TYPE = "project_type";
    public static final String PROJECT_ID = "project_id";
    public static final String PROJECT_TYPE_ID = "project_type_id";


    private String json;

    public ArrayList<TaskDetails> data;

    public TaskDetailsReturnJson(String json) {
        this.json = json;
    }

    public ArrayList<TaskDetails> TaskDetailsReturnJson() {

        data = new ArrayList<TaskDetails>();

        try {

            JSONArray jsonArray = new JSONArray(json);

            for(int i = 0 ; i<jsonArray.length() ; i++)
            {
                TaskDetails taskDetails = new TaskDetails();

                JSONObject jsonobject = jsonArray.getJSONObject(i);

                Log.d("TaskDetailsReturnJson", "TaskDetailsReturnJson: " + jsonobject);

                taskDetails.setTask_id(jsonobject.getString(TASK_ID).trim());
                taskDetails.setTask_name(jsonobject.getString(TASK_NAME).trim());
                taskDetails.setTask_parent_id(jsonobject.getString(SUBTASK_PARENT_ID).trim());
                taskDetails.setSub_task_name(jsonobject.getString(SUBTASK_PARENT_NAME).trim());
                taskDetails.setTask_description(jsonobject.getString(TASK_DESCRIPTION).trim());
                taskDetails.setTask_start_date(jsonobject.getString(TASK_START_DATE).trim());
                taskDetails.setTask_end_date(jsonobject.getString(TASK_END_DATE).trim());
                taskDetails.setTask_assign_by(jsonobject.getString(ASSIGN_BY).trim());
                taskDetails.setTask_assign_to(jsonobject.getString(ASSIGN_TO).trim());
                taskDetails.setTask_assign_by_id(jsonobject.getString(ASSIGN_BY_ID).trim());
                taskDetails.setTask_assign_to_id(jsonobject.getString(ASSIGN_TO_ID).trim());
                taskDetails.setTask_priority(jsonobject.getString(TASK_PRIORITY).trim());
                taskDetails.setTask_type(jsonobject.getString(TASK_TYPE).trim());
                taskDetails.setTask_status(jsonobject.getString(TASK_STATUS).trim());
                taskDetails.setTask_create_date(jsonobject.getString(TASK_CREATE_DATE).trim());

                taskDetails.setProject_id(jsonobject.getString(PROJECT_ID).trim());
                taskDetails.setProject_type_id(jsonobject.getString(PROJECT_TYPE_ID).trim());
                taskDetails.setProject_name(jsonobject.getString(PROJECT_NAME).trim());
                taskDetails.setProject_description(jsonobject.getString(PROJECT_DESCRIPTION).trim());
                taskDetails.setProject_created_by(jsonobject.getString(PROJECT_CREATED_BY).trim());
                taskDetails.setProject_created_date(jsonobject.getString(PROJECT_CREATED_DATE).trim());
                taskDetails.setProject_status(jsonobject.getString(PROJECT_STATUS).trim());
                taskDetails.setProject_type(jsonobject.getString(PROJECT_TYPE).trim());

                data.add(taskDetails);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

}
