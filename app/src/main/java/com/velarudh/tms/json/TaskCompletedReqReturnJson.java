package com.velarudh.tms.json;

import android.util.Log;

import com.velarudh.tms.model.TaskCompleted;
import com.velarudh.tms.model.TaskDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Atrijit on 07-06-2017.
 */

public class TaskCompletedReqReturnJson {

    public static final String TASK_ID = "task_id";
    public static final String TASK_NAME = "task_name";
    public static final String SUBTASK_PARENT_ID = "subtask_parent_id";
    public static final String SUBTASK_PARENT_NAME = "subtask_parent_name";
    public static final String TASK_DESCRIPTION = "task_description";
    public static final String TASK_START_DATE = "task_start_date";
    public static final String TASK_END_DATE = "task_end_date";
    public static final String ASSIGN_BY = "assign_by";
    public static final String ASSIGN_TO = "assign_to";
    public static final String ASSIGN_BY_ID = "assign_by_id";
    public static final String ASSIGN_TO_ID = "assign_to_id";
    public static final String TASK_PRIORITY = "task_priority";
    public static final String TASK_TYPE = "task_type";
    public static final String TASK_STATUS = "task_status";
    public static final String TASK_CREATE_DATE = "task_create_date";

    public static final String PROJECT_NAME = "project_name";
    public static final String PROJECT_DESCRIPTION = "project_description";
    public static final String PROJECT_CREATED_BY = "project_created_by";
    public static final String PROJECT_CREATED_DATE = "project_created_date";
    public static final String PROJECT_STATUS = "project_status";
    public static final String PROJECT_TYPE = "project_type";
    public static final String PROJECT_ID = "project_id";
    public static final String PROJECT_TYPE_ID = "project_type_id";
    public static final String COMPLETED_HOUR = "completed_hr";

    private String json;

    public ArrayList<TaskCompleted> data;

    public TaskCompletedReqReturnJson(String json) {
        this.json = json;
    }

    public ArrayList<TaskCompleted> TaskCompletedReqReturnJson() {

        data = new ArrayList<TaskCompleted>();

        try {

            JSONArray jsonArray = new JSONArray(json);

            for(int i = 0 ; i<jsonArray.length() ; i++)
            {
                TaskCompleted taskCompleted = new TaskCompleted();

                JSONObject jsonobject = jsonArray.getJSONObject(i);

                Log.d("TeaskCompletedJson", "TaskCompletedReqReturnJson: " + jsonobject);

                taskCompleted.setTask_id(jsonobject.getString(TASK_ID).trim());
                taskCompleted.setTask_name(jsonobject.getString(TASK_NAME).trim());
                taskCompleted.setTask_parent_id(jsonobject.getString(SUBTASK_PARENT_ID).trim());
                taskCompleted.setSub_task_name(jsonobject.getString(SUBTASK_PARENT_NAME).trim());
                taskCompleted.setTask_description(jsonobject.getString(TASK_DESCRIPTION).trim());
                taskCompleted.setTask_start_date(jsonobject.getString(TASK_START_DATE).trim());
                taskCompleted.setTask_end_date(jsonobject.getString(TASK_END_DATE).trim());
                taskCompleted.setTask_assign_by(jsonobject.getString(ASSIGN_BY).trim());
                taskCompleted.setTask_assign_to(jsonobject.getString(ASSIGN_TO).trim());
                taskCompleted.setTask_assign_by_id(jsonobject.getString(ASSIGN_BY_ID).trim());
                taskCompleted.setTask_assign_to_id(jsonobject.getString(ASSIGN_TO_ID).trim());
                taskCompleted.setTask_priority(jsonobject.getString(TASK_PRIORITY).trim());
                taskCompleted.setTask_type(jsonobject.getString(TASK_TYPE).trim());
                taskCompleted.setTask_status(jsonobject.getString(TASK_STATUS).trim());
                taskCompleted.setTask_create_date(jsonobject.getString(TASK_CREATE_DATE).trim());

                taskCompleted.setProject_id(jsonobject.getString(PROJECT_ID).trim());
//                taskDetails.setProject_type_id(jsonobject.getString(PROJECT_TYPE_ID).trim());
                taskCompleted.setProject_name(jsonobject.getString(PROJECT_NAME).trim());
                taskCompleted.setProject_description(jsonobject.getString(PROJECT_DESCRIPTION).trim());
                taskCompleted.setProject_created_by(jsonobject.getString(PROJECT_CREATED_BY).trim());
                taskCompleted.setProject_created_date(jsonobject.getString(PROJECT_CREATED_DATE).trim());
                taskCompleted.setProject_status(jsonobject.getString(PROJECT_STATUS).trim());
//                taskDetails.setProject_type(jsonobject.getString(PROJECT_TYPE).trim());
                taskCompleted.setCompleted_hour(jsonobject.getString(COMPLETED_HOUR).trim());

                data.add(taskCompleted);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }
}
