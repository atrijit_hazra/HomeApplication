package com.velarudh.tms.json;

import android.content.Context;
import android.util.Log;

import com.velarudh.tms.model.TakProject_count;
import com.velarudh.tms.util.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by Atrijit on 26-05-2017.
 */

public class LogInReturnJson {

    public static final String EMAIL_ID = "emp_email";
    public static final String USER_NAME = "emp_name";
    public static final String USER_PHNO = "emp_phno";
    public static final String USER_DESIGNATION = "emp_designation";
    public static final String USER_ID = "emp_id";
    public static final String IS_ADMIN = "is_admin";
    public static final String CAN_ASSIGN = "can_assign";

    private String json;

    UserSessionManager usm;

    Context context;

    String login_id;
    String user_email;
    String user_password;
    String user_name;
    String user_phno;
    String user_designation;
    String user_id;
    String is_admin;
    String can_assign;


    public LogInReturnJson(String json, Context context, String login_id, String user_password) {
        this.json = json;
        this.context = context;
        this.login_id = login_id;
        this.user_password = user_password;

    }

    public boolean LogInReturnJson() {

        usm = new UserSessionManager(context);

        boolean valid = false;

        try
        {
            JSONArray jsonArray = new JSONArray(json);

            for(int i =0;i<jsonArray.length();i++) {

                JSONObject jsonobject =jsonArray.getJSONObject(i);

                user_email = jsonobject.getString(EMAIL_ID);
                user_name = jsonobject.getString(USER_NAME).trim();
                user_phno = jsonobject.getString(USER_PHNO).trim();
                user_designation = jsonobject.getString(USER_DESIGNATION).trim();
                user_id = jsonobject.getString(USER_ID).trim();
                is_admin = jsonobject.getString(IS_ADMIN).trim();
                can_assign = jsonobject.getString(CAN_ASSIGN).trim();

                usm.setUserDetails(login_id,user_email, user_password, user_name, user_phno, user_designation, user_id, is_admin, can_assign);

                valid = true;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return valid;
    }

}
