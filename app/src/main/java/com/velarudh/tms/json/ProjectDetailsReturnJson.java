package com.velarudh.tms.json;

import android.util.Log;

import com.velarudh.tms.model.EmployeeDetails;
import com.velarudh.tms.model.ProjectDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Atrijit on 25-05-2017.
 */

public class ProjectDetailsReturnJson {

    public static final String PROJECT_ID = "project_id";
    public static final String PROJECT_NAME = "project_name";
    public static final String PROJECT_TYPE = "project_type";
    public static final String PROJECT_TYPE_ID = "project_type_id";
    public static final String PROJECT_CREATED_BY = "project_created_by";
    public static final String PROJECT_CREATED_BY_ID = "project_created_by_id";
    public static final String PROJECT_CREATED_DATE = "project_create_date";
    public static final String PROJECT_STATUS = "project_status";
    public static final String PROJECT_DESCRIPTION = "project_description";
    public static final String PROJECT_FILE = "project_file";

    private String json;

    public ArrayList<ProjectDetails> data;

    public ProjectDetailsReturnJson(String json) {
        this.json = json;
    }

    public ArrayList<ProjectDetails> ProjectDetailsReturnJson() {

        data = new ArrayList<ProjectDetails>();

        try {

            JSONArray jsonArray = new JSONArray(json);

            for(int i = 0 ; i<jsonArray.length() ; i++)
            {
                ProjectDetails projectDetails = new ProjectDetails();

                JSONObject jsonobject = jsonArray.getJSONObject(i);

                Log.d("ProjectDetailturnJson", "ProjectDetlsReturnJson: " + jsonobject);

                projectDetails.setProject_id(jsonobject.getString(PROJECT_ID).trim());
                projectDetails.setProject_name(jsonobject.getString(PROJECT_NAME).trim());
                projectDetails.setProject_type(jsonobject.getString(PROJECT_TYPE).trim());
                projectDetails.setProject_type_id(jsonobject.getString(PROJECT_TYPE_ID).trim());
                projectDetails.setProject_created_by(jsonobject.getString(PROJECT_CREATED_BY).trim());
                projectDetails.setProject_created_by_id(jsonobject.getString(PROJECT_CREATED_BY_ID).trim());
                projectDetails.setProject_created_date(OnlyDate(jsonobject.getString(PROJECT_CREATED_DATE).trim()));
                projectDetails.setProject_created_time(OnlyTime(jsonobject.getString(PROJECT_CREATED_DATE).trim()));
                projectDetails.setProject_status(jsonobject.getString(PROJECT_STATUS).trim());
                projectDetails.setProject_description(jsonobject.getString(PROJECT_DESCRIPTION).trim());
                projectDetails.setProject_file(jsonobject.getString(PROJECT_FILE).trim());


                data.add(projectDetails);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    public String OnlyDate(String date) {

        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss a", Locale.ENGLISH);

        Date date1 = null;
        try {
            date1 = myFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy/MM/dd");

        return newFormat.format(date1);

    }

    public String OnlyTime(String date) {

        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss a",Locale.ENGLISH);

        Date date1 = null;
        try {
            date1 = myFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat newFormat = new SimpleDateFormat("HH:mm:ss a");

        return newFormat.format(date1);

    }

}
