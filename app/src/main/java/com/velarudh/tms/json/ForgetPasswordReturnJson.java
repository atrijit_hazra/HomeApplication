package com.velarudh.tms.json;

import android.util.Log;

import com.velarudh.tms.model.ForgetPasswordDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Atrijit on 08-02-2017.
 */

public class ForgetPasswordReturnJson {

    public static final String KEY_STATUS = "status";
    public static final String KEY_OTP = "otp";

    private String json;

    public ArrayList<ForgetPasswordDetails> user;

    public ForgetPasswordReturnJson(String json){
        this.json = json;
    }

    public ArrayList<ForgetPasswordDetails> ForgetPasswordReturnJson(){

        try
        {
            user = new ArrayList<ForgetPasswordDetails>();

            JSONArray jsonarray = new JSONArray(json);

            for (int i = 0; i < jsonarray.length(); i++){

                JSONObject jsonObject = jsonarray.getJSONObject(i);

                ForgetPasswordDetails forget_details = new ForgetPasswordDetails();
                forget_details.setStatus(jsonObject.getString(KEY_STATUS).trim());

                if(forget_details.getStatus().equalsIgnoreCase("1")) {
                    forget_details.setOtp(jsonObject.getString(KEY_OTP).trim());
                }
                user.add(forget_details);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }
}
