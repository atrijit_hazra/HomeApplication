package com.velarudh.tms.json;

import android.content.Context;
import android.util.Log;

import com.velarudh.tms.model.EmployeeDetails;
import com.velarudh.tms.model.TaskDetails;
import com.velarudh.tms.util.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Atrijit on 25-05-2017.
 */

public class EmployeeDetailsReturnJson {

    public static final String EMP_ID = "emp_id";
    public static final String EMP_NAME = "emp_name";
    public static final String EMP_EMAIL = "emp_email";
    public static final String EMP_PHNO = "emp_phno";
    public static final String EMP_DESIGNATION = "emp_designation";
    public static final String IS_ADMIN = "is_admin";
    public static final String CAN_ASSIGN = "can_assign";

    Context context;

    private String json;

    public ArrayList<EmployeeDetails> data;

    public EmployeeDetailsReturnJson(String json, Context context) {
        this.json = json;
        this.context = context;
    }

    UserSessionManager usm;

    HashMap<String, String> user_details;
    String U_EMP_ID = "user_id";
    String user_emp_id;


    public ArrayList<EmployeeDetails> EmployeeDetailsReturnJson() {

        data = new ArrayList<EmployeeDetails>();

        usm = new UserSessionManager(context);
        user_details = usm.getUserDetails();
        user_emp_id = user_details.get(U_EMP_ID);


        try {

            JSONArray jsonArray = new JSONArray(json);

            for (int i = 0; i < jsonArray.length(); i++) {
                EmployeeDetails employeeDetails = new EmployeeDetails();

                JSONObject jsonobject = jsonArray.getJSONObject(i);

                employeeDetails.setEmployee_id(jsonobject.getString(EMP_ID).trim());
                employeeDetails.setEmployee_name(jsonobject.getString(EMP_NAME).trim());
                employeeDetails.setEmployee_email(jsonobject.getString(EMP_EMAIL).trim());
                employeeDetails.setEmployee_contact(jsonobject.getString(EMP_PHNO).trim());
                employeeDetails.setEmployee_designation(jsonobject.getString(EMP_DESIGNATION).trim());
                employeeDetails.setEmployee_is_admin(jsonobject.getString(IS_ADMIN).trim());
                employeeDetails.setEmployee_can_assign(jsonobject.getString(CAN_ASSIGN).trim());
                data.add(employeeDetails);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

}
