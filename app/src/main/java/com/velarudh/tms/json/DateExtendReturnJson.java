package com.velarudh.tms.json;

import android.util.Log;

import com.velarudh.tms.model.DateExtendDetails;
import com.velarudh.tms.model.ProjectDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Atrijit on 01-06-2017.
 */

public class DateExtendReturnJson {

    public static final String TASK_ID = "task_id";
    public static final String TASK_NAME = "task_name";
    public static final String REQUESTED_BY = "emp_name";
    public static final String END_DATE = "original_end_date";
    public static final String REQUESTED_DATE = "request_end_date";

    private String json;

    public ArrayList<DateExtendDetails> data;

    public DateExtendReturnJson(String json) {
        this.json = json;
    }

    public ArrayList<DateExtendDetails> DateExtendReturnJson() {

        data = new ArrayList<DateExtendDetails>();

        try {

            JSONArray jsonArray = new JSONArray(json);

            for(int i = 0 ; i<jsonArray.length() ; i++)
            {
                DateExtendDetails dateExtendDetails = new DateExtendDetails();

                JSONObject jsonobject = jsonArray.getJSONObject(i);

                Log.d("ProjectDetailturnJson", "ProjectDetlsReturnJson: " + jsonobject);

                dateExtendDetails.setTask_id(jsonobject.getString(TASK_ID).trim());
                dateExtendDetails.setTask_name(jsonobject.getString(TASK_NAME).trim());
                dateExtendDetails.setRequested_by(jsonobject.getString(REQUESTED_BY).trim());
                dateExtendDetails.setEnd_date(jsonobject.getString(END_DATE).trim());
                dateExtendDetails.setRequested_date(jsonobject.getString(REQUESTED_DATE).trim());


                data.add(dateExtendDetails);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

}
