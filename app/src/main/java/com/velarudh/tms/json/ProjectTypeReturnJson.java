package com.velarudh.tms.json;

import android.util.Log;

import com.velarudh.tms.model.ProjectDetails;
import com.velarudh.tms.model.ProjectType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Atrijit on 01-06-2017.
 */

public class ProjectTypeReturnJson {

    public static final String PROJECT_TYPE = "project_type";
    public static final String PROJECT_TYPE_ID = "project_type_id";

    private String json;

    public ArrayList<ProjectType> data;

    public ProjectTypeReturnJson(String json) {
        this.json = json;
    }

    public ArrayList<ProjectType> ProjectTypeReturnJson() {

        data = new ArrayList<ProjectType>();

        try {

            JSONArray jsonArray = new JSONArray(json);

            for (int i = 0; i < jsonArray.length(); i++) {
                ProjectType projectType = new ProjectType();

                JSONObject jsonobject = jsonArray.getJSONObject(i);

                Log.d("ProjectType", "ProjectType: " + jsonobject);

                projectType.setProject_type(jsonobject.getString(PROJECT_TYPE).trim());
                projectType.setProject_type_id(jsonobject.getString(PROJECT_TYPE_ID).trim());

                data.add(projectType);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    public String OnlyDate(String date) {

        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss a");

        Date date1 = null;
        try {
            date1 = myFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy/MM/dd");

        return newFormat.format(date1);

    }

    public String OnlyTime(String date) {

        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss a");

        Date date1 = null;
        try {
            date1 = myFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat newFormat = new SimpleDateFormat("HH:mm:ss a");

        return newFormat.format(date1);

    }

}

