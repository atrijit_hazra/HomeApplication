package com.velarudh.tms.json;

import android.util.Log;

import com.velarudh.tms.model.TakProject_count;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Atrijit on 22-05-2017.
 */

public class TaskProject_CountReturnJson {

    public static final String TOTAL_EMPLOYEE = "total_employee";

    public static final String TOTAL_PROJECT = "total_project";
    public static final String TOTAL_INCOMPLETE_PROJECT = "total_incomplete_project";
    public static final String TOTAL_COMPLETE_PROJECT = "total_complete_project";

    public static final String TOTAL_TASK = "total_task";
    public static final String TOTAL_INCOMPLETE_TASK = "total_incomplete_task";
    public static final String TOTAL_COMPLETE_TASK = "total_complete_task";

    public static final String MY_TOTAL_TASK = "my_total_task";
    public static final String MY_TOTAL_INCOMPLETE_TASK = "my_total_incomplete_task";
    public static final String MY_TOTAL_COMPLETE_TASK = "my_total_complete_task";

    public static final String MY_PROJECT = "my_project";

    public static final String TOTAL_DATE_REQUEST = "total_date_request";
    public static final String TOTAL_TASK_COMPLETE_REQUEST = "total_task_complete_request";

    private String json;

    public ArrayList<TakProject_count> data;

    public TaskProject_CountReturnJson(String json) {
        this.json = json;
    }

    public ArrayList<TakProject_count> TaskProject_CountReturnJson() {

        data = new ArrayList<TakProject_count>();

        try {

            TakProject_count tpc = new TakProject_count();

            JSONObject jsonobject = new JSONObject(json);

            Log.d("TaskProject_CountReturn", "TaskProject_CountReturnJson: " + jsonobject);

            tpc.setTotal_employee(jsonobject.getString(TOTAL_EMPLOYEE).trim());

            tpc.setTotal_project(jsonobject.getString(TOTAL_PROJECT).trim());
            tpc.setTotal_incomplete_project(jsonobject.getString(TOTAL_INCOMPLETE_PROJECT).trim());
            tpc.setTotal_complete_project(jsonobject.getString(TOTAL_COMPLETE_PROJECT).trim());

            tpc.setTotal_task(jsonobject.getString(TOTAL_TASK).trim());
            tpc.setTotal_incomplete_task(jsonobject.getString(TOTAL_INCOMPLETE_TASK).trim());
            tpc.setTotal_complete_task(jsonobject.getString(TOTAL_COMPLETE_TASK).trim());

            tpc.setMy_total_task(jsonobject.getString(MY_TOTAL_TASK).trim());
            tpc.setMy_total_complete_task(jsonobject.getString(MY_TOTAL_COMPLETE_TASK).trim());
            tpc.setMy_total_incomplete_task(jsonobject.getString(MY_TOTAL_INCOMPLETE_TASK).trim());

            tpc.setMy_project(jsonobject.getString(MY_PROJECT).trim());

            tpc.setTotal_date_request(jsonobject.getString(TOTAL_DATE_REQUEST).trim());
            tpc.setTotal_task_complete_request(jsonobject.getString(TOTAL_TASK_COMPLETE_REQUEST).trim());

            data.add(tpc);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

}
