package com.velarudh.tms.project;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.velarudh.tms.R;
import com.velarudh.tms.adapter.Manage_AllProject_adapter;
import com.velarudh.tms.json.ProjectDetailsReturnJson;
import com.velarudh.tms.main.Dashboard;
import com.velarudh.tms.model.ProjectDetails;
import com.velarudh.tms.util.ConnectivityReceiver;
import com.velarudh.tms.util.NetErrorActivity;
import com.velarudh.tms.util.Url;
import com.velarudh.tms.util.UserSessionManager;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

public class ManageAllProject extends AppCompatActivity {

    String TAG = "ManageAllTask";

    UserSessionManager usm;
    HashMap<String, String> user_details;
    String U_EMP_ID = "user_id";
    String user_emp_id;

    ArrayList<ProjectDetails> project_detail;

    Toolbar toolbar;
    SwipeRefreshLayout swipe_layout;
    RecyclerView managetask_project_recycler;
    TextView no_task;
    ConnectivityReceiver myReceiver;

    String BASE_URL = Url.BASE_URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_all_project);

        if (checkConnection()) {
            init();
            getProjectDetails();
            swipe_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getProjectDetails();
                }
            });
        } else {
            Intent in = new Intent(getApplicationContext(), NetErrorActivity.class);
            in.putExtra("layout_flag", "1");
            startActivity(in);
        }
    }

    public void init() {
        usm = new UserSessionManager(this);
        user_details = usm.getUserDetails();
        user_emp_id = user_details.get(U_EMP_ID);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.title_activity_manage_all_project);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        swipe_layout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        managetask_project_recycler = (RecyclerView) findViewById(R.id.manageall_project_recycler);
        no_task = (TextView) findViewById(R.id.no_task);

        Typeface Bahu_thin = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_thin.ttf");
        no_task.setTypeface(Bahu_thin);

    }

    public void getProjectDetails() {

        String JSON_URL_TOKEN_PROJECT_DETAILS = BASE_URL + "manage_project_list.php?user_id="+user_emp_id;
        Log.d(TAG, "getProjectDetails: " + JSON_URL_TOKEN_PROJECT_DETAILS);
        try {
            // Volley's json array request object
            JsonArrayRequest req2 = new JsonArrayRequest(JSON_URL_TOKEN_PROJECT_DETAILS,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d("getProjectDetails", response.toString());
                            try {
                                ProjectDetailsReturnJson pds = new ProjectDetailsReturnJson(response.toString());
                                project_detail = pds.ProjectDetailsReturnJson();
                                swipe_layout.setRefreshing(false);
                                setData();

                            } catch (Exception e) {
                                Log.d("getProjectDetails", "onResponse: " + e);
                                swipe_layout.setRefreshing(false);
                            }
                        }

                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("getProjectDetails", "Server Error: " + error.getMessage());
                            swipe_layout.setRefreshing(false);
                        }
                    });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            int socketTimeout = 600000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req2.setRetryPolicy(policy);
            requestQueue.add(req2);
        } catch (Exception e) {
            Log.d("getProjectDetails", "exception: " + e);
            swipe_layout.setRefreshing(false);
        }
    }

    public void setData() {

        if (project_detail.isEmpty()) {
            no_task.setVisibility(View.VISIBLE);
            managetask_project_recycler.setVisibility(View.GONE);
        } else {
            no_task.setVisibility(View.GONE);
            managetask_project_recycler.setVisibility(View.VISIBLE);
            Manage_AllProject_adapter dash_adapter = new Manage_AllProject_adapter(this, project_detail, user_emp_id);
            managetask_project_recycler.setAdapter(dash_adapter);
            GridLayoutManager gm = new GridLayoutManager(this,2);
            managetask_project_recycler.setLayoutManager(gm);
            managetask_project_recycler.setNestedScrollingEnabled(false);

            DividerItemDecoration horizontalDecoration = new DividerItemDecoration(managetask_project_recycler.getContext(),
                    DividerItemDecoration.VERTICAL);
            Drawable horizontalDivider = ContextCompat.getDrawable(this, R.drawable.recycler_decoration);
            horizontalDecoration.setDrawable(horizontalDivider);

            managetask_project_recycler.addItemDecoration(horizontalDecoration);
        }

    }

    public void ReloadData() {
        getProjectDetails();
    }

    @Override
    protected void onResume() {
        super.onResume();
        myReceiver = new ConnectivityReceiver();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(myReceiver);
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected(getApplicationContext());
        return isConnected;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent in = new Intent(this, Dashboard.class);
//        startActivity(in);
//        finish();
//        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}
