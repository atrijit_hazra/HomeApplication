package com.velarudh.tms.main;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.velarudh.tms.adapter.Dashboard_task_project_adapter;
import com.velarudh.tms.adapter.DrawerItem_adapter;
import com.velarudh.tms.calendar.Task_calendar;
import com.velarudh.tms.firebase.FireBaseSessionManager;
import com.velarudh.tms.home.MainActivity;
import com.velarudh.tms.json.ProjectDetailsReturnJson;
import com.velarudh.tms.json.TaskDetailsReturnJson;
import com.velarudh.tms.json.TaskProject_CountReturnJson;
import com.velarudh.tms.model.DrawerItem;
import com.velarudh.tms.R;
import com.velarudh.tms.model.ProjectDetails;
import com.velarudh.tms.model.ProjectType;
import com.velarudh.tms.model.TakProject_count;
import com.velarudh.tms.model.TaskDetails;
import com.velarudh.tms.project.ManageAllProject;
import com.velarudh.tms.task.DateExtend;
import com.velarudh.tms.task.ManageAllTask;
import com.velarudh.tms.task.NewTask_SubTask;
import com.velarudh.tms.task.SeeYourAllTasks;
import com.velarudh.tms.task.SeeYourAllTasksTree;
import com.velarudh.tms.task.Task_Completed;
import com.velarudh.tms.util.CircularTextView;
import com.velarudh.tms.util.ConnectivityReceiver;
import com.velarudh.tms.util.NetErrorActivity;
import com.velarudh.tms.util.Url;
import com.velarudh.tms.util.UserSessionManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipart;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;

import static android.graphics.Color.WHITE;

public class Dashboard extends Activity {

    String TAG = "DASHBOARD";

    DrawerLayout mDrawerLayout = null;
    Toolbar toolbar;
    TextView header_user_name;
    TextView header_user_ph;

    ArrayList<DrawerItem> drawer_item;
    ArrayList<String> item_name;
    ArrayList<Integer> item_icon;
    ArrayList<Integer> date_complete_task_count;

    RecyclerView draweritem_recycler;
    DrawerItem_adapter adapter;

    RelativeLayout content_dashboard;

    CardView layout_user;
    LinearLayout layout_total_project;
    RelativeLayout layout_your_project;
    RelativeLayout layout_total_task;
    RelativeLayout layout_your_task;
    View project_view;

    TextView tuser;
    TextView tproject;
    TextView mproject;
    TextView ttask;
    TextView mtask;

    SwipeRefreshLayout swipe_layout;
    TextView count_user;
    TextView count_total_project;
    TextView count_my_project;
    TextView count_total_task;
    TextView count_my_task;

    TextView count_total_complete_project;
    TextView count_total_incomplete_project;
    TextView count_total_complete_task;
    TextView count_total_incomplete_task;
    TextView count_my_complete_task;
    TextView count_my_incomplete_task;
    TextView my_incomplete_project;
    TextView my_complete_project;


    RecyclerView task_project_recycler;
    TextView no_task;
    Button see_all_tree;
    Button see_all;

    ArrayList<TakProject_count> task_project_count;
    ArrayList<TaskDetails> task_details;

    UserSessionManager usm;
    String U_NAME = "user_name";
    String U_IS_ADMIN = "user_is_admin";
    String U_CAN_ASSIGN = "user_can_assign";
    String U_PHNO = "user_phno";
    String U_EMP_ID = "user_id";
    HashMap<String, String> user_details;

    String user_name;
    String user_is_admin;
    String user_can_assign;
    String user_ph;
    String user_emp_id;

    Dialog dialog_create_project = null;
    AlertDialog dialog2 = null;
    String JSON_URL_TOKEN_NEW_PROJECT;
    ArrayList<ProjectDetails> project_detail;
    ArrayList<String> project_type_list;
    ArrayList<String> project_type_id_list;
    String create_project_name;
    String create_project_type_id = "0";
    String project_desc;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss a");
    String current_date_time;

    TakProject_count count_details;

    Spinner project_type;
    EditText project_name;
    EditText details_project;
    Button choose_document;
    TextView document_name;
    Button submit;
    Button cancel;

    int REQUEST_READ = 01;

    private static final int PICK_FILE_REQUEST = 1;
    String selectedFilePath;
    String selectedFileName;
    File choseToSend;

    ConnectivityReceiver myReceiver;

    String BASE_URL = Url.BASE_URL;

    Typeface Arial;
    Typeface Bahu_bold;
    Typeface Bahu_thin;

    ProgressDialog progressDialog;

    private Boolean exit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        init();

        if (checkConnection()) {

            FirebaseApp.initializeApp(getApplicationContext());

            getTaskProjectCount();

            swipe_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getTaskProjectCount();
                }
            });

            see_all.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent in = new Intent(getApplicationContext(), SeeYourAllTasks.class);
                    in.putParcelableArrayListExtra("task_details", task_details);
                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(in);
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                }
            });

            see_all_tree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(getApplicationContext(), SeeYourAllTasksTree.class);
                    in.putParcelableArrayListExtra("task_details", task_details);
                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(in);
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                }
            });
        } else {
            Intent in = new Intent(getApplicationContext(), NetErrorActivity.class);
            in.putExtra("layout_flag", "1");
            startActivity(in);

        }

    }

    public void init() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Loading data...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

        content_dashboard = (RelativeLayout) findViewById(R.id.content_dashboard);

        layout_user = (CardView) findViewById(R.id.layout_user);
        layout_total_project = (LinearLayout) findViewById(R.id.layout_total_project);
        layout_your_project = (RelativeLayout) findViewById(R.id.layout_your_project);
        layout_total_task = (RelativeLayout) findViewById(R.id.layout_total_task);
        layout_your_task = (RelativeLayout) findViewById(R.id.layout_your_task);
        project_view = (View) findViewById(R.id.project_view);
        swipe_layout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);

        tuser = (TextView) findViewById(R.id.tuser);
        tproject = (TextView) findViewById(R.id.tproject);
        mproject = (TextView) findViewById(R.id.mproject);
        ttask = (TextView) findViewById(R.id.ttask);
        mtask = (TextView) findViewById(R.id.mtask);

        count_user = (TextView) findViewById(R.id.count_user);
        count_total_project = (TextView) findViewById(R.id.count_total_project);
        count_my_project = (TextView) findViewById(R.id.count_my_project);
        count_total_task = (TextView) findViewById(R.id.count_total_task);
        count_my_task = (TextView) findViewById(R.id.count_my_task);

        count_total_complete_project = (TextView) findViewById(R.id.count_total_complete_project);
        count_total_incomplete_project = (TextView) findViewById(R.id.count_total_incomplete_project);
        count_total_complete_task = (TextView) findViewById(R.id.count_total_complete_task);
        count_total_incomplete_task = (TextView) findViewById(R.id.count_total_incomplete_task);
        count_my_complete_task = (TextView) findViewById(R.id.count_my_complete_task);
        count_my_incomplete_task = (TextView) findViewById(R.id.count_my_incomplete_task);

        my_incomplete_project = (TextView) findViewById(R.id.my_incomplete_project);
        my_complete_project = (TextView) findViewById(R.id.my_complete_project);

        task_project_recycler = (RecyclerView) findViewById(R.id.task_project_recycler);
        no_task = (TextView) findViewById(R.id.no_task);

        see_all = (Button) findViewById(R.id.see_all);
        see_all_tree = (Button) findViewById(R.id.see_all_tree);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        TextView title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        title.setText(R.string.title_activity_dashboard);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        draweritem_recycler = (RecyclerView) findViewById(R.id.navigation_draweritem_recycler);

        header_user_name = (TextView) findViewById(R.id.header_user_name);
        header_user_ph = (TextView) findViewById(R.id.header_user_ph);

        usm = new UserSessionManager(this);
        user_details = usm.getUserDetails();

        user_name = user_details.get(U_NAME);
        user_is_admin = user_details.get(U_IS_ADMIN);
        user_can_assign = user_details.get(U_CAN_ASSIGN);
        user_ph = user_details.get(U_PHNO);
        user_emp_id = user_details.get(U_EMP_ID);

        Arial = Typeface.createFromAsset(getAssets(), "fonts/arial.ttf");
        Bahu_bold = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_bold.ttf");
        Bahu_thin = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_thin.ttf");

        header_user_name.setText(user_name);
        header_user_ph.setText(user_ph);

        header_user_name.setTypeface(Bahu_thin);
        header_user_ph.setTypeface(Bahu_thin);

        tuser.setTypeface(Bahu_bold);
        tproject.setTypeface(Bahu_bold);
        mproject.setTypeface(Bahu_bold);
        ttask.setTypeface(Bahu_bold);
        mtask.setTypeface(Bahu_bold);
        see_all.setTypeface(Bahu_thin);
        see_all_tree.setTypeface(Bahu_thin);
        no_task.setTypeface(Bahu_thin);

        count_user.setTypeface(Bahu_bold);
        count_total_project.setTypeface(Bahu_bold);
        count_my_project.setTypeface(Bahu_bold);
        count_total_task.setTypeface(Bahu_bold);
        count_my_task.setTypeface(Bahu_bold);

        count_total_complete_project.setTypeface(Bahu_thin);
        count_total_incomplete_project.setTypeface(Bahu_thin);
        count_total_complete_task.setTypeface(Bahu_thin);
        count_total_incomplete_task.setTypeface(Bahu_thin);
        count_my_complete_task.setTypeface(Bahu_thin);
        count_my_incomplete_task.setTypeface(Bahu_thin);

        draweritem_recycler.addOnItemTouchListener(new DrawerItem_adapter.RecyclerTouchListener(Dashboard.this, draweritem_recycler, new DrawerItem_adapter.ClickListener() {
            @Override
            public void onClick(int position) {

                DrawerItem di = drawer_item.get(position);
                Intent in = null;

                switch (di.getTitle()) {
                    case "DASHBOARD":
                        break;
                    case "ADD EMPLOYEE":
                        in = new Intent(getApplicationContext(), CreateEmployee.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(in);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        break;
                    case "MANAGE EMPLOYEE":
                        in = new Intent(getApplicationContext(), Employee_Details.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(in);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        break;
                    case "CALENDAR":
                        in = new Intent(getApplicationContext(), Task_calendar.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        in.putParcelableArrayListExtra("task_details", task_details);
                        startActivity(in);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        break;
                    case "CREATE NEW PROJECT":
                        CreateNewProject();
                        break;
                    case "MANAGE ALL PROJECTS":
                        in = new Intent(getApplicationContext(), ManageAllProject.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(in);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        break;
                    case "MY TASKS":
                        in = new Intent(getApplicationContext(), SeeYourAllTasks.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        in.putParcelableArrayListExtra("task_details", task_details);
                        startActivity(in);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        break;
                    case "ASSIGN NEW TASK":
                        in = new Intent(getApplicationContext(), NewTask_SubTask.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        in.putExtra("sub_task", "no");
                        startActivity(in);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        break;
                    case "MANAGE ASSIGN TASKS":
                        in = new Intent(getApplicationContext(), ManageAllTask.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(in);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        break;
                    case "Settings":
                        break;
                    case "DATE EXTEND REQUEST":
                        in = new Intent(getApplicationContext(), DateExtend.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(in);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        break;
                    case "TASK COMPLETE REQUEST":
                        in = new Intent(getApplicationContext(), Task_Completed.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(in);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        break;
                    case "USER DETAILS":
                        in = new Intent(getApplicationContext(), UserDetails.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(in);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        break;
                    case "LOG OUT":
                        Logout();
                        break;
                }

                if (mDrawerLayout != null) {
                    Log.d(TAG, "onClick: open " + mDrawerLayout.isDrawerOpen(Gravity.LEFT));
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                }

            }

            @Override
            public void onLongClick(int position) {
            }
        }));
    }

    public void getTaskProjectCount() {

        swipe_layout.setRefreshing(true);
        String JSON_URL_TOKEN_DASHBOARD_COUNT = BASE_URL + "dash_info.php?user_id=" + user_emp_id;
        Log.d(TAG, "getData: dash info" + JSON_URL_TOKEN_DASHBOARD_COUNT);

        try {
            // Volley's json array request object
            JsonObjectRequest req2 = new JsonObjectRequest(JSON_URL_TOKEN_DASHBOARD_COUNT, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("MainActivity", response.toString());
                            try {
                                if (response.length() > 0) {
                                    TaskProject_CountReturnJson tpc = new TaskProject_CountReturnJson(response.toString());
                                    task_project_count = tpc.TaskProject_CountReturnJson();
                                    getTaskDetailsData();
                                } else {
                                    swipe_layout.setRefreshing(false);
                                }
                            } catch (Exception e) {
                                Log.d("MainActivity", "onResponse: " + e);
                                swipe_layout.setRefreshing(false);
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("MainActivity", "Server Error: " + error.getMessage());
                    swipe_layout.setRefreshing(false);
                    setData();
                }
            });
            // Adding request to request queue
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            int socketTimeout = 600000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req2.setRetryPolicy(policy);
            requestQueue.add(req2);

        } catch (Exception e) {
            Log.d("MainActivity", "getCustomer: " + e);
        }
    }

    public void getTaskDetailsData() {

        String JSON_URL_TOKEN_TAK_DETAILS = BASE_URL + "user_all_task_details.php?user_id=" + user_emp_id;

        Log.d(TAG, "getData: task details" + JSON_URL_TOKEN_TAK_DETAILS);

        try {
            // Volley's json array request object
            JsonArrayRequest req2 = new JsonArrayRequest(JSON_URL_TOKEN_TAK_DETAILS,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d("getTaskDetailsData", response.toString());
                            try {
                                swipe_layout.setRefreshing(false);
                                TaskDetailsReturnJson tpc = new TaskDetailsReturnJson(response.toString());
                                task_details = tpc.TaskDetailsReturnJson();
                                swipe_layout.setRefreshing(false);
                                setData();
                                getProjectDetails();

                            } catch (Exception e) {
                                Log.d("getTaskDetailsData", "onResponse: " + e);
                                swipe_layout.setRefreshing(false);
                                setData();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("getTaskDetailsData", "Server Error: " + error.getMessage());
                            swipe_layout.setRefreshing(false);
                            setData();
                        }
                    });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            int socketTimeout = 600000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req2.setRetryPolicy(policy);
            requestQueue.add(req2);
        } catch (Exception e) {
            Log.d("getTaskDetailsData", "exception: " + e);
        }
    }

    public void setData() {

        if (task_project_count != null) {
            count_details = task_project_count.get(0);

            count_user.setText(count_details.getTotal_employee());

            count_total_project.setText(count_details.getTotal_project());
            count_total_complete_project.setText(count_details.getTotal_complete_project());
            count_total_incomplete_project.setText(count_details.getTotal_incomplete_project());

            count_total_task.setText(count_details.getTotal_task());
            count_total_complete_task.setText(count_details.getTotal_complete_task());
            count_total_incomplete_task.setText(count_details.getTotal_incomplete_task());

            count_my_project.setText(count_details.getMy_project());
            my_incomplete_project.setVisibility(View.INVISIBLE);
            my_complete_project.setVisibility(View.INVISIBLE);

            count_my_task.setText(count_details.getMy_total_task());
            count_my_complete_task.setText(count_details.getMy_total_complete_task());
            count_my_incomplete_task.setText(count_details.getMy_total_incomplete_task());
        }


        if (task_details.isEmpty()) {
            no_task.setVisibility(View.VISIBLE);
            task_project_recycler.setVisibility(View.GONE);
            see_all.setVisibility(View.GONE);
            see_all_tree.setVisibility(View.GONE);
        } else {
            no_task.setVisibility(View.GONE);
            see_all.setVisibility(View.VISIBLE);
            see_all_tree.setVisibility(View.VISIBLE);
            task_project_recycler.setVisibility(View.VISIBLE);
            Dashboard_task_project_adapter dash_adapter = new Dashboard_task_project_adapter(this, task_details, user_can_assign);
            task_project_recycler.setAdapter(dash_adapter);
            GridLayoutManager gm = new GridLayoutManager(this, 2);
            task_project_recycler.setLayoutManager(gm);
            task_project_recycler.setNestedScrollingEnabled(false);

        }

        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
            swipe_layout.setVisibility(View.VISIBLE);
        }

        navigation();
    }

    public void navigation() {

        item_name = new ArrayList<>();
        item_icon = new ArrayList<>();
        date_complete_task_count = new ArrayList<>();

        if (user_is_admin.equalsIgnoreCase("YES") && user_can_assign.equalsIgnoreCase("YES")) {
            item_name.add("DASHBOARD");
            item_name.add("ADD EMPLOYEE");
            item_name.add("MANAGE EMPLOYEE");
            item_name.add("CREATE NEW PROJECT");
            item_name.add("MANAGE ALL PROJECTS");
            item_name.add("MY TASKS");
            item_name.add("ASSIGN NEW TASK");
            item_name.add("MANAGE ASSIGN TASKS");
            item_name.add("CALENDAR");
            item_name.add("DATE EXTEND REQUEST");
            item_name.add("TASK COMPLETE REQUEST");
            item_name.add("USER DETAILS");
            item_name.add("LOG OUT");

            item_icon.add(R.drawable.ic_dashboard);
            item_icon.add(R.drawable.ic_add_employee);
            item_icon.add(R.drawable.ic_manage_employee);
            item_icon.add(R.drawable.ic_create_project);
            item_icon.add(R.drawable.ic_all_project);
            item_icon.add(R.drawable.ic_tasks);
            item_icon.add(R.drawable.ic_add_newtask);
            item_icon.add(R.drawable.ic_manage_task);
            item_icon.add(R.drawable.ic_calendar_dashboard);
            item_icon.add(R.drawable.ic_date_extended_req);
            item_icon.add(R.drawable.ic_task_complete);
            item_icon.add(R.drawable.ic_person);
            item_icon.add(R.drawable.ic_logout);

            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(Integer.parseInt(count_details.getTotal_date_request()));
            date_complete_task_count.add(Integer.parseInt(count_details.getTotal_task_complete_request()));
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
        } else if (user_is_admin.equalsIgnoreCase("YES") && user_can_assign.equalsIgnoreCase("NO")) {
            item_name.add("DASHBOARD");
            item_name.add("ADD EMPLOYEE");
            item_name.add("MANAGE EMPLOYEE");
            item_name.add("CREATE NEW PROJECT");
            item_name.add("MANAGE ALL PROJECTS");
            item_name.add("MY TASKS");
            item_name.add("CALENDAR");
            item_name.add("DATE EXTEND REQUEST");
            item_name.add("TASK COMPLETE REQUEST");
            item_name.add("USER DETAILS");
            item_name.add("LOG OUT");

            item_icon.add(R.drawable.ic_dashboard);
            item_icon.add(R.drawable.ic_dashboard);
            item_icon.add(R.drawable.ic_dashboard);
            item_icon.add(R.drawable.ic_create_project);
            item_icon.add(R.drawable.ic_all_project);
            item_icon.add(R.drawable.ic_tasks);
            item_icon.add(R.drawable.ic_calendar_dashboard);
            item_icon.add(R.drawable.ic_date_extended_req);
            item_icon.add(R.drawable.ic_task_complete);
            item_icon.add(R.drawable.ic_person);
            item_icon.add(R.drawable.ic_logout);

            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(Integer.parseInt(count_details.getTotal_date_request()));
            date_complete_task_count.add(Integer.parseInt(count_details.getTotal_task_complete_request()));
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
        } else if (user_is_admin.equalsIgnoreCase("NO") && user_can_assign.equalsIgnoreCase("YES")) {

            layout_user.setVisibility(View.GONE);
            layout_total_project.setVisibility(View.GONE);
            project_view.setVisibility(View.GONE);

            item_name.add("DASHBOARD");
            item_name.add("CREATE NEW PROJECT");
            item_name.add("MANAGE ALL PROJECTS");
            item_name.add("MY TASKS");
            item_name.add("ASSIGN NEW TASK");
            item_name.add("MANAGE ASSIGN TASKS");
            item_name.add("CALENDAR");
            item_name.add("DATE EXTEND REQUEST");
            item_name.add("TASK COMPLETE REQUEST");
            item_name.add("USER DETAILS");
            item_name.add("LOG OUT");

            item_icon.add(R.drawable.ic_dashboard);
            item_icon.add(R.drawable.ic_create_project);
            item_icon.add(R.drawable.ic_all_project);
            item_icon.add(R.drawable.ic_tasks);
            item_icon.add(R.drawable.ic_add_newtask);
            item_icon.add(R.drawable.ic_manage_task);
            item_icon.add(R.drawable.ic_calendar_dashboard);
            item_icon.add(R.drawable.ic_date_extended_req);
            item_icon.add(R.drawable.ic_task_complete);
            item_icon.add(R.drawable.ic_person);
            item_icon.add(R.drawable.ic_logout);

            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(Integer.parseInt(count_details.getTotal_date_request()));
            date_complete_task_count.add(Integer.parseInt(count_details.getTotal_task_complete_request()));
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);

        } else if (user_is_admin.equalsIgnoreCase("NO") && user_can_assign.equalsIgnoreCase("NO")) {

            layout_user.setVisibility(View.VISIBLE);
            layout_total_project.setVisibility(View.VISIBLE);
            project_view.setVisibility(View.VISIBLE);

            item_name.add("DASHBOARD");
            item_name.add("MY TASKS");
            item_name.add("CALENDAR");
            item_name.add("USER DETAILS");
            item_name.add("LOG OUT");

            item_icon.add(R.drawable.ic_dashboard);
            item_icon.add(R.drawable.ic_tasks);
            item_icon.add(R.drawable.ic_calendar_dashboard);
            item_icon.add(R.drawable.ic_person);
            item_icon.add(R.drawable.ic_logout);

            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
            date_complete_task_count.add(0);
        }
        drawer_item = new ArrayList<>();

        for (int i = 0; i < item_name.size(); i++) {
            DrawerItem di = new DrawerItem();

            di.setTitle(item_name.get(i));
            di.setIcon(item_icon.get(i));
            di.setCount(date_complete_task_count.get(i));

            drawer_item.add(di);
        }

        adapter = new DrawerItem_adapter(this, drawer_item);
        draweritem_recycler.setAdapter(adapter);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        draweritem_recycler.setLayoutManager(lm);

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                if (slideOffset == 0) {
                    View decorView = getWindow().getDecorView();
                    int uiOptions = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                    decorView.setSystemUiVisibility(uiOptions);
                } else {
                    View decorView = getWindow().getDecorView();
                    int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
                    decorView.setSystemUiVisibility(uiOptions);     ///////////hide status bar
                }
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        content_dashboard.setVisibility(View.VISIBLE);

    }

    public void Logout() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d(TAG, "onClick: " + new FireBaseSessionManager(getApplicationContext()).getUserToken());
                    FirebaseInstanceId.getInstance().deleteInstanceId();
                    new FireBaseSessionManager(getApplicationContext()).clearSession();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        usm.clearSession();
        Intent in = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(in);
        finish();
//        String JSON_URL_DELETE_FIREBASE_TOKEN = BASE_URL + "delete_firebase_token.php?user_id=" + user_emp_id;
//
//        try {
//            Log.d(TAG, "Logout onClick:   json " + JSON_URL_DELETE_FIREBASE_TOKEN);
//
//            StringRequest req2 = new StringRequest(JSON_URL_DELETE_FIREBASE_TOKEN,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            Log.d("Logout res", response.toString());
//                            try {
//                                if (response.length() > 0) {
//
//                                    if (response.toString().equalsIgnoreCase("1")) {
//
//                                        new Thread(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                try {
//                                                    Log.d(TAG, "onClick: " + new FireBaseSessionManager(getApplicationContext()).getUserToken());
//                                                    FirebaseInstanceId.getInstance().deleteInstanceId();
//                                                    new FireBaseSessionManager(getApplicationContext()).clearSession();
//                                                } catch (IOException e) {
//                                                    e.printStackTrace();
//                                                }
//                                            }
//                                        }).start();
//                                        usm.clearSession();
//                                        Intent in = new Intent(getApplicationContext(), MainActivity.class);
//                                        startActivity(in);
//                                        finish();
//                                    }
//
//                                } else {
//                                    Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
//
//                                }
//                            } catch (Exception e) {
//                                Log.d("Logout", "onResponse: " + e);
//                                Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
//
//                            }
//                        }
//
//                    }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    Log.e("Logout", "Server Error: " + error.getMessage());
//                    Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
//
//                }
//            });
//
//            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
//            int socketTimeout = 600000;//30 seconds - change to what you want
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            req2.setRetryPolicy(policy);
//            requestQueue.add(req2);
//
//        } catch (Exception e) {
//            Log.d("Logout", "getCustomer: " + e);
//            Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
//        }
    }

    public void CreateNewProject() {

        Log.d(TAG, "CreateNewProject: ");
        dialog_create_project = new Dialog(this);
        dialog_create_project.setCancelable(true);
        dialog_create_project.setCanceledOnTouchOutside(true);
        dialog_create_project.setContentView(R.layout.content_new_project);

        TextView projettype = (TextView) dialog_create_project.findViewById(R.id.projettype);
        TextView projetname = (TextView) dialog_create_project.findViewById(R.id.projetname);
        TextView projetdesc = (TextView) dialog_create_project.findViewById(R.id.projetdesc);

        project_type = (Spinner) dialog_create_project.findViewById(R.id.project_type);
        project_name = (EditText) dialog_create_project.findViewById(R.id.create_project_name);
        details_project = (EditText) dialog_create_project.findViewById(R.id.details_project);
        choose_document = (Button) dialog_create_project.findViewById(R.id.choose_document);
        document_name = (TextView) dialog_create_project.findViewById(R.id.document_name);
        submit = (Button) dialog_create_project.findViewById(R.id.submit);
        cancel = (Button) dialog_create_project.findViewById(R.id.cancel_project);

        projettype.setTypeface(Bahu_bold);
        projetname.setTypeface(Bahu_bold);
        projetdesc.setTypeface(Bahu_bold);

        project_name.setTypeface(Bahu_thin);
        details_project.setTypeface(Bahu_thin);
        choose_document.setTypeface(Bahu_thin);
        document_name.setTypeface(Bahu_thin);
        submit.setTypeface(Bahu_thin);
        cancel.setTypeface(Bahu_thin);

        project_type_list = new ArrayList<>();
        project_type_id_list = new ArrayList<>();

        project_type_list.add("Select");
        project_type_id_list.add("0");

        for (ProjectDetails project : project_detail) {

            String type = project.getProject_type();
            String type_id = project.getProject_type_id();

            ProjectType projectType = new ProjectType();

            if (project_type_list.isEmpty()) {
                project_type_list.add(type);
                project_type_id_list.add(type_id);
            } else {
                if (!project_type_list.contains(type)) {
                    project_type_list.add(type);
                    project_type_id_list.add(type_id);
                }
            }
            projectType.project_type = type;
            projectType.project_type_id = type_id;

        }

        final int color = 0xff2d7da6;

        ArrayAdapter<String> adapter_project_type = new ArrayAdapter<String>(this, R.layout.spinner_item, project_type_list) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(Bahu_thin);//Typeface for normal view
                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(Bahu_thin);//Typeface for normal view
                return v;
            }
        };
        adapter_project_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        project_type.setAdapter(adapter_project_type);
        project_type.setSelection(project_type_list.indexOf(0));
        project_type.getBackground().setColorFilter(getResources().getColor(R.color.ItemColor), PorterDuff.Mode.SRC_ATOP);

        project_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                create_project_type_id = project_type_id_list.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        choose_document.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getPermission();

                Intent intent = new Intent();
                //sets the select file to all types of files
                intent.setType("*/*");
                //allows to select data and return it
                intent.setAction(Intent.ACTION_GET_CONTENT);
                //starts new activity to select file and return data
                startActivityForResult(Intent.createChooser(intent, "Choose File to Upload.."), PICK_FILE_REQUEST);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                create_project_name = project_name.getText().toString().trim();
                project_desc = details_project.getText().toString().trim();

                if (create_project_name.length() < 1) {
                    project_name.setError("Enter project name");
                } else if (project_desc.length() < 1) {
                    details_project.setError("Enter project description");
                } else {
                    project_name.setError(null);
                    details_project.setError(null);

                    create_project_name = MakeSubTextToParse(create_project_name);
                    project_desc = MakeSubTextToParse(project_desc);

                    SendNewProjectData();
                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: ");
                dialog_create_project.dismiss();
            }
        });

        dialog_create_project.show();
        Window window = dialog_create_project.getWindow();
        window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
    }

    public void getProjectDetails() {
        String JSON_URL_TOKEN_PROJECT_DETAILS = BASE_URL + "all_project_list.php?";

        try {
            // Volley's json array request object
            JsonArrayRequest req2 = new JsonArrayRequest(JSON_URL_TOKEN_PROJECT_DETAILS,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d("getProjectDetails", response.toString());
                            try {
                                if (response.length() > 0) {
                                    ProjectDetailsReturnJson pds = new ProjectDetailsReturnJson(response.toString());
                                    project_detail = pds.ProjectDetailsReturnJson();
                                } else {
                                }
                            } catch (Exception e) {
                                Log.d("getProjectDetails", "onResponse: " + e);

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("getProjectDetails", "Server Error: " + error.getMessage());
                        }
                    });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            int socketTimeout = 600000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req2.setRetryPolicy(policy);
            requestQueue.add(req2);
        } catch (Exception e) {
            Log.d("getProjectDetails", "exception: " + e);
        }
    }

    public void SendNewProjectData() {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle("Please confirm to submit");

        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Calendar cal = Calendar.getInstance();
                current_date_time = simpleDateFormat.format(cal.getTime());
                JSON_URL_TOKEN_NEW_PROJECT = BASE_URL + "new_project.php?user_id=" + user_emp_id + "&project_type_id=" + create_project_type_id + "&project_name=" + create_project_name + "&status=0" + "&desc=" + project_desc + "&date=" + current_date_time;

                try {
                    Log.d(TAG, "SendNewProjectData onClick:   json " + JSON_URL_TOKEN_NEW_PROJECT);
                    StringRequest req2 = new StringRequest(JSON_URL_TOKEN_NEW_PROJECT,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("SendNewProjectData", response.toString());

                                    try {
                                        if (response.length() > 0) {
                                            if (response.toString().equalsIgnoreCase("1")) {
                                                Toast.makeText(getApplicationContext(), "Project has been created successfully", Toast.LENGTH_SHORT).show();
                                                dialog2.dismiss();
                                                dialog_create_project.dismiss();

                                            }

                                        } else {
                                            Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                            dialog2.dismiss();
                                            dialog_create_project.dismiss();
                                        }
                                    } catch (Exception e) {
                                        Log.d("MainActivity", "onResponse: " + e);
                                        Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                        dialog2.dismiss();
                                        dialog_create_project.dismiss();
                                    }
                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("SendNewProjectData", "Server Error: " + error.getMessage());
                            Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                            dialog2.dismiss();
                            dialog_create_project.dismiss();
                        }
                    });

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                    int socketTimeout = 600000;//30 seconds - change to what you want
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    req2.setRetryPolicy(policy);

                    requestQueue.add(req2);

                } catch (Exception e) {
                    Log.d("SendNewProjectData", "getCustomer: " + e);
                    Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                    dialog2.dismiss();
                    dialog_create_project.dismiss();
                }
            }
        });

        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialog2.dismiss();
            }
        });

        dialog2 = dialog.show();

    }

    public String MakeSubTextToParse(String name) {

        String[] name1 = name.split("\\s");

        name = "";
        for (String n2 : name1) {
            Log.d("n2", "" + n2);
            name += n2 + "%20";
        }
        return name;

    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout != null) {

            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            } else {
                if (exit) {
                    finishAffinity(); // finish activity
                } else {
                    Toast.makeText(this, "Press Back again to Exit.",
                            Toast.LENGTH_SHORT).show();
                    exit = true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            exit = false;
                        }
                    }, 3 * 1000);
                }
            }
        } else {
            if (exit) {
                finishAffinity(); // finish activity
            } else {
                Toast.makeText(this, "Press Back again to Exit.",
                        Toast.LENGTH_SHORT).show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 3 * 1000);
            }
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
//        getTaskProjectCount();
        myReceiver = new ConnectivityReceiver();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (myReceiver != null) {
            unregisterReceiver(myReceiver);
            myReceiver = null;
        }

    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected(getApplicationContext());
        return isConnected;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_FILE_REQUEST) {
                if (data == null) {
                    //no data present
                    return;
                }
                Uri selectedFileUri = data.getData();
                selectedFilePath = selectedFileUri.getPath();
                selectedFileName = selectedFileUri.getLastPathSegment();
                Log.i(TAG, "Selected File Path:" + selectedFilePath);
                choseToSend = new File(selectedFilePath);

                if (selectedFilePath != null && !selectedFilePath.equals("")) {
                    document_name.setText(selectedFileName);
                } else {
                    Toast.makeText(this, "Cannot upload file to server", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void getPermission() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_READ);
            }
        } else {
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_READ) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            } else {
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }


}
