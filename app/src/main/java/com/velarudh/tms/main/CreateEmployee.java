package com.velarudh.tms.main;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.velarudh.tms.R;
import com.velarudh.tms.task.ManageAllTask;
import com.velarudh.tms.task.SeeYourAllTasks;
import com.velarudh.tms.util.ConnectivityReceiver;
import com.velarudh.tms.util.NetErrorActivity;
import com.velarudh.tms.util.Url;

import java.util.Random;

public class CreateEmployee extends AppCompatActivity implements View.OnClickListener {

    String TAG = "CreateEmployee";

    Toolbar toolbar;
    FloatingActionButton fab_open;

    TextView empname;
    TextView empcontact;
    TextView empmail;
    TextView empph;
    TextView empdesig;
    TextView empadmin;
    TextView empassign;
    TextView empgid;
    TextView empgpassword;

    EditText employee_fname;
    EditText employee_lname;
    EditText employee_email;
    EditText employee_ph;
    EditText employee_designation;
    EditText login_id;
    EditText employee_password;

    RadioButton admin_yes;
    RadioButton admin_no;
    RadioButton assign_yes;
    RadioButton assign_no;

    Button auto_id;
    Button submit;

    String f_name;
    String l_name;
    String email_value;
    String ph_value;
    String designation_value;
    String assign_value = "";
    String admin_value = "";
    String logid_value;
    String password_value;

    AlertDialog dialog2 = null;

    ConnectivityReceiver myReceiver;

    String BASE_URL = Url.BASE_URL;

    Typeface bahu_thin;
    Typeface bahu_bold;

    Intent in = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_employee);

        init();

    }

    public void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.title_activity_create_employee);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        fab_open = (FloatingActionButton) findViewById(R.id.fab_open);

        empname = (TextView) findViewById(R.id.empname);
        empcontact = (TextView) findViewById(R.id.empcontact);
        empmail = (TextView) findViewById(R.id.empmail);
        empph = (TextView) findViewById(R.id.empph);
        empdesig = (TextView) findViewById(R.id.empdesig);
        empadmin = (TextView) findViewById(R.id.empadmin);
        empassign = (TextView) findViewById(R.id.empassign);
        empgid = (TextView) findViewById(R.id.empgid);
        empgpassword = (TextView) findViewById(R.id.empgpassword);

        employee_fname = (EditText) findViewById(R.id.employee_fname);
        employee_lname = (EditText) findViewById(R.id.employee_lname);
        employee_email = (EditText) findViewById(R.id.employee_email);
        employee_ph = (EditText) findViewById(R.id.employee_ph);
        employee_designation = (EditText) findViewById(R.id.employee_designation);
        login_id = (EditText) findViewById(R.id.employee_login_id);
        employee_password = (EditText) findViewById(R.id.employee_password);

        admin_yes = (RadioButton) findViewById(R.id.admin_yes);
        admin_no = (RadioButton) findViewById(R.id.admin_no);
        assign_yes = (RadioButton) findViewById(R.id.assign_yes);
        assign_no = (RadioButton) findViewById(R.id.assign_no);

        auto_id = (Button) findViewById(R.id.auto_id);
        submit = (Button) findViewById(R.id.submit);

        auto_id.setOnClickListener(this);
        submit.setOnClickListener(this);

        bahu_thin = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_thin.ttf");
        bahu_bold = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_bold.ttf");

        SetTypeface();

        fab_open.setOnClickListener(this);

    }

    public void SetTypeface() {

        empname.setTypeface(bahu_bold);
        empcontact.setTypeface(bahu_bold);
        empmail.setTypeface(bahu_bold);
        empph.setTypeface(bahu_bold);
        empdesig.setTypeface(bahu_bold);
        empadmin.setTypeface(bahu_bold);
        empassign.setTypeface(bahu_bold);
        empgid.setTypeface(bahu_bold);
        empgpassword.setTypeface(bahu_bold);

        employee_fname.setTypeface(bahu_thin);
        employee_lname.setTypeface(bahu_thin);
        employee_email.setTypeface(bahu_thin);
        employee_ph.setTypeface(bahu_thin);
        employee_designation.setTypeface(bahu_thin);
        login_id.setTypeface(bahu_thin);
        employee_password.setTypeface(bahu_thin);

        admin_yes.setTypeface(bahu_bold);
        admin_no.setTypeface(bahu_bold);
        assign_yes.setTypeface(bahu_bold);
        assign_no.setTypeface(bahu_bold);

        auto_id.setTypeface(bahu_thin);
        submit.setTypeface(bahu_thin);

    }

    public boolean Validation() {

        boolean valid = true;

        f_name = employee_fname.getText().toString().trim();
        l_name = employee_lname.getText().toString().trim();
        email_value = employee_email.getText().toString().trim();
        ph_value = employee_ph.getText().toString().trim();
        designation_value = employee_designation.getText().toString().trim();
        logid_value = login_id.getText().toString().trim();
        password_value = employee_password.getText().toString().trim();
        if (admin_yes.isChecked()) {
            admin_value = "1";
        } else if (admin_no.isChecked()) {
            admin_value = "0";
        }
        if (assign_yes.isChecked()) {
            assign_value = "1";
        } else if (assign_no.isChecked()) {
            assign_value = "0";
        }

        if (f_name.isEmpty()) {
            valid = false;
            employee_fname.setError("Name cant be empty");
        } else {
            employee_fname.setError(null);
        }

        if (l_name.isEmpty()) {
            valid = false;
            employee_lname.setError("Name cant be empty");
        } else {
            employee_lname.setError(null);
        }

        if (email_value.isEmpty()) {
            valid = false;
            employee_email.setError("Email cant be empty");
        } else {
            employee_email.setError(null);
        }

        if (ph_value.isEmpty()) {
            valid = false;
            employee_ph.setError("Phone number cant be empty");
        } else if (ph_value.length() != 10) {
            valid = false;
            employee_ph.setError("enter correct phone number");
        } else {
            employee_ph.setError(null);
        }

        if (designation_value.isEmpty()) {
            valid = false;
            employee_designation.setError("Designation cant be empty");
        } else {
            employee_designation.setError(null);
        }


        if (logid_value.isEmpty()) {
            valid = false;
            login_id.setError("Log-In Id needed");
        } else {
            login_id.setError(null);
        }

        if (password_value.isEmpty()) {
            valid = false;
            employee_password.setError("Password needed");
        } else {
            employee_password.setError(null);
        }

        if (admin_value.isEmpty()) {
            valid = false;
            admin_yes.setError("");
            admin_no.setError("");
        } else {
            admin_yes.setError(null);
            admin_no.setError(null);
        }

        if (assign_value.isEmpty()) {
            valid = false;
            assign_yes.setError("");
            assign_no.setError("");
        } else {
            assign_yes.setError(null);
            assign_no.setError(null);
        }

        return valid;
    }

    public void makeid() {
        char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 4; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String output = sb.toString();
        login_id.setText(output);
        makePassword(output);
    }

    public void makePassword(String id) {
        char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 4; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String output = id + "@" + sb.toString();
        employee_password.setText(output);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.fab_open:
                SetShortCutLayout();
                break;

            case R.id.auto_id:
                makeid();
                break;

            case R.id.submit:
                if (Validation()) {
                    if (checkConnection()) {

                        designation_value = MakeSubTextToParse(designation_value);

                        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                        dialog.setCancelable(false);
                        dialog.setTitle("Please confirm to submit");

                        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {


                                String JSON_URL_TOKEN_NEW_EMPLOYEE = BASE_URL + "create_employee.php?f_name=" + f_name + "&l_name=" + l_name + "&email=" + email_value + "&ph=" + ph_value + "&designation=" + designation_value + "&admin=" + admin_value + "&assign=" + assign_value + "&login_id=" + logid_value + "&password=" + password_value;


                                try {
                                    Log.d(TAG, "SendNewemployeetData onClick:   json " + JSON_URL_TOKEN_NEW_EMPLOYEE);
                                    StringRequest req2 = new StringRequest(JSON_URL_TOKEN_NEW_EMPLOYEE,
                                            new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {
                                                    Log.d("SendNewemployeetData", response.toString());

                                                    try {
                                                        if (response.length() > 0) {
                                                            if (response.toString().equalsIgnoreCase("1")) {
                                                                Toast.makeText(getApplicationContext(), "Employee has created successfully", Toast.LENGTH_SHORT).show();
                                                                dialog2.dismiss();
                                                                onBackPressed();

                                                            }

                                                        } else {
                                                            Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                                            dialog2.dismiss();
                                                        }
                                                    } catch (Exception e) {
                                                        Log.d("MainActivity", "onResponse: " + e);
                                                        Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                                        dialog2.dismiss();
                                                    }
                                                }

                                            }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Log.e("SendNewProjectData", "Server Error: " + error.getMessage());
                                            Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                            dialog2.dismiss();
                                        }
                                    });

                                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                                    int socketTimeout = 600000;//30 seconds - change to what you want
                                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                                    req2.setRetryPolicy(policy);

                                    requestQueue.add(req2);

                                } catch (Exception e) {
                                    Log.d("SendNewProjectData", "getCustomer: " + e);
                                    Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                    dialog2.dismiss();
                                }
                            }
                        });

                        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                dialog2.dismiss();
                            }
                        });

                        dialog2 = dialog.show();
                    } else {
                        Intent in = new Intent(this, NetErrorActivity.class);
                        in.putExtra("layout_flag", "1");
                        startActivity(in);
                    }
                }
        }
    }

    public String MakeSubTextToParse(String name) {

        String[] name1 = name.split("\\s");

        name = "";
        for (String n2 : name1) {
            Log.d("n2", "" + n2);
            name += n2 + "%20";
        }
        return name;

    }

    public void SetShortCutLayout() {

        LayoutInflater inflate = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
        Snackbar snackbar = Snackbar.make(findViewById(R.id.content_new_task), "", 1000);
// Get the Snackbar's layout view
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);
        layout.setBackgroundColor(Color.parseColor("#3581a1"));
// Inflate our custom view
        View snackView = inflate.inflate(R.layout.snackbar_layout, null);
// Configure the view

        RelativeLayout dashboard = (RelativeLayout) snackView.findViewById(R.id.dashboard);
        RelativeLayout your_task = (RelativeLayout) snackView.findViewById(R.id.your_task);
        RelativeLayout all_task = (RelativeLayout) snackView.findViewById(R.id.all_task);
        RelativeLayout group_chat = (RelativeLayout) snackView.findViewById(R.id.group_chat);

        dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                in = new Intent(getApplicationContext(), Dashboard.class);
                startActivity(in);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        your_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                in = new Intent(getApplicationContext(), SeeYourAllTasks.class);
                startActivity(in);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        all_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                in = new Intent(getApplicationContext(), ManageAllTask.class);
                startActivity(in);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        group_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
// Add the view to the Snackbar's layout
        layout.addView(snackView, 0);
// Show the Snackbar
        snackbar.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();

        myReceiver = new ConnectivityReceiver();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(myReceiver);
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected(getApplicationContext());
        return isConnected;
    }
}
