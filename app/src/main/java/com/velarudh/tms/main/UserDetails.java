package com.velarudh.tms.main;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.velarudh.tms.R;
import com.velarudh.tms.project.ManageAllProject;
import com.velarudh.tms.task.ManageAllTask;
import com.velarudh.tms.task.SeeYourAllTasks;
import com.velarudh.tms.util.ConnectivityReceiver;
import com.velarudh.tms.util.NetErrorActivity;
import com.velarudh.tms.util.Url;
import com.velarudh.tms.util.UserSessionManager;

import java.util.HashMap;

public class UserDetails extends AppCompatActivity implements View.OnClickListener {

    String TAG = "UserDetails";

    Toolbar toolbar;

    TextView userid;
    TextView userloginid;
    TextView username;
    TextView usercontact;
    TextView usermail;
    TextView userph;
    TextView userdesig;
    TextView useradmin;
    TextView userassign;

    TextView user_id;
    TextView login_id;
    TextView user_name;
    TextView user_email;
    TextView user_ph;
    TextView user_designation;
    TextView is_admin;
    TextView can_assign;

    String id;
    String log_in_id_value;
    String password;
    String name;
    String email;
    String ph;
    String designation;
    String admin;
    String assign;

    Button change_id;
    Button change_password;

    FloatingActionButton fab;

    UserSessionManager usm;
    HashMap<String, String> user_details;

    EditText new_logIn_id;
    EditText confirm_logIn_id;
    Button submit;
    Button cancel;

    String change_logIn_id_value;

    EditText new_password;
    EditText confirm_password;
    ImageButton password_visible;
    ImageButton password_visible_confirm;

    String change_password_value;

    AlertDialog dialog2 = null;

    boolean password_isVisible = false;
    boolean password_isVisible_confirm = false;
    ConnectivityReceiver myReceiver;
    String BASE_URL = Url.BASE_URL;

    Intent in_shortcut = null;

    Typeface bahu_thin;
    Typeface bahu_bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        if(checkConnection()) {
            init();
            setData();
        }
        else
        {
            Intent in = new Intent(this, NetErrorActivity.class);
            in.putExtra("layout_flag","1");
            startActivity(in);
        }
    }

    public void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.title_activity_user_details);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab_open);

        userid = (TextView) findViewById(R.id.userid);
        userloginid = (TextView) findViewById(R.id.userloginid);
        username = (TextView) findViewById(R.id.username);
        usercontact = (TextView) findViewById(R.id.usercontact);
        usermail = (TextView) findViewById(R.id.usermail);
        userph = (TextView) findViewById(R.id.userph);
        userdesig = (TextView) findViewById(R.id.userdesig);
        useradmin = (TextView) findViewById(R.id.useradmin);
        userassign = (TextView) findViewById(R.id.userassign);

        user_id = (TextView) findViewById(R.id.user_id);
        login_id = (TextView) findViewById(R.id.login_id);
        user_name = (TextView) findViewById(R.id.user_name);
        user_email = (TextView) findViewById(R.id.user_email);
        user_ph = (TextView) findViewById(R.id.user_ph);
        user_designation = (TextView) findViewById(R.id.user_designation);
        is_admin = (TextView) findViewById(R.id.is_admin);
        can_assign = (TextView) findViewById(R.id.can_assign);

        change_id = (Button) findViewById(R.id.change_id);
        change_password = (Button) findViewById(R.id.change_password);

        change_id.setOnClickListener(this);
        change_password.setOnClickListener(this);
        fab.setOnClickListener(this);
        usm = new UserSessionManager(this);

        user_details = usm.getUserDetails();

        SetTypeFace();
    }

    public void SetTypeFace() {

        bahu_thin = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_thin.ttf");
        bahu_bold = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_bold.ttf");

        userid.setTypeface(bahu_bold);
        userloginid.setTypeface(bahu_bold);
        username.setTypeface(bahu_bold);
        usercontact.setTypeface(bahu_bold);
        usermail.setTypeface(bahu_bold);
        userph.setTypeface(bahu_bold);
        userdesig.setTypeface(bahu_bold);
        useradmin.setTypeface(bahu_bold);
        userassign.setTypeface(bahu_bold);

        user_id.setTypeface(bahu_thin);
        login_id.setTypeface(bahu_thin);
        user_name.setTypeface(bahu_thin);
        user_email.setTypeface(bahu_thin);
        user_ph.setTypeface(bahu_thin);
        user_designation.setTypeface(bahu_thin);
        is_admin.setTypeface(bahu_thin);
        can_assign.setTypeface(bahu_thin);

        change_id.setTypeface(bahu_thin);
        change_password.setTypeface(bahu_thin);
    }

    public void setData() {

        id = user_details.get("user_id");
        log_in_id_value = user_details.get("login_id");
        password = user_details.get("user_password");
        name = user_details.get("user_name");
        email = user_details.get("user_email");
        ph = user_details.get("user_phno");
        designation = user_details.get("user_designation");
        admin = user_details.get("user_is_admin");
        assign = user_details.get("user_can_assign");

        user_id.setText(id);
        login_id.setText(log_in_id_value);
        user_name.setText(name);
        user_email.setText(email);
        user_ph.setText(ph);
        user_designation.setText(designation);
        is_admin.setText(admin);
        can_assign.setText(assign);

        if (user_details.get("user_can_assign").toLowerCase().equalsIgnoreCase("no")) {
            can_assign.setBackgroundColor(Color.parseColor("#F44336"));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       /* Intent in = new Intent(this, Dashboard.class);
        startActivity(in);
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);*/
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.change_id:
                ChangeId();
                break;

            case R.id.change_password:
                ChangePassword();
                break;

            case R.id.fab_open:
                SetShortCutLayout();
                break;
        }
    }

    public void SetShortCutLayout() {

        LayoutInflater inflate = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
        Snackbar snackbar = Snackbar.make(findViewById(R.id.content_user_details), "", 5000);
// Get the Snackbar's layout view
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);
        layout.setBackgroundColor(Color.parseColor("#3581a1"));
// Inflate our custom view
        View snackView = inflate.inflate(R.layout.snackbar_layout, null);
// Configure the view

        RelativeLayout dashboard = (RelativeLayout) snackView.findViewById(R.id.dashboard);
        RelativeLayout your_task = (RelativeLayout) snackView.findViewById(R.id.your_task);
        RelativeLayout all_task = (RelativeLayout) snackView.findViewById(R.id.all_task);
        RelativeLayout group_chat = (RelativeLayout) snackView.findViewById(R.id.group_chat);

        dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                in_shortcut = new Intent(getApplicationContext(), Dashboard.class);
                startActivity(in_shortcut);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        your_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                in_shortcut = new Intent(getApplicationContext(), SeeYourAllTasks.class);
                startActivity(in_shortcut);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        all_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                in_shortcut = new Intent(getApplicationContext(), ManageAllTask.class);
                startActivity(in_shortcut);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        group_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
// Add the view to the Snackbar's layout
        layout.addView(snackView, 0);
// Show the Snackbar
        snackbar.show();
    }

    public void ChangeId() {

        final Dialog dialog1 = new Dialog(this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.change_userid_layout);
        dialog1.setCancelable(false);

        TextView oldname = (TextView) dialog1.findViewById(R.id.oldname);
        TextView newname = (TextView) dialog1.findViewById(R.id.newname);

        new_logIn_id = (EditText) dialog1.findViewById(R.id.new_mail_id);
        confirm_logIn_id = (EditText) dialog1.findViewById(R.id.confirm_mail_id);
        submit = (Button) dialog1.findViewById(R.id.submit);
        cancel = (Button) dialog1.findViewById(R.id.cancel);

        oldname.setTypeface(bahu_bold);
        newname.setTypeface(bahu_bold);

        new_logIn_id.setTypeface(bahu_thin);
        confirm_logIn_id.setTypeface(bahu_thin);
        submit.setTypeface(bahu_thin);
        cancel.setTypeface(bahu_thin);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ChangeIdValidation()) {

                    SendChangeMailId(dialog1);
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });

        dialog1.show();
        Window window = dialog1.getWindow();
        window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);

    }

    public void SendChangeMailId(final Dialog dialog1) {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle("Please confirm to submit");

        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                String JSON_URL_TOKEN_CHNAGE_ID = BASE_URL + "change_userid.php?user_id=" + user_details.get("user_id") + "&login_id=" + change_logIn_id_value;

                try {
                    Log.d(TAG, "onClick:   json " + JSON_URL_TOKEN_CHNAGE_ID);
                    StringRequest req2 = new StringRequest(JSON_URL_TOKEN_CHNAGE_ID,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("ConfirmationAlert", response.toString());

                                    try {
                                        if (response.length() > 0) {
                                            if (response.toString().equalsIgnoreCase("1")) {
                                                Toast.makeText(getApplicationContext(), "Your user id has been changed", Toast.LENGTH_SHORT).show();
                                                usm.setUserDetails(change_logIn_id_value, email, password, name, ph, designation, id, admin, assign);
                                                dialog1.dismiss();
                                                dialog2.dismiss();
                                                startActivity(getIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));

                                            }

                                        } else {
                                            Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                            dialog1.dismiss();
                                        }
                                    } catch (Exception e) {
                                        Log.d("ConfirmationAlert", "onResponse: " + e);
                                        Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                        dialog1.dismiss();
                                        dialog2.dismiss();
                                    }
                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("ConfirmationAlert", "Server Error: " + error.getMessage());
                            Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                            dialog1.dismiss();
                            dialog2.dismiss();
                        }
                    });

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                    int socketTimeout = 600000;//30 seconds - change to what you want
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    req2.setRetryPolicy(policy);

                    requestQueue.add(req2);

                } catch (Exception e) {
                    Log.d("ConfirmationAlert", "getCustomer: " + e);
                    Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                    dialog1.dismiss();
                    dialog2.dismiss();
                }

            }
        });

        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialog2.dismiss();
            }
        });

        dialog2 = dialog.show();
    }

    public boolean ChangeIdValidation() {

        boolean valid = true;

        String confirm_change_mail_id = confirm_logIn_id.getText().toString().trim();

        change_logIn_id_value = new_logIn_id.getText().toString().trim();

        if (change_logIn_id_value.isEmpty()) {
            valid = false;
            new_logIn_id.setError("Please fill new log-In id");
        } else if (confirm_change_mail_id.isEmpty()) {
            valid = false;
            confirm_logIn_id.setError("Please fill new log_In id");
        } else if (!change_logIn_id_value.equalsIgnoreCase(confirm_change_mail_id)) {
            valid = false;
            confirm_logIn_id.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            confirm_logIn_id.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error, 0);
        } else if (change_logIn_id_value.equalsIgnoreCase(confirm_change_mail_id)) {
            Log.d(TAG, "ChangeIdValidation: ");
            new_logIn_id.setError(null);
            confirm_logIn_id.setError(null);
            confirm_logIn_id.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            confirm_logIn_id.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_correct, 0);
        }

        return valid;
    }

    public void ChangePassword() {
        final Dialog dialog1 = new Dialog(this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.change_password_layout);
        dialog1.setCancelable(false);

        new_password = (EditText) dialog1.findViewById(R.id.new_password);
        confirm_password = (EditText) dialog1.findViewById(R.id.confirm_password);
        submit = (Button) dialog1.findViewById(R.id.submit);
        cancel = (Button) dialog1.findViewById(R.id.cancel);

        password_visible = (ImageButton) dialog1.findViewById(R.id.password_visible);
        password_visible_confirm = (ImageButton) dialog1.findViewById(R.id.password_visible_confirm);


        password_visible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (password_isVisible) {
                    password_isVisible = false;
                    new_password.setTransformationMethod(null);
                } else {
                    new_password.setTransformationMethod(new PasswordTransformationMethod());
                    password_isVisible = true;
                }
            }
        });

        password_visible_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (password_isVisible_confirm) {
                    password_isVisible_confirm = false;
                    confirm_password.setTransformationMethod(null);
                } else {
                    confirm_password.setTransformationMethod(new PasswordTransformationMethod());
                    password_isVisible_confirm = true;
                }
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ChangePasswordValidation()) {

                    SendChangePassword(dialog1);
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
                password_isVisible = false;
                password_isVisible_confirm = false;
            }
        });

        dialog1.show();
        Window window = dialog1.getWindow();
        window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
    }

    public boolean ChangePasswordValidation() {

        boolean valid = true;

        String confirm_change_password = confirm_password.getText().toString().trim();

        change_password_value = new_password.getText().toString().trim();

        if (change_password_value.isEmpty()) {
            valid = false;
            new_password.setError("Please fill password");
        } else if (confirm_change_password.isEmpty()) {
            valid = false;
            confirm_password.setError("Please confirm new password");
        } else if (!change_password_value.equalsIgnoreCase(confirm_change_password)) {
            valid = false;
            confirm_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            confirm_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error, 0);
        } else {
            change_password.setError(null);
            new_password.setError(null);
            confirm_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            confirm_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_correct, 0);
        }

        return valid;
    }

    public void SendChangePassword(final Dialog dialog1) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle("Please confirm to submit");

        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                String JSON_URL_TOKEN_CHNAGE_PASSWORD = BASE_URL + "change_password.php?user_id=" + user_details.get("user_id") + "&password=" + change_password_value;

                try {
                    Log.d(TAG, "onClick:   json " + JSON_URL_TOKEN_CHNAGE_PASSWORD);
                    StringRequest req2 = new StringRequest(JSON_URL_TOKEN_CHNAGE_PASSWORD,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("ConfirmationAlert", response.toString());

                                    try {
                                        if (response.length() > 0) {
                                            if (response.toString().equalsIgnoreCase("1")) {
                                                Toast.makeText(getApplicationContext(), "Your password has been changed", Toast.LENGTH_SHORT).show();
                                                usm.setUserDetails(log_in_id_value, email, change_password_value, name, ph, designation, id, admin, assign);
                                                dialog1.dismiss();
                                                dialog2.dismiss();
                                                password_isVisible = false;
                                                password_isVisible_confirm = false;
                                                startActivity(getIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                                            }

                                        } else {
                                            Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                            dialog1.dismiss();
                                            dialog2.dismiss();
                                            password_isVisible = false;
                                            password_isVisible_confirm = false;
                                        }
                                    } catch (Exception e) {
                                        Log.d("ConfirmationAlert", "onResponse: " + e);
                                        Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                        dialog1.dismiss();
                                        dialog2.dismiss();
                                        password_isVisible = false;
                                        password_isVisible_confirm = false;
                                    }
                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("ConfirmationAlert", "Server Error: " + error.getMessage());
                            Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                            dialog1.dismiss();
                            dialog2.dismiss();
                            password_isVisible = false;
                            password_isVisible_confirm = false;
                        }
                    });

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                    int socketTimeout = 600000;//30 seconds - change to what you want
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    req2.setRetryPolicy(policy);

                    requestQueue.add(req2);

                } catch (Exception e) {
                    Log.d("ConfirmationAlert", "getCustomer: " + e);
                    Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                    dialog1.dismiss();
                    dialog2.dismiss();
                    password_isVisible = false;
                    password_isVisible_confirm = false;
                }

            }
        });

        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialog2.dismiss();
            }
        });

        dialog2 = dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        myReceiver = new ConnectivityReceiver();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(myReceiver);
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected(getApplicationContext());
        return isConnected;
    }
}
