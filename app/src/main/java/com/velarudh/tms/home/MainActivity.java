package com.velarudh.tms.home;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.Color;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.velarudh.tms.firebase.FireBaseSessionManager;
import com.velarudh.tms.json.ForgetPasswordReturnJson;
import com.velarudh.tms.json.LogInReturnJson;
import com.velarudh.tms.json.TaskProject_CountReturnJson;
import com.velarudh.tms.main.Dashboard;
import com.velarudh.tms.R;
import com.velarudh.tms.model.ForgetPasswordDetails;
import com.velarudh.tms.project.ManageAllProject;
import com.velarudh.tms.task.SeeYourAllTasks;
import com.velarudh.tms.util.ConnectivityReceiver;
import com.velarudh.tms.util.NetErrorActivity;
import com.velarudh.tms.util.Url;
import com.velarudh.tms.util.UserSessionManager;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    String TAG = "MainActivity";

    RelativeLayout content_main;

    RelativeLayout start_splash_layout;
    RelativeLayout start_verification_layout;
    LinearLayout login_layout;

    EditText code_input;
    EditText input_email;
    EditText input_password;

    Button code_submit;
    Button login_submit;
    Button forget_password;
    ImageButton password_visible;

    int width;
    int height;

    boolean valid_codeCheck = false;
    boolean valid_LoginCheck = false;

    String login_id;
    String password;

    UserSessionManager usm;

    Dialog dialog_forget_password;
    RelativeLayout mail_layout;
    RelativeLayout otp_layout;
    LinearLayout password_layout;

    EditText email_for_otp;
    Button submit_email;

    EditText otp;
    Button submit_otp;

    EditText new_password;
    EditText confirm_password;
    Button submit_password;
    ImageButton password_f_visible;
    ImageButton password_f_visible_confirm;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+[a-z]+";
    String email_f_value;
    String otp_get_value;
    String password_value;

    boolean pass_visible;
    boolean pass_f_visible;
    boolean pass_f_v_visible;

    String BASE_URL;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createShorcut();
        init();

        if (checkConnection()) {
            FirebaseApp.initializeApp(getApplicationContext());
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                //bundle must contain all info sent in "data" field of the notification
                Log.d(TAG, "onCreate: bundle " + bundle.toString());
            }

            Display display = getWindowManager().getDefaultDisplay();
            width = display.getWidth();
            height = display.getHeight();

            Thread splashTread = new Thread() {
                @Override
                public void run() {
                    try {
                        sleep(1000);
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Animation fadeInAnimation = new TranslateAnimation(0, 0, 0, -height * 2 / 7);
                                fadeInAnimation.setDuration(2000); // time for animation in milliseconds
                                fadeInAnimation.setFillAfter(true); // make the transformation persist
                                fadeInAnimation.setAnimationListener(new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationEnd(Animation animation) {
                                        Log.d(TAG, "onAnimationEnd:   done");
                                        if (usm.isLogIn()) {
                                            Intent in = new Intent(getApplicationContext(), Dashboard.class);
                                            startActivity(in);
                                        } else {
//                                            start_verification_layout.setVisibility(View.VISIBLE);
                                            login_layout.setVisibility(View.VISIBLE);
                                        }
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {
                                    }

                                    @Override
                                    public void onAnimationStart(Animation animation) {
                                    }
                                });
                                start_splash_layout.startAnimation(fadeInAnimation);
                            }
                        });

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            splashTread.start();
        } else {
            Intent in = new Intent(this, NetErrorActivity.class);
            in.putExtra("layout_flag", "0");
            startActivity(in);
        }

    }

    @TargetApi(Build.VERSION_CODES.N_MR1)
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void createShorcut() {
        ShortcutManager sM = (ShortcutManager)getSystemService(SHORTCUT_SERVICE);

        Intent intent1 = new Intent(getApplicationContext(), SeeYourAllTasks.class);
        intent1.setAction(Intent.ACTION_VIEW);

        ShortcutInfo shortcut1 = new ShortcutInfo.Builder(this, "shortcut1")
                .setIntent(intent1)
                .setShortLabel("test shortcut")
                .setLongLabel("Shortcut 1")
                .setShortLabel("This is the shortcut 1")
                .setDisabledMessage("Login to open this")
                .setIcon(Icon.createWithResource(this, R.drawable.ic_add_employee))
                .build();

        sM.setDynamicShortcuts(Arrays.asList(shortcut1));
    }

    public void init() {
        content_main = (RelativeLayout) findViewById(R.id.content_main);
        start_splash_layout = (RelativeLayout) findViewById(R.id.start_splash_layout);
        start_verification_layout = (RelativeLayout) findViewById(R.id.start_verification_layout);
        login_layout = (LinearLayout) findViewById(R.id.login_layout);

        code_input = (EditText) findViewById(R.id.code_input);
        input_email = (EditText) findViewById(R.id.input_email);
        input_password = (EditText) findViewById(R.id.input_password);

        code_submit = (Button) findViewById(R.id.code_submit);
        code_submit.setOnClickListener(this);

        login_submit = (Button) findViewById(R.id.login_submit);
        login_submit.setOnClickListener(this);

        forget_password = (Button) findViewById(R.id.forget_password);
        forget_password.setOnClickListener(this);

        password_visible = (ImageButton) findViewById(R.id.password_visible);
        password_visible.setOnClickListener(this);

        usm = new UserSessionManager(this);

        BASE_URL = Url.BASE_URL;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.code_submit:
                start_verification_layout.setVisibility(View.GONE);
                login_layout.setVisibility(View.VISIBLE);
//                if (code_input.getText().toString().isEmpty())
//                {
//                    code_input.setError("Verify the security code please");
//                }
//                else
//                {
//                    if (checkSecurityCode())
//                    {
//                        start_verification_layout.setVisibility(View.GONE);
//                        login_layout.setVisibility(View.VISIBLE);
//                    }
//                    else
//                    {
//                        Snackbar snackbar = Snackbar.make(start_verification_layout,"Plaese enter the valid security code to process",Snackbar.LENGTH_SHORT);
//                        snackbar.show();
//                    }
//                }
                break;

            case R.id.login_submit:

                if (checkConnection()) {
                    if (Validation()) {
                        checkLogIn();
                    }
                } else {
                    Intent in = new Intent(getApplicationContext(), NetErrorActivity.class);
                    in.putExtra("layout_flag", "0");
                    startActivity(in);
                }
                break;

            case R.id.forget_password:
                ForgetPasswordinit();
                break;

            case R.id.submit_email:

                if (EmailValidation()) {
                    SendEmailForOtp();
                }
                break;

            case R.id.submit_otp:

                if (OtpValidation()) {
                    otp_layout.setVisibility(View.GONE);
                    password_layout.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.submit_password:

                if (PasswordValidation()) {
                    SendPasswordToChange();
                }
                break;

            case R.id.password_f_visible:

                if (pass_f_visible) {
                    pass_f_visible = false;
                    new_password.setTransformationMethod(null);
                } else {
                    new_password.setTransformationMethod(new PasswordTransformationMethod());
                    pass_f_visible = true;
                }
                break;

            case R.id.password_f_visible_confirm:

                if (pass_f_v_visible) {
                    pass_f_v_visible = false;
                    confirm_password.setTransformationMethod(null);
                } else {
                    confirm_password.setTransformationMethod(new PasswordTransformationMethod());
                    pass_f_v_visible = true;
                }
                break;

            case R.id.password_visible:

                if (pass_visible) {
                    pass_visible = false;
                    input_password.setTransformationMethod(null);
                } else {
                    input_password.setTransformationMethod(new PasswordTransformationMethod());
                    pass_visible = true;
                }
                break;
        }
    }

    public boolean checkSecurityCode() {

        String JSON_URL_TOKEN_DASHBOARD_COUNT = "https://tms.velarudh.com/test/json/dash_info.php";


        try {
            // Volley's json array request object
            StringRequest req2 = new StringRequest(JSON_URL_TOKEN_DASHBOARD_COUNT,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("MainActivity", response.toString());

                            try {
                                if (response.length() > 0) {
                                    if (response.toString().equalsIgnoreCase("yes"))
                                        valid_codeCheck = true;
                                } else {
                                    Snackbar snackbar = Snackbar.make(start_verification_layout, "Enter correct security code", Snackbar.LENGTH_SHORT);
                                    snackbar.show();
                                }
                            } catch (Exception e) {
                                Log.d("MainActivity", "onResponse: " + e);
                                Snackbar snackbar = Snackbar.make(start_verification_layout, "Server error! please try after some time", Snackbar.LENGTH_SHORT);
                                snackbar.show();
//
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("MainActivity", "Server Error: " + error.getMessage());
                    Snackbar snackbar = Snackbar.make(start_verification_layout, "Server error! please try after some time", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            int socketTimeout = 600000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req2.setRetryPolicy(policy);

            requestQueue.add(req2);

        } catch (Exception e) {
            Log.d("MainActivity", "getCustomer: " + e);
            Snackbar snackbar = Snackbar.make(start_verification_layout, "Server error! please try after some time", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        return valid_codeCheck;
    }

    public boolean Validation() {

        boolean valid = true;

        login_id = input_email.getText().toString().trim();
        password = input_password.getText().toString().trim();

        if (login_id.isEmpty()) {
            input_email.setError("Field can't be empty");
            valid = false;
        } else {
            input_email.setError(null);
        }

        if (password.isEmpty()) {
            input_password.setError("Field can't be empty");
            valid = false;
        } else {
            input_password.setError(null);
        }

        return valid;
    }

    public void checkLogIn() {

        String JSON_URL_TOKEN_LOGIN = BASE_URL + "login.php?user_id=" + login_id + "&password=" + password;

        Log.d(TAG, "checkLogIn: " + JSON_URL_TOKEN_LOGIN);

        try {
            // Volley's json array request object
            StringRequest req2 = new StringRequest(JSON_URL_TOKEN_LOGIN,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("MainActivity", response.toString());

                            try {
                                if (response.length() > 0) {
                                    LogInReturnJson logInReturnJson = new LogInReturnJson(response.toString(), getApplicationContext(), login_id, password);
                                    valid_LoginCheck = logInReturnJson.LogInReturnJson();
                                    if (valid_LoginCheck) {
                                        Log.d(TAG, "onResponse: token " + new FireBaseSessionManager(getApplicationContext()).getUserToken());
                                        if (new FireBaseSessionManager(getApplicationContext()).getUserToken() == null) {
                                            FirebaseInstanceId.getInstance().getToken();
                                        }
                                        usm.setUserLogIn(true);
                                        Intent in = new Intent(getApplicationContext(), Dashboard.class);
                                        startActivity(in);
                                        finish();
                                    }
                                } else {
                                    Snackbar snackbar = Snackbar.make(start_verification_layout, "Enter correct email or password", Snackbar.LENGTH_SHORT);
                                    snackbar.show();
                                }
                            } catch (Exception e) {
                                Log.d("MainActivity", "onResponse: " + e);
                                Snackbar snackbar = Snackbar.make(start_verification_layout, "Server error! please try after some time", Snackbar.LENGTH_SHORT);
                                snackbar.show();

//
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("MainActivity", "Server Error: " + error.getMessage());
                    Snackbar snackbar = Snackbar.make(start_verification_layout, "Server error! please try after some time", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            int socketTimeout = 600000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req2.setRetryPolicy(policy);

            requestQueue.add(req2);

        } catch (Exception e) {
            Log.d("MainActivity", "getCustomer: " + e);
            Snackbar snackbar = Snackbar.make(start_verification_layout, "Server error! please try after some time", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

    }

    public void ForgetPasswordinit() {

        dialog_forget_password = new Dialog(this);
        dialog_forget_password.setCancelable(true);
        dialog_forget_password.setCanceledOnTouchOutside(true);
        dialog_forget_password.setContentView(R.layout.foreget_password_dialog);

        mail_layout = (RelativeLayout) dialog_forget_password.findViewById(R.id.mail_layout);
        otp_layout = (RelativeLayout) dialog_forget_password.findViewById(R.id.otp_layout);
        password_layout = (LinearLayout) dialog_forget_password.findViewById(R.id.password_layout);

        mail_layout.setVisibility(View.VISIBLE);
        otp_layout.setVisibility(View.GONE);
        password_layout.setVisibility(View.GONE);

        email_for_otp = (EditText) dialog_forget_password.findViewById(R.id.email_for_otp);
        submit_email = (Button) dialog_forget_password.findViewById(R.id.submit_email);
        Button cancel_email = (Button) dialog_forget_password.findViewById(R.id.cancel_email);

        otp = (EditText) dialog_forget_password.findViewById(R.id.otp);
        submit_otp = (Button) dialog_forget_password.findViewById(R.id.submit_otp);
        Button cancel_otp = (Button) dialog_forget_password.findViewById(R.id.cancel_otp);

        new_password = (EditText) dialog_forget_password.findViewById(R.id.new_password);
        confirm_password = (EditText) dialog_forget_password.findViewById(R.id.confirm_password);
        password_f_visible = (ImageButton) dialog_forget_password.findViewById(R.id.password_f_visible);
        password_f_visible_confirm = (ImageButton) dialog_forget_password.findViewById(R.id.password_f_visible_confirm);
        submit_password = (Button) dialog_forget_password.findViewById(R.id.submit_password);
        Button cancel_password = (Button) dialog_forget_password.findViewById(R.id.cancel_password);

        submit_email.setOnClickListener(this);
        submit_otp.setOnClickListener(this);
        password_f_visible.setOnClickListener(this);
        password_f_visible_confirm.setOnClickListener(this);
        submit_password.setOnClickListener(this);

        cancel_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_forget_password.dismiss();
            }
        });

        cancel_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_forget_password.dismiss();
            }
        });

        cancel_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_forget_password.dismiss();
            }
        });

        dialog_forget_password.show();
        Window window = dialog_forget_password.getWindow();
        window.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
    }

    public boolean EmailValidation() {

        boolean valid = true;

        email_f_value = email_for_otp.getText().toString().trim();

        if (email_f_value.isEmpty()) {
            valid = false;
            email_for_otp.setError("Enter email to get otp");
        } else if (!email_f_value.matches(emailPattern)) {
            valid = false;
            email_for_otp.setError("Enter correct email");
        } else {
            email_for_otp.setError(null);
        }

        return valid;
    }

    public void SendEmailForOtp() {

        String JSON_URL_TOKEN_FORGOT_PASSWORD = BASE_URL + "forget_password.php?email_id=" + email_f_value;

        try {
            Log.d(TAG, "onClick:   json " + JSON_URL_TOKEN_FORGOT_PASSWORD);
            StringRequest req2 = new StringRequest(JSON_URL_TOKEN_FORGOT_PASSWORD,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("SendEmailForOtp", response.toString());

                            try {
                                if (response.length() > 0) {

                                    ForgetPasswordReturnJson fpr = new ForgetPasswordReturnJson(response.toString());
                                    ArrayList<ForgetPasswordDetails> fp = fpr.ForgetPasswordReturnJson();

                                    if (fp.get(0).getStatus().equalsIgnoreCase("1")) {
                                        otp_get_value = fp.get(0).getOtp();
                                        mail_layout.setVisibility(View.GONE);
                                        otp_layout.setVisibility(View.VISIBLE);
                                        Snackbar snackbar = Snackbar.make(mail_layout, "OTP has been sent to your email id...", Snackbar.LENGTH_SHORT);
                                        snackbar.show();
                                    } else if (fp.get(0).getStatus().equalsIgnoreCase("0")) {
                                        Snackbar snackbar = Snackbar.make(mail_layout, "Invalid email id", Snackbar.LENGTH_SHORT);
                                        snackbar.show();
                                    } else {
                                        Snackbar snackbar = Snackbar.make(mail_layout, "Server error!please try after some time", Snackbar.LENGTH_SHORT);
                                        snackbar.show();
                                    }

                                } else {

                                }
                            } catch (Exception e) {
                                Log.d("SendEmailForOtp", "onResponse: " + e);
                                Snackbar snackbar = Snackbar.make(mail_layout, "Server error!please try after some time", Snackbar.LENGTH_SHORT);
                                snackbar.show();
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("SendEmailForOtp", "Server Error: " + error.getMessage());
                    Snackbar snackbar = Snackbar.make(mail_layout, "Server error!please try after some time", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            int socketTimeout = 600000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req2.setRetryPolicy(policy);

            requestQueue.add(req2);

        } catch (Exception e) {
            Log.d("SendEmailForOtp", "getCustomer: " + e);
            Snackbar snackbar = Snackbar.make(mail_layout, "Server error!please try after some time", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
    }

    public boolean OtpValidation() {

        boolean valid = true;

        String otpvalue = otp.getText().toString().trim();

        if (otpvalue.isEmpty()) {
            valid = false;
            otp.setError("Enter otp");
        } else if (!otpvalue.equals(otp_get_value)) {
            valid = false;
            otp.setError("Enter correct otp");
        } else {
            otp.setError(null);
        }

        return valid;
    }

    public boolean PasswordValidation() {

        boolean valid = true;

        String confirm_pass_value = confirm_password.getText().toString().trim();
        password_value = new_password.getText().toString().trim();

        if (password_value.isEmpty()) {
            valid = false;
            new_password.setError("Enter password");
        } else if (confirm_pass_value.isEmpty()) {
            valid = false;
            confirm_password.setError("Re-Enter password");
        } else if (!confirm_pass_value.equals(password_value)) {
            valid = false;
            confirm_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            confirm_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error, 0);
        } else {
            confirm_password.setError(null);
            new_password.setError(null);
            confirm_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            confirm_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_correct, 0);
        }

        return valid;
    }

    public void SendPasswordToChange() {

        String JSON_URL_TOKEN_CHANGE_PASSWORD = BASE_URL + "update_password.php?email_id=" + email_f_value + "&password=" + password_value;


        try {
            Log.d(TAG, "onClick:   json " + JSON_URL_TOKEN_CHANGE_PASSWORD);
            StringRequest req2 = new StringRequest(JSON_URL_TOKEN_CHANGE_PASSWORD,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("SendEmailForOtp", response.toString());

                            try {
                                if (response.length() > 0) {

                                    if (response.toString().equalsIgnoreCase("1")) {
                                        Snackbar snackbar = Snackbar.make(password_layout, "Password has been changed", Snackbar.LENGTH_SHORT);
                                        snackbar.show();
                                        password_layout.setVisibility(View.GONE);
                                        dialog_forget_password.dismiss();
                                    }

                                } else {

                                }
                            } catch (Exception e) {
                                Log.d("SendEmailForOtp", "onResponse: " + e);
                                Snackbar snackbar = Snackbar.make(mail_layout, "Server error!please try after some time", Snackbar.LENGTH_SHORT);
                                snackbar.show();
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("SendEmailForOtp", "Server Error: " + error.getMessage());
                    Snackbar snackbar = Snackbar.make(mail_layout, "Server error!please try after some time", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            int socketTimeout = 600000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req2.setRetryPolicy(policy);

            requestQueue.add(req2);

        } catch (Exception e) {
            Log.d("SendEmailForOtp", "getCustomer: " + e);
            Snackbar snackbar = Snackbar.make(mail_layout, "Server error!please try after some time", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
    }

    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected(this);
        return isConnected;
    }

    @Override
    public void onBackPressed() {
        if (dialog_forget_password != null && dialog_forget_password.isShowing()) {
            dialog_forget_password.dismiss();
        } else {
            finishAffinity();
        }
    }
}
