package com.velarudh.tms.task;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.velarudh.tms.R;
import com.velarudh.tms.firebase.OnlineUsersAvailable;
import com.velarudh.tms.main.Dashboard;
import com.velarudh.tms.model.TaskDetails;
import com.velarudh.tms.util.ConnectivityReceiver;
import com.velarudh.tms.util.CountDownTimerView;
import com.velarudh.tms.util.ExpandableTextView;
import com.velarudh.tms.util.ExpandableTextViewTwo;
import com.velarudh.tms.util.NetErrorActivity;
import com.velarudh.tms.util.Url;
import com.velarudh.tms.util.UserSessionManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Task_details extends AppCompatActivity implements View.OnClickListener {

    String TAG = "Task_details";

    Toolbar toolbar;
    UserSessionManager usm;
    HashMap<String, String> user_details;
    String U_CAN_ASSIGN = "user_can_assign";
    String U_EMP_ID = "user_id";
    String user_can_assign;
    String user_emp_id;

    TextView project;
    TextView subtask;
    TextView taskassign;
    TextView taskcreate;
    TextView taskstart;
    TextView taskend;
    TextView taskremain;
    TextView taskprior;
    TextView tasktype;
    TextView taskdetail;


    ExpandableTextViewTwo details_project_name;
    TextView details_task_name;

    LinearLayout sub_task_layout;
    TextView details_sub_task_name;
    TextView details_task_end_date;
    TextView details_task_assignd_by;
    TextView details_task_created_date;
    TextView details_task_start_date;
    CountDownTimerView details_task_remain_time;
    TextView details_task_priority;
    TextView details_task_type;
    com.velarudh.tms.util.ExpandableTextView details_task_desc;

    ImageButton assign_task_others;
    Button date_extend_request;
    Button status;

    ArrayList<TaskDetails> select_task;

    FloatingActionButton fab_open;

    Intent in = null;

    Calendar newCalendar = Calendar.getInstance();

    String selected_status;

    EditText complete_hour;
    String completed_hour = "0";

    String extend_date_req;

    boolean status_change = false;

    String curr_date;

    ConnectivityReceiver myReceiver;

    String BASE_URL = Url.BASE_URL;

    Typeface bahu_thin;
    Typeface bahu_bold;


    SimpleDateFormat myFormat = new SimpleDateFormat("yyyy/MM/dd");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);

        select_task = getIntent().getParcelableArrayListExtra("select_task");

        if (checkConnection()) {
            init();
            if (select_task != null && !select_task.isEmpty())
                setData();
        } else {
            Intent in = new Intent(getApplicationContext(), NetErrorActivity.class);
            in.putExtra("layout_flag", "1");
            startActivity(in);
        }

    }

    public void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.title_activity_task_details);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        project = (TextView) findViewById(R.id.project);
        taskassign = (TextView) findViewById(R.id.taskassign);
        taskcreate = (TextView) findViewById(R.id.taskcreate);
        taskend = (TextView) findViewById(R.id.taskend);
        taskremain = (TextView) findViewById(R.id.taskremain);
        taskstart = (TextView) findViewById(R.id.taskstart);
        taskprior = (TextView) findViewById(R.id.taskprior);
        tasktype = (TextView) findViewById(R.id.tasktype);
        taskdetail = (TextView) findViewById(R.id.taskdetail);
        subtask = (TextView) findViewById(R.id.subtask);

        sub_task_layout = (LinearLayout) findViewById(R.id.sub_task_layout);

        details_project_name = (ExpandableTextViewTwo) findViewById(R.id.details_project_name);
        details_task_name = (TextView) findViewById(R.id.details_task_name);
        details_sub_task_name = (TextView) findViewById(R.id.details_sub_task_name);
        details_task_end_date = (TextView) findViewById(R.id.details_task__end_date);
        details_task_assignd_by = (TextView) findViewById(R.id.details_task_assignd_by);
        details_task_created_date = (TextView) findViewById(R.id.details_task_created_date);
        details_task_start_date = (TextView) findViewById(R.id.details_task_start_date);
        details_task_priority = (TextView) findViewById(R.id.details_task_priority);
        details_task_type = (TextView) findViewById(R.id.details_task_type);
        details_task_desc = (ExpandableTextView) findViewById(R.id.details_task_desc);
        details_task_remain_time = (CountDownTimerView) findViewById(R.id.details_task_remain_time);

        assign_task_others = (ImageButton) findViewById(R.id.assign_task_others);
        date_extend_request = (Button) findViewById(R.id.date_extend_request);
        status = (Button) findViewById(R.id.status);

        fab_open = (FloatingActionButton) findViewById(R.id.fab_open);

        assign_task_others.setOnClickListener(this);
        fab_open.setOnClickListener(this);
        date_extend_request.setOnClickListener(this);
        status.setOnClickListener(this);

        usm = new UserSessionManager(this);

        user_details = usm.getUserDetails();
        user_emp_id = user_details.get(U_EMP_ID);
        user_can_assign = user_details.get(U_CAN_ASSIGN);

        if(user_can_assign.equalsIgnoreCase("no"))
            assign_task_others.setVisibility(View.GONE);

        SetTypeFace();

    }

    public void SetTypeFace() {

        bahu_thin = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_thin.ttf");
        bahu_bold = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_bold.ttf");

        project.setTypeface(bahu_bold);
        taskassign.setTypeface(bahu_bold);
        taskcreate.setTypeface(bahu_bold);
        taskend.setTypeface(bahu_bold);
        taskremain.setTypeface(bahu_bold);
        taskstart.setTypeface(bahu_bold);
        taskdetail.setTypeface(bahu_bold);
        taskprior.setTypeface(bahu_bold);
        tasktype.setTypeface(bahu_bold);
        subtask.setTypeface(bahu_bold);

        details_project_name.setTypeface(bahu_thin);
        details_task_name.setTypeface(bahu_bold);
        details_sub_task_name.setTypeface(bahu_thin);
        details_task_end_date.setTypeface(bahu_thin);
        details_task_assignd_by.setTypeface(bahu_thin);
        details_task_created_date.setTypeface(bahu_thin);
        details_task_start_date.setTypeface(bahu_thin);
        details_task_desc.setTypeface(bahu_thin);
        details_task_remain_time.setTypeface(bahu_thin);
        details_task_priority.setTypeface(bahu_thin);
        details_task_type.setTypeface(bahu_thin);

        status.setTypeface(bahu_thin);
        date_extend_request.setTypeface(bahu_thin);
    }

    public void setData() {

        String Curr_date = myFormat.format(Calendar.getInstance().getTime());
        String Start_date = select_task.get(0).getTask_start_date();
        String task_end_ = select_task.get(0).getTask_end_date();

        Date StartDate = null;
        Date task_end = null;
        Date CurrentDate = null;

        try {
            StartDate = myFormat.parse(Start_date);
            CurrentDate = myFormat.parse(Curr_date);
            task_end = myFormat.parse(task_end_);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        details_project_name.setText(select_task.get(0).getProject_name());

        if (select_task.get(0).getTask_parent_id().equalsIgnoreCase("0")) {
            sub_task_layout.setVisibility(View.GONE);
            details_task_name.setText(select_task.get(0).getTask_name());
        } else {
            sub_task_layout.setVisibility(View.VISIBLE);
            details_sub_task_name.setText(select_task.get(0).getSub_task_name());
            details_task_name.setText(select_task.get(0).getTask_name());
        }

        details_task_start_date.setText(select_task.get(0).getTask_start_date());
        details_task_end_date.setText(select_task.get(0).getTask_end_date());
        details_task_assignd_by.setText(select_task.get(0).getTask_assign_by());
        details_task_created_date.setText(OnlyDate(select_task.get(0).getTask_create_date()));
        details_task_priority.setText(select_task.get(0).getTask_priority());
        details_task_type.setText(select_task.get(0).getTask_type());

        if (select_task.get(0).getTask_description().isEmpty())
            details_task_desc.setText("No details provided");
        else
            details_task_desc.setText(html2text(select_task.get(0).getTask_description()));

        if (CurrentDate.before(StartDate)) {
            Log.d(TAG, "setData: time date " + StartDate + "  curr  " + CurrentDate);
            taskremain.setText("will start in");
            long diff = StartDate.getTime() - CurrentDate.getTime();
            Log.d(TAG, "setData: time diff " + diff);
            details_task_remain_time.setTime(diff);
        } else {
            details_task_remain_time.setTime(diffInMilliSeconds(select_task.get(0).getTask_end_date()));
        }

        if (CurrentDate.after(task_end)) {
            assign_task_others.setVisibility(View.GONE);
        } else {
            assign_task_others.setVisibility(View.VISIBLE);
        }

        details_task_remain_time.startCountDown();
        details_task_remain_time.setOnTimerListener(new CountDownTimerView.TimerListener() {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                details_task_remain_time.setText("Time up!");
            }
        });


    }

    long diffInMilliSeconds(String end_date) {

        long diff = 0;
        Date end = null;
        Calendar cal = Calendar.getInstance();
        long curr_date = cal.getTimeInMillis();

        try {
            end = myFormat.parse(end_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long finish_date = end.getTime();

        diff = (finish_date - curr_date) + TimeUnit.DAYS.toMillis(1);

        return diff;
    }

    public static String html2text(String html) {
        return Html.fromHtml(html).toString();
    }

    public String OnlyDate(String date) {

        Log.d(TAG, "OnlyDate: " + date);
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss a", Locale.ENGLISH);

        Date date1 = null;
        try {
            date1 = myFormat.parse(date);
            Log.d(TAG, "OnlyDate: date1" + date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy/MM/dd");

        return newFormat.format(date1);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.fab_open:
                SetShortCutLayout();
                break;

            case R.id.date_extend_request:

                Log.d(TAG, "onClick:     date picker");
                new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        newCalendar.set(year, monthOfYear, dayOfMonth, 0, 0);

                        Date end_date_formatted = null;
                        Date select_date_formatted = null;

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
//                        String end_date = simpleDateFormat.format(select_task.get(0).getTask_end_date());
                        try {
                            end_date_formatted = simpleDateFormat.parse(select_task.get(0).getTask_end_date());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        String select_date = simpleDateFormat.format(newCalendar.getTime());
                        try {
                            select_date_formatted = simpleDateFormat.parse(select_date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        if (end_date_formatted.after(select_date_formatted)) {
                            Snackbar snackbar = Snackbar.make(findViewById(R.id.main_content), "Select a valid date", 1000);
                            snackbar.show();
                        } else {
                            DateExtendAlert(newCalendar);
                        }

                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;

            case R.id.status:

                StatusSubmitAlert();
                break;

            case R.id.assign_task_others:
                Intent in = new Intent(this, NewTask_SubTask.class);
                in.putExtra("sub_task", "yes");
                in.putParcelableArrayListExtra("select_task", select_task);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(in);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

        }
    }

    public void SetShortCutLayout() {

        LayoutInflater inflate = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
        Snackbar snackbar = Snackbar.make(findViewById(R.id.main_content), "", 1000);
// Get the Snackbar's layout view
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);
        layout.setBackgroundColor(Color.parseColor("#3581a1"));
// Inflate our custom view
        View snackView = inflate.inflate(R.layout.snackbar_layout, null);
// Configure the view

        RelativeLayout dashboard = (RelativeLayout) snackView.findViewById(R.id.dashboard);
        RelativeLayout your_task = (RelativeLayout) snackView.findViewById(R.id.your_task);
        RelativeLayout all_task = (RelativeLayout) snackView.findViewById(R.id.all_task);
        RelativeLayout group_chat = (RelativeLayout) snackView.findViewById(R.id.group_chat);

        dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                in = new Intent(getApplicationContext(), Dashboard.class);
                startActivity(in);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        your_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                in = new Intent(getApplicationContext(), SeeYourAllTasks.class);
                startActivity(in);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        all_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                in = new Intent(getApplicationContext(), ManageAllTask.class);
                startActivity(in);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        group_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                in = new Intent(getApplicationContext(), OnlineUsersAvailable.class);
//                startActivity(in);
//                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });
// Add the view to the Snackbar's layout
        layout.addView(snackView, 0);
// Show the Snackbar
        snackbar.show();
    }

    public void DateExtendAlert(Calendar calendar) {

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd");

        extend_date_req = dateFormatter.format(calendar.getTime());

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.date_extenddialog_layout);

        TextView extend_date = (TextView) dialog.findViewById(R.id.extend_date);
        Button cancel = (Button) dialog.findViewById(R.id.cancel);
        Button send = (Button) dialog.findViewById(R.id.send);

        extend_date.setTypeface(bahu_bold);
        cancel.setTypeface(bahu_thin);
        send.setTypeface(bahu_thin);

        extend_date.setText(extend_date_req);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String JSON_URL_TOKEN_DATE_EXTEND = BASE_URL + "date_extend_request.php?user_id=" + user_emp_id + "&task_id=" + select_task.get(0).getTask_id() + "&assign_by_id=" + select_task.get(0).getTask_assign_by_id() + "&extend_date=" + extend_date_req;


                Log.d(TAG, "DateExtendAlert json : " + JSON_URL_TOKEN_DATE_EXTEND);

                try {
                    // Volley's json array request object
                    StringRequest req2 = new StringRequest(JSON_URL_TOKEN_DATE_EXTEND,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("DateExtendAlert", response.toString());

                                    try {
                                        if (response.length() > 0) {
                                            if (response.equalsIgnoreCase("1")) {
                                                Toast.makeText(getApplicationContext(), "Your request for the date extension has been send ", Toast.LENGTH_SHORT).show();
                                                dialog.dismiss();
                                            } else {
                                                Toast.makeText(getApplicationContext(), "Server error! Try later", Toast.LENGTH_SHORT).show();
                                                dialog.dismiss();
                                            }


                                        } else {
                                            Toast.makeText(getApplicationContext(), "Server error! Try later", Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                        }
                                    } catch (Exception e) {
                                        Log.d("DateExtendAlert", "onResponse: " + e);
                                        Toast.makeText(getApplicationContext(), "Server error! Try later", Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();
                                    }
                                }

                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(getApplicationContext(), "Server error! Try later", Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();

                                }
                            });

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                    int socketTimeout = 600000;//30 seconds - change to what you want
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    req2.setRetryPolicy(policy);

                    requestQueue.add(req2);


                } catch (Exception e) {
                    Log.d("DateExtendAlert", "exception: " + e);
                    Toast.makeText(getApplicationContext(), "Server error! Try later", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }


            }
        });

        dialog.show();
    }

    public void StatusSubmitAlert() {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss a");
        Calendar calendar = Calendar.getInstance();
        curr_date = simpleDateFormat.format(calendar.getTime());

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.status_dialog_layout);

        final RadioButton pending = (RadioButton) dialog.findViewById(R.id.pending);

        final RadioButton going_on = (RadioButton) dialog.findViewById(R.id.going_on);

        final RadioButton stand_by = (RadioButton) dialog.findViewById(R.id.stand_by);

        final RadioButton new_open = (RadioButton) dialog.findViewById(R.id.new_open);

        final RadioButton completed = (RadioButton) dialog.findViewById(R.id.completed);

        complete_hour = (EditText) dialog.findViewById(R.id.complete_hour);

        pending.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (pending.isChecked()) {
                    selected_status = pending.getText().toString().trim();
                    complete_hour.setVisibility(View.GONE);
                    status_change = true;
                }
            }
        });
        going_on.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (going_on.isChecked()) {
                    selected_status = going_on.getText().toString().trim();
                    complete_hour.setVisibility(View.GONE);
                    status_change = true;
                }
            }
        });
        stand_by.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (stand_by.isChecked()) {
                    selected_status = stand_by.getText().toString().trim();
                    complete_hour.setVisibility(View.GONE);
                    status_change = true;
                }
            }
        });
        new_open.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (new_open.isChecked()) {
                    selected_status = new_open.getText().toString().trim();
                    complete_hour.setVisibility(View.GONE);
                    status_change = true;
                }
            }
        });
        completed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                Log.d(TAG, "onCheckedChanged: " + completed.isChecked());
                if (completed.isChecked()) {
                    selected_status = completed.getText().toString().trim();
                    complete_hour.setVisibility(View.VISIBLE);
                }
            }
        });

        Button cancel = (Button) dialog.findViewById(R.id.cancel);
        Button send = (Button) dialog.findViewById(R.id.send);


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                status_change = false;
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (completed.isChecked()) {
                    completed_hour = complete_hour.getText().toString().trim();

                    if (!completed_hour.equalsIgnoreCase("0") && !completed_hour.isEmpty()) {
                        status_change = true;
                    } else {
                        complete_hour.setError("Enter ");
                        status_change = false;
                    }
                }

                if (status_change) {

                    String JSON_URL_TOKEN_STATUS_CHANGE = BASE_URL + "status_change_request.php?user_id=" + user_emp_id + "&task_id=" + select_task.get(0).getTask_id() + "&status=" + selected_status;
//                    String JSON_URL_TOKEN_STATUS_CHANGE = "https://tms.velarudh.com/test/json/status_change_request.php?user_id=" + user_emp_id + "&task_id=" + select_task.get(0).getTask_id()+ "&status=" + selected_status + "&completed_hour=" + completed_hour + "&date=" + curr_date;

                    Log.d(TAG, "getData: " + JSON_URL_TOKEN_STATUS_CHANGE);

                    try {
                        // Volley's json array request object
                        StringRequest req2 = new StringRequest(JSON_URL_TOKEN_STATUS_CHANGE,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Log.d("getTaskDetailsData", response.toString());

                                        try {
                                            if (response.length() > 0) {
                                                if (response.equalsIgnoreCase("1")) {
                                                    Toast.makeText(getApplicationContext(), "Your request to update your task status has been sent", Toast.LENGTH_SHORT).show();
                                                    dialog.dismiss();
                                                    Intent in = new Intent(getApplicationContext(), Dashboard.class);
                                                    startActivity(in);
                                                    finish();
                                                } else {
                                                    Toast.makeText(getApplicationContext(), "Server error! Try later", Toast.LENGTH_SHORT).show();
                                                    dialog.dismiss();
                                                }


                                            } else {
                                                Toast.makeText(getApplicationContext(), "Server error! Try later", Toast.LENGTH_SHORT).show();
                                                dialog.dismiss();
                                            }
                                        } catch (Exception e) {
                                            Log.d("getTaskDetailsData", "onResponse: " + e);
                                            Toast.makeText(getApplicationContext(), "Server error! Try later", Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                        }
                                    }

                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(getApplicationContext(), "Server error! Try later", Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();

                                    }
                                });

                        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                        int socketTimeout = 600000;//30 seconds - change to what you want
                        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                        req2.setRetryPolicy(policy);

                        requestQueue.add(req2);


                    } catch (Exception e) {
                        Log.d("getTaskDetailsData", "exception: " + e);
                        Toast.makeText(getApplicationContext(), "Server error! Try later", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

            }
        });

        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        myReceiver = new ConnectivityReceiver();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(myReceiver);
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected(getApplicationContext());
        return isConnected;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}


