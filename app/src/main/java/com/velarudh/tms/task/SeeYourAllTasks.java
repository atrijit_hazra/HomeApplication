package com.velarudh.tms.task;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.unnamed.b.atv.model.TreeNode;
import com.velarudh.tms.R;
import com.velarudh.tms.adapter.Dashboard_Alltask_adapter;
import com.velarudh.tms.json.TaskDetailsReturnJson;
import com.velarudh.tms.main.Dashboard;
import com.velarudh.tms.model.TaskDetails;
import com.velarudh.tms.util.ConnectivityReceiver;
import com.velarudh.tms.util.NetErrorActivity;
import com.velarudh.tms.util.Url;
import com.velarudh.tms.util.UserSessionManager;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class SeeYourAllTasks extends AppCompatActivity implements View.OnClickListener {

    String TAG = "SeeYourAllTasks";

    Toolbar toolbar;

    FloatingActionButton fab_open;
    SwipeRefreshLayout swipe_layout;
    RecyclerView alltask_project_recycler;
    RelativeLayout container;
    ArrayList<TaskDetails> taskDetails;
    TextView no_task;
    Dashboard_Alltask_adapter dash_adapter;

    ConnectivityReceiver myReceiver;

    AlertDialog levelDialog = null;

    int select_item_filter = 0;

    ArrayList<String> project_type;
    ArrayList<String> project_name;
    ArrayList<TaskDetails> filter_result;
    String select_value;

    String BASE_URL = Url.BASE_URL;

    SearchView searchView;

    String U_EMP_ID = "user_id";
    String U_CAN_ASSIGN = "user_can_assign";
    String user_emp_id;
    String user_can_assign;

    Intent in_shortcut = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_all_tasks);

        init();

        if (checkConnection()) {
            swipe_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    GetData();
                }
            });
        } else {
            Intent in = new Intent(getApplicationContext(), NetErrorActivity.class);
            in.putExtra("layout_flag", "1");
            startActivity(in);

        }
    }

    public void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.title_activity_see_all_tasks);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        fab_open = (FloatingActionButton) findViewById(R.id.fab_open);

        swipe_layout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        container = (RelativeLayout) findViewById(R.id.container);
        alltask_project_recycler = (RecyclerView) findViewById(R.id.alltask_project_recycler);
        no_task = (TextView) findViewById(R.id.no_task);

        Typeface Bahu_thin = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_thin.ttf");
        no_task.setTypeface(Bahu_thin);

        UserSessionManager usm = new UserSessionManager(this);
        HashMap<String, String> user_details = usm.getUserDetails();
        user_emp_id = user_details.get(U_EMP_ID);
        user_can_assign = user_details.get(U_CAN_ASSIGN);

        fab_open.setOnClickListener(this);
        GetData();
    }

    public void GetData() {

        String JSON_URL_TOKEN_TAK_DETAILS = BASE_URL + "user_all_task_details.php?user_id=" + user_emp_id;


        Log.d(TAG, "getData: " + JSON_URL_TOKEN_TAK_DETAILS);

        try {
            // Volley's json array request object
            JsonArrayRequest req2 = new JsonArrayRequest(JSON_URL_TOKEN_TAK_DETAILS,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d("getTaskDetailsData", response.toString());

                            try {

                                TaskDetailsReturnJson tpc = new TaskDetailsReturnJson(response.toString());
                                taskDetails = tpc.TaskDetailsReturnJson();

                                swipe_layout.setRefreshing(false);
                                setData();

                            } catch (Exception e) {
                                Log.d("getTaskDetailsData", "onResponse: " + e);
                                swipe_layout.setRefreshing(false);
//                                setData();
                            }
                        }

                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Log.e("getTaskDetailsData", "Server Error: " + error.getMessage());
                            swipe_layout.setRefreshing(false);
//                            setData();
                        }
                    });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            int socketTimeout = 600000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req2.setRetryPolicy(policy);

            requestQueue.add(req2);


        } catch (Exception e) {
            Log.d("getTaskDetailsData", "exception: " + e);
        }
    }

    public void setData() {

        if (taskDetails.isEmpty()) {
            no_task.setVisibility(View.VISIBLE);
            alltask_project_recycler.setVisibility(View.GONE);
        } else {

            no_task.setVisibility(View.GONE);
            alltask_project_recycler.setVisibility(View.VISIBLE);

            dash_adapter = new Dashboard_Alltask_adapter(this, taskDetails, user_can_assign);
            alltask_project_recycler.setAdapter(dash_adapter);

            GridLayoutManager lm = new GridLayoutManager(this, 2);
            alltask_project_recycler.setLayoutManager(lm);

            DividerItemDecoration horizontalDecoration = new DividerItemDecoration(alltask_project_recycler.getContext(),
                    DividerItemDecoration.VERTICAL);
            Drawable horizontalDivider = ContextCompat.getDrawable(this, R.drawable.recycler_decoration);
            horizontalDecoration.setDrawable(horizontalDivider);

            alltask_project_recycler.addItemDecoration(horizontalDecoration);
        }

    }

    @Override
    public void onBackPressed() {

        if (searchView != null) {
            Log.d(TAG, "onBackPressed: " + searchView.isIconified());
            if (!searchView.isIconified()) {
                searchView.clearFocus();
                searchView.onActionViewCollapsed();
            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        myReceiver = new ConnectivityReceiver();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(myReceiver);
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected(getApplicationContext());
        return isConnected;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_filter, menu);

        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.search));
        searchView.setQueryHint("Search by task name");
//        searchView.setIconifiedByDefault(true);
        searchView.bringToFront();
        searchView.setClickable(true);

        try {
            searchView.setOnSearchClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                @Override
                public boolean onQueryTextSubmit(String query) {

                    if (!taskDetails.isEmpty()) {
                        if (TextUtils.isEmpty(query)) {
                            dash_adapter.filter("");
                        } else {
                            dash_adapter.filter(query);
                        }
                    }
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String s) {

                    if (!taskDetails.isEmpty()) {
                        if (TextUtils.isEmpty(s)) {
                            dash_adapter.filter("");
                        } else {
                            dash_adapter.filter(s);
                        }
                    }
                    return true;
                }
            });

            searchView.setOnCloseListener(new SearchView.OnCloseListener() {
                @Override
                public boolean onClose() {
                    searchView.clearFocus();
                    searchView.onActionViewCollapsed();
                    return false;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.filter_project_type:

                project_type = new ArrayList<>();

                project_type.add("See All");

                for (TaskDetails td : taskDetails) {
                    if (project_type.isEmpty()) {
                        project_type.add(td.getProject_type());
                    } else {
                        if (!project_type.contains(td.getProject_type())) {
                            project_type.add(td.getProject_type());
                        }
                    }
                }

                final CharSequence[] type = project_type.toArray(new CharSequence[project_type.size()]);

                // Creating and Building the Dialog
                AlertDialog.Builder builder_type = new AlertDialog.Builder(this, R.style.MyDialogTheme);
                builder_type.setTitle("Select project Type");

                builder_type.setSingleChoiceItems(type, select_item_filter,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int i) {
                                select_item_filter = i;

                                filter_result = new ArrayList<TaskDetails>();
                                select_value = project_type.get(i);

                                if (!select_value.equalsIgnoreCase("See All")) {
                                    for (TaskDetails td : taskDetails) {
                                        if (td.getProject_type().equalsIgnoreCase(select_value)) {
                                            filter_result.add(td);
                                        }
                                    }
                                    SetFilterResult(filter_result);

                                } else {
                                    SetFilterResult(taskDetails);
                                }

                                levelDialog.dismiss();
                            }
                        });
                levelDialog = builder_type.create();
                levelDialog.show();

                return true;

            case R.id.filter_project_name:

                project_name = new ArrayList<>();

                project_name.add("See All");

                for (TaskDetails td : taskDetails) {
                    if (project_name.isEmpty()) {
                        project_name.add(td.getProject_name());
                    } else {
                        if (!project_name.contains(td.getProject_name())) {
                            project_name.add(td.getProject_name());
                        }
                    }
                }

                final CharSequence[] name = project_name.toArray(new CharSequence[project_name.size()]);

                // Creating and Building the Dialog
                AlertDialog.Builder builder_name = new AlertDialog.Builder(this, R.style.MyDialogTheme);
                builder_name.setTitle("Select project name");

                builder_name.setSingleChoiceItems(name, select_item_filter,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int i) {
                                select_item_filter = i;

                                filter_result = new ArrayList<TaskDetails>();
                                select_value = project_name.get(i);

                                if (!select_value.equalsIgnoreCase("See All")) {
                                    for (TaskDetails td : taskDetails) {
                                        if (td.getProject_name().equalsIgnoreCase(select_value)) {
                                            filter_result.add(td);
                                        }
                                    }
                                    SetFilterResult(filter_result);

                                } else {
                                    SetFilterResult(taskDetails);
                                }

                                levelDialog.dismiss();
                            }
                        });
                levelDialog = builder_name.create();
                levelDialog.show();

                return true;

            default:

                return super.onOptionsItemSelected(item);
        }

    }

    public void SetFilterResult(ArrayList<TaskDetails> filter_result) {
        dash_adapter = new Dashboard_Alltask_adapter(this, filter_result, user_can_assign);
//        alltask_project_recycler.setAdapter(dash_adapter);

        GridLayoutManager lm = new GridLayoutManager(this, 2);
//        alltask_project_recycler.setLayoutManager(lm);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.fab_open:
                SetShortCutLayout();
                break;
        }
    }

    public void SetShortCutLayout() {

        LayoutInflater inflate = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
        Snackbar snackbar = Snackbar.make(findViewById(R.id.content_see_all_tasks), "", 1000);
// Get the Snackbar's layout view
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);
        layout.setBackgroundColor(Color.parseColor("#3581a1"));
// Inflate our custom view
        View snackView = inflate.inflate(R.layout.snackbar_layout, null);
// Configure the view

        RelativeLayout dashboard = (RelativeLayout) snackView.findViewById(R.id.dashboard);
        RelativeLayout your_task = (RelativeLayout) snackView.findViewById(R.id.your_task);
        RelativeLayout all_task = (RelativeLayout) snackView.findViewById(R.id.all_task);
        RelativeLayout group_chat = (RelativeLayout) snackView.findViewById(R.id.group_chat);

        dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                in_shortcut = new Intent(getApplicationContext(), Dashboard.class);
                startActivity(in_shortcut);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        your_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                in_shortcut = new Intent(getApplicationContext(), NewTask_SubTask.class);
                in_shortcut.putExtra("sub_task", "no");
                startActivity(in_shortcut);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        all_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                in_shortcut = new Intent(getApplicationContext(), ManageAllTask.class);
                startActivity(in_shortcut);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        group_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
// Add the view to the Snackbar's layout
        layout.addView(snackView, 0);
// Show the Snackbar
        snackbar.show();
    }

}
