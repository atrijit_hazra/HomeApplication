package com.velarudh.tms.task;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.velarudh.tms.R;
import com.velarudh.tms.adapter.Date_Extend_adapter;
import com.velarudh.tms.json.DateExtendReturnJson;
import com.velarudh.tms.json.ProjectDetailsReturnJson;
import com.velarudh.tms.main.Dashboard;
import com.velarudh.tms.model.DateExtendDetails;
import com.velarudh.tms.util.ConnectivityReceiver;
import com.velarudh.tms.util.NetErrorActivity;
import com.velarudh.tms.util.Url;
import com.velarudh.tms.util.UserSessionManager;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

public class DateExtend extends AppCompatActivity implements View.OnClickListener {

    String TAG = "DateExtend";
    Toolbar toolbar;
    FloatingActionButton fab;
    RecyclerView date_extend_recycler;
    TextView no_task;

    UserSessionManager usm;
    String U_EMP_ID = "user_id";
    HashMap<String, String> user_details;

    String user_id;

    ArrayList<DateExtendDetails> dateExtendDetailses;

    ConnectivityReceiver myReceiver;

    String BASE_URL = Url.BASE_URL;

    Intent in_shortcut = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_extend);

        if (checkConnection()) {
            init();
            getData();
        } else {
            Intent in = new Intent(getApplicationContext(), NetErrorActivity.class);
            in.putExtra("layout_flag", "1");
            startActivity(in);

        }


    }

    public void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.title_activity_date_extend);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab_open);
        date_extend_recycler = (RecyclerView) findViewById(R.id.date_extend_recycler);
        no_task = (TextView) findViewById(R.id.no_task);

        Typeface Bahu_thin = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_thin.ttf");
        no_task.setTypeface(Bahu_thin);

        usm = new UserSessionManager(this);
        user_details = usm.getUserDetails();
        user_id = user_details.get(U_EMP_ID);

        fab.setOnClickListener(this);
    }

    public void getData() {

        String JSON_URL_TOKEN_DATE_EXTEND = BASE_URL + "date_extend_data.php?user_id=" + user_id;

        Log.d(TAG, "getData: " + JSON_URL_TOKEN_DATE_EXTEND);

        try {
            // Volley's json array request object
            JsonArrayRequest req2 = new JsonArrayRequest(JSON_URL_TOKEN_DATE_EXTEND,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d("DateExtend", response.toString());
                            try {
                                DateExtendReturnJson dtr = new DateExtendReturnJson(response.toString());
                                dateExtendDetailses = dtr.DateExtendReturnJson();
                                setData();
                            } catch (Exception e) {
                                Log.d("DateExtend", "onResponse: " + e);
                            }
                        }

                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("DateExtend", "Server Error: " + error.getMessage());
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            int socketTimeout = 600000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req2.setRetryPolicy(policy);
            requestQueue.add(req2);
        } catch (Exception e) {
            Log.d("DateExtend", "exception: " + e);
        }
    }

    public void setData() {

        Log.d(TAG, "setData: " + dateExtendDetailses.isEmpty());
        if (dateExtendDetailses.isEmpty()) {
            no_task.setVisibility(View.VISIBLE);
            date_extend_recycler.setVisibility(View.GONE);
        } else {
            no_task.setVisibility(View.GONE);
            date_extend_recycler.setVisibility(View.VISIBLE);

            Date_Extend_adapter adapter = new Date_Extend_adapter(this, dateExtendDetailses);
            date_extend_recycler.setAdapter(adapter);

            LinearLayoutManager lm = new LinearLayoutManager(this);
            date_extend_recycler.setLayoutManager(lm);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        myReceiver = new ConnectivityReceiver();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        if (checkConnection()) {
            init();

            getData();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(myReceiver);
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected(getApplicationContext());
        return isConnected;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(this, Dashboard.class);
        startActivity(in);
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.fab_open:
                SetShortCutLayout();
                break;
        }
    }

    public void SetShortCutLayout() {

        LayoutInflater inflate = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
        Snackbar snackbar = Snackbar.make(findViewById(R.id.content_date_extend), "", 1000);
// Get the Snackbar's layout view
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);
        layout.setBackgroundColor(Color.parseColor("#3581a1"));
// Inflate our custom view
        View snackView = inflate.inflate(R.layout.snackbar_layout, null);
// Configure the view

        RelativeLayout dashboard = (RelativeLayout) snackView.findViewById(R.id.dashboard);
        RelativeLayout your_task = (RelativeLayout) snackView.findViewById(R.id.your_task);
        RelativeLayout all_task = (RelativeLayout) snackView.findViewById(R.id.all_task);
        RelativeLayout group_chat = (RelativeLayout) snackView.findViewById(R.id.group_chat);

        dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                in_shortcut = new Intent(getApplicationContext(), Dashboard.class);
                startActivity(in_shortcut);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        your_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                in_shortcut = new Intent(getApplicationContext(), SeeYourAllTasks.class);
                startActivity(in_shortcut);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        all_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                in_shortcut = new Intent(getApplicationContext(), ManageAllTask.class);
                startActivity(in_shortcut);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        group_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
// Add the view to the Snackbar's layout
        layout.addView(snackView, 0);
// Show the Snackbar
        snackbar.show();
    }
}
