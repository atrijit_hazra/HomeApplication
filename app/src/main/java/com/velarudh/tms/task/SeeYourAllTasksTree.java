package com.velarudh.tms.task;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;
import com.velarudh.tms.R;
import com.velarudh.tms.adapter.Dashboard_Alltask_adapter;
import com.velarudh.tms.adapter.ProjectandParentTaskItemHolder;
import com.velarudh.tms.adapter.TaskItemHolder;
import com.velarudh.tms.json.TaskDetailsReturnJson;
import com.velarudh.tms.main.Dashboard;
import com.velarudh.tms.model.TaskDetails;
import com.velarudh.tms.util.ConnectivityReceiver;
import com.velarudh.tms.util.NetErrorActivity;
import com.velarudh.tms.util.Url;
import com.velarudh.tms.util.UserSessionManager;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

public class SeeYourAllTasksTree extends AppCompatActivity implements View.OnClickListener {

    String TAG = "SeeYourAllTasks";

    Toolbar toolbar;

    FloatingActionButton fab_open;
    SwipeRefreshLayout swipe_layout;
    RelativeLayout container;
    ArrayList<TaskDetails> taskDetails;
    TextView no_task;

    ConnectivityReceiver myReceiver;

    String BASE_URL = Url.BASE_URL;


    String U_EMP_ID = "user_id";
    String U_CAN_ASSIGN = "user_can_assign";
    String user_emp_id;
    String user_can_assign;

    Intent in_shortcut = null;

    HashMap<String, ArrayList<TaskDetails>> projectSortTask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_all_tasks_tree);

        init();

        if (checkConnection()) {
            swipe_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    GetData();
                }
            });
        } else {
            Intent in = new Intent(getApplicationContext(), NetErrorActivity.class);
            in.putExtra("layout_flag", "1");
            startActivity(in);

        }
    }

    public void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.title_activity_see_all_tasks);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        fab_open = (FloatingActionButton) findViewById(R.id.fab_open);

        swipe_layout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        container = (RelativeLayout) findViewById(R.id.container);
//        alltask_project_recycler = (RecyclerView) findViewById(R.id.alltask_project_recycler);
        no_task = (TextView) findViewById(R.id.no_task);

        Typeface Bahu_thin = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_thin.ttf");
        no_task.setTypeface(Bahu_thin);

        UserSessionManager usm = new UserSessionManager(this);
        HashMap<String, String> user_details = usm.getUserDetails();
        user_emp_id = user_details.get(U_EMP_ID);
        user_can_assign = user_details.get(U_CAN_ASSIGN);

        fab_open.setOnClickListener(this);
        GetData();
    }

    public void GetData() {

        String JSON_URL_TOKEN_TAK_DETAILS = BASE_URL + "user_all_task_details.php?user_id=" + user_emp_id;


        Log.d(TAG, "getData: " + JSON_URL_TOKEN_TAK_DETAILS);

        try {
            // Volley's json array request object
            JsonArrayRequest req2 = new JsonArrayRequest(JSON_URL_TOKEN_TAK_DETAILS,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d("getTaskDetailsData", response.toString());

                            try {

                                TaskDetailsReturnJson tpc = new TaskDetailsReturnJson(response.toString());
                                taskDetails = tpc.TaskDetailsReturnJson();

                                swipe_layout.setRefreshing(false);
                                setData();

                            } catch (Exception e) {
                                Log.d("getTaskDetailsData", "onResponse: " + e);
                                swipe_layout.setRefreshing(false);
//                                setData();
                            }
                        }

                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Log.e("getTaskDetailsData", "Server Error: " + error.getMessage());
                            swipe_layout.setRefreshing(false);
//                            setData();
                        }
                    });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            int socketTimeout = 600000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req2.setRetryPolicy(policy);

            requestQueue.add(req2);


        } catch (Exception e) {
            Log.d("getTaskDetailsData", "exception: " + e);
        }
    }

    public void setData() {

        if (taskDetails.isEmpty()) {
            no_task.setVisibility(View.VISIBLE);
            container.setVisibility(View.GONE);
        } else {
            no_task.setVisibility(View.GONE);
            container.setVisibility(View.VISIBLE);
            setDataInTreeStructure();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        myReceiver = new ConnectivityReceiver();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(myReceiver);
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected(getApplicationContext());
        return isConnected;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.fab_open:
                SetShortCutLayout();
                break;
        }
    }

    public void SetShortCutLayout() {

        LayoutInflater inflate = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
        Snackbar snackbar = Snackbar.make(findViewById(R.id.content_see_all_tasks), "", 1000);
// Get the Snackbar's layout view
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);
        layout.setBackgroundColor(Color.parseColor("#3581a1"));
// Inflate our custom view
        View snackView = inflate.inflate(R.layout.snackbar_layout, null);
// Configure the view

        RelativeLayout dashboard = (RelativeLayout) snackView.findViewById(R.id.dashboard);
        RelativeLayout your_task = (RelativeLayout) snackView.findViewById(R.id.your_task);
        RelativeLayout all_task = (RelativeLayout) snackView.findViewById(R.id.all_task);
        RelativeLayout group_chat = (RelativeLayout) snackView.findViewById(R.id.group_chat);

        dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                in_shortcut = new Intent(getApplicationContext(), Dashboard.class);
                startActivity(in_shortcut);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        your_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                in_shortcut = new Intent(getApplicationContext(), NewTask_SubTask.class);
                in_shortcut.putExtra("sub_task", "no");
                startActivity(in_shortcut);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        all_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                in_shortcut = new Intent(getApplicationContext(), ManageAllTask.class);
                startActivity(in_shortcut);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        group_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
// Add the view to the Snackbar's layout
        layout.addView(snackView, 0);
// Show the Snackbar
        snackbar.show();
    }

    public void setDataInTreeStructure() {
        final TreeNode root = TreeNode.root();

        TreeNode project = null;
        TreeNode task;

        ArrayList<TaskDetails> taskSort;

        projectSortTask = new HashMap<>();
        for (TaskDetails td : taskDetails) {
            taskSort = new ArrayList<>();
            if (!projectSortTask.containsKey(td.getProject_name())) {
                taskSort.add(td);
                projectSortTask.put(td.getProject_name(), taskSort);
            } else {
                projectSortTask.get(td.getProject_name()).add(td);
            }

        }

        for (String projectName : projectSortTask.keySet()) {
            taskDetails = new ArrayList<>();
            taskDetails = projectSortTask.get(projectName);
            project = new TreeNode(new ProjectandParentTaskItemHolder.IconTreeItem(projectName, taskDetails.size())).setViewHolder(new ProjectandParentTaskItemHolder(this));
            for (int i = 0; i < taskDetails.size(); i++) {
                TaskDetails taskDetail = taskDetails.get(i);
                task = new TreeNode(new TaskItemHolder.IconTreeItem(taskDetail.getTask_name())).setViewHolder(new TaskItemHolder(this, taskDetail, this));
                project.addChild(task);
            }
            root.addChildren(project);
        }
        AndroidTreeView tView = new AndroidTreeView(this, root);
        tView.setDefaultAnimation(true);
        tView.setDefaultContainerStyle(R.style.TreeNodeStyleDivided, true);
        tView.setUseAutoToggle(true);
        container.addView(tView.getView());
    }

}
