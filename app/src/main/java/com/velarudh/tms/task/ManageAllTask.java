package com.velarudh.tms.task;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.velarudh.tms.R;
import com.velarudh.tms.adapter.Manage_AllTask_adapter;
import com.velarudh.tms.json.EmployeeDetailsReturnJson;
import com.velarudh.tms.json.ProjectDetailsReturnJson;
import com.velarudh.tms.json.TaskDetailsReturnJson;
import com.velarudh.tms.main.Dashboard;
import com.velarudh.tms.model.EmployeeDetails;
import com.velarudh.tms.model.ProjectDetails;
import com.velarudh.tms.model.ProjectType;
import com.velarudh.tms.model.TaskDetails;
import com.velarudh.tms.model.TaskTypeStatusList;
import com.velarudh.tms.util.ConnectivityReceiver;
import com.velarudh.tms.util.NetErrorActivity;
import com.velarudh.tms.util.Url;
import com.velarudh.tms.util.UserSessionManager;

import org.json.JSONArray;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class ManageAllTask extends AppCompatActivity implements View.OnClickListener {

    String TAG = "ManageAllTask";

    UserSessionManager usm;
    HashMap<String, String> user_details;
    String U_EMP_ID = "user_id";
    String user_emp_id;

    FloatingActionButton fab_open;
    ArrayList<TaskDetails> task_details;

    Toolbar toolbar;
    SwipeRefreshLayout swipe_layout;
    RecyclerView managetask_project_recycler;
    TextView no_task;
    ConnectivityReceiver myReceiver;

    String BASE_URL = Url.BASE_URL;

    BottomSheetBehavior behavior = null;
    View bottom_sheet_edit_task;

    ArrayList<String> task_priority_list;
    ArrayList<String> task_type_list;
    ArrayList<String> task_status_list;

    String parent_id;
    String task_name;
    String assign_to_id_selected;
    String selected_task_priority;
    String selected_task_status;
    String details = "";
    String start_date = "";
    String end_date = "";
    String selected_task_type;

    ArrayList<EmployeeDetails> employee_detail;
    ArrayList<String> employee_name;
    ArrayList<String> employee_id;

    LinearLayout sub_task_layout;
    RelativeLayout button_layout;
    TextView projetname;
    TextView selectassiby;
    TextView selectuser;
    TextView subtask;
    TextView taskname;
    TextView taskpriority;
    TextView tasktype;
    TextView taskstatus;
    TextView startdate;
    TextView endtdate;
    TextView taskdetails;

    TextView manage_project_name;
    TextView manage_assigned_by;
    TextView manage_user_name;
    TextView parent_task_name;
    EditText manage_task_name;
    TextView manage_priority_name;
    TextView manage_type_name;
    TextView manage_status_name;
    Button manage_start_date_dialog;
    Button manage_end_date_dialog;
    EditText create_task_desc;

    Spinner manage_assign_to_dialog;
    Spinner select_task_priority;
    Spinner select_task_type;
    Spinner select_status_type;

    Button submit;
    Button cancel;

    String JSON_URL_TOKEN_EDIT_TASK;
    String JSON_URL_TOKEN_DELETE_TASK;

    Calendar newCalendar;
    SimpleDateFormat dateFormatter;

    AlertDialog dialog2 = null;

    String TASK_ID;

    Intent in_shortcut = null;

    Typeface bahu_thin;
    Typeface bahu_bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_all_task);

        if (checkConnection()) {
            init();
            getAssignTaskDetailsData();
            swipe_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {

                    getAssignTaskDetailsData();
                }
            });
        } else {
            Intent in = new Intent(getApplicationContext(), NetErrorActivity.class);
            in.putExtra("layout_flag", "1");
            startActivity(in);

        }
    }

    public void init() {

        usm = new UserSessionManager(this);
        user_details = usm.getUserDetails();
        user_emp_id = user_details.get(U_EMP_ID);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.title_activity_manage_all_task);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        fab_open = (FloatingActionButton) findViewById(R.id.fab_open);
        swipe_layout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        managetask_project_recycler = (RecyclerView) findViewById(R.id.manageall_task_recycler);
        no_task = (TextView) findViewById(R.id.no_task);

        Typeface Bahu_thin = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_thin.ttf");
        no_task.setTypeface(Bahu_thin);

        newCalendar = Calendar.getInstance();
        dateFormatter = new SimpleDateFormat("yyyy/MM/dd");

        fab_open.setOnClickListener(this);

    }

    public void getAssignTaskDetailsData() {

        String JSON_URL_TOKEN_TAK_DETAILS = BASE_URL + "manage_task_details.php?user_id=" + user_emp_id;
        Log.d(TAG, "getData: " + JSON_URL_TOKEN_TAK_DETAILS);

        try {
            // Volley's json array request object
            JsonArrayRequest req2 = new JsonArrayRequest(JSON_URL_TOKEN_TAK_DETAILS,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d("getAssignTaskDeta", response.toString());

                            try {
                                TaskDetailsReturnJson tpc = new TaskDetailsReturnJson(response.toString());
                                task_details = tpc.TaskDetailsReturnJson();
                                swipe_layout.setRefreshing(false);
                                setData();

                            } catch (Exception e) {
                                Log.d("getAssignTaskDetsData", "onResponse: " + e);
                                swipe_layout.setRefreshing(false);
                                setData();
                            }
                        }

                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Log.e("getAssignTaskDetaita", "Server Error: " + error.getMessage());
                            swipe_layout.setRefreshing(false);
                            setData();
                        }
                    });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            int socketTimeout = 600000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req2.setRetryPolicy(policy);

            requestQueue.add(req2);


        } catch (Exception e) {
            Log.d("getAssignTaskDelsData", "exception: " + e);
        }
    }

    public void setData() {

        if (task_details.isEmpty()) {
            no_task.setVisibility(View.VISIBLE);
            managetask_project_recycler.setVisibility(View.GONE);
        } else {
            no_task.setVisibility(View.GONE);
            managetask_project_recycler.setVisibility(View.VISIBLE);

            Manage_AllTask_adapter dash_adapter = new Manage_AllTask_adapter(this, task_details, user_emp_id);
            managetask_project_recycler.setAdapter(dash_adapter);

            GridLayoutManager lm = new GridLayoutManager(this, 2);
            managetask_project_recycler.setLayoutManager(lm);

            managetask_project_recycler.setNestedScrollingEnabled(false);
        }
    }

    public void EditAssignTask(Integer i, String display_type) {

        bottom_sheet_edit_task = (RelativeLayout) findViewById(R.id.bottom_sheet_edit_task);
        behavior = BottomSheetBehavior.from(bottom_sheet_edit_task);

        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });

        EditInit();

        if (display_type.equalsIgnoreCase("View"))
            SetDetailsToView(i);
        else
            getAllUserDetails(i);

        manage_start_date_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ManageAllTask.this, new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        newCalendar.set(year, monthOfYear, dayOfMonth, 0, 0);
                        start_date = (dateFormatter.format(newCalendar.getTime()));
                        manage_start_date_dialog.setText(start_date);

                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        manage_end_date_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ManageAllTask.this, new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        newCalendar.set(year, monthOfYear, dayOfMonth, 0, 0);
                        end_date = (dateFormatter.format(newCalendar.getTime()));
                        manage_end_date_dialog.setText(end_date);

                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(bottom_sheet_edit_task.getWindowToken(), 0);

                task_name = manage_task_name.getText().toString().trim();
                details = create_task_desc.getText().toString().trim();

                if (Validation()) {
                    task_name = MakeSubTextToParse(task_name);
                    details = MakeSubTextToParse(details);
                    ConfirmationAlert();

                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });

        bottom_sheet_edit_task.setVisibility(View.VISIBLE);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        TASK_ID = task_details.get(i).getTask_id();
        task_name = task_details.get(i).getTask_name();
        parent_id = task_details.get(i).getTask_parent_id();
        start_date = task_details.get(i).getTask_start_date();
        end_date = task_details.get(i).getTask_end_date();
    }

    public void EditInit() {

        bahu_thin = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_thin.ttf");
        bahu_bold = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_bold.ttf");

        sub_task_layout = (LinearLayout) findViewById(R.id.sub_task_layout);
        button_layout = (RelativeLayout) findViewById(R.id.button_layout);

        projetname = (TextView) findViewById(R.id.projetname);
        selectassiby = (TextView) findViewById(R.id.selectassiby);
        selectuser = (TextView) findViewById(R.id.selectuser);
        subtask = (TextView) findViewById(R.id.subtask);
        taskname = (TextView) findViewById(R.id.taskname);
        taskpriority = (TextView) findViewById(R.id.taskpriority);
        tasktype = (TextView) findViewById(R.id.tasktype);
        taskstatus = (TextView) findViewById(R.id.taskstatus);
        startdate = (TextView) findViewById(R.id.startdate);
        endtdate = (TextView) findViewById(R.id.endtdate);
        taskdetails = (TextView) findViewById(R.id.taskdetails);

        manage_project_name = (TextView) findViewById(R.id.manage_project_name_task);
        manage_assigned_by = (TextView) findViewById(R.id.manage_assigned_by);
        manage_user_name = (TextView) findViewById(R.id.manage_user_name);
        parent_task_name = (TextView) findViewById(R.id.parent_task_name);
        manage_priority_name = (TextView) findViewById(R.id.manage_priority_name);
        manage_type_name = (TextView) findViewById(R.id.manage_type_name);
        manage_status_name = (TextView) findViewById(R.id.manage_status_name);
        manage_task_name = (EditText) findViewById(R.id.manage_task_name_change);
        create_task_desc = (EditText) findViewById(R.id.create_task_desc);
        manage_start_date_dialog = (Button) findViewById(R.id.manage_start_date_dialog);
        manage_end_date_dialog = (Button) findViewById(R.id.manage_end_date_dialog);

        manage_assign_to_dialog = (Spinner) findViewById(R.id.manage_assign_to_dialog);
        select_task_priority = (Spinner) findViewById(R.id.select_task_priority);
        select_task_type = (Spinner) findViewById(R.id.select_task_type);
        select_status_type = (Spinner) findViewById(R.id.select_status_type);

        submit = (Button) findViewById(R.id.submit);
        cancel = (Button) findViewById(R.id.cancel);

        projetname.setTypeface(bahu_bold);
        selectassiby.setTypeface(bahu_bold);
        selectuser.setTypeface(bahu_bold);
        subtask.setTypeface(bahu_bold);
        taskname.setTypeface(bahu_bold);
        taskpriority.setTypeface(bahu_bold);
        tasktype.setTypeface(bahu_bold);
        taskstatus.setTypeface(bahu_bold);
        startdate.setTypeface(bahu_bold);
        endtdate.setTypeface(bahu_bold);
        taskdetails.setTypeface(bahu_bold);

        manage_project_name.setTypeface(bahu_thin);
        manage_assigned_by.setTypeface(bahu_thin);
        manage_user_name.setTypeface(bahu_thin);
        parent_task_name.setTypeface(bahu_thin);
        manage_priority_name.setTypeface(bahu_thin);
        manage_status_name.setTypeface(bahu_thin);
        manage_type_name.setTypeface(bahu_thin);
        create_task_desc.setTypeface(bahu_thin);
        manage_start_date_dialog.setTypeface(bahu_thin);
        manage_end_date_dialog.setTypeface(bahu_thin);

        submit.setTypeface(bahu_thin);
        cancel.setTypeface(bahu_thin);
    }

    public void SetDetailsToView(Integer i) {

        TaskDetails taskDetails = task_details.get(i);

        Log.d(TAG, "SetDetailsToView: " + taskDetails.getProject_name());
        manage_project_name.setText(taskDetails.getProject_name());
        manage_assigned_by.setText(taskDetails.getTask_assign_by());
        manage_user_name.setText(taskDetails.getTask_assign_to());
        manage_priority_name.setText(taskDetails.getTask_priority());
        manage_type_name.setText(taskDetails.getTask_type());
        manage_status_name.setText(taskDetails.getTask_status());
        manage_task_name.setText(taskDetails.getTask_name());
        manage_start_date_dialog.setText(taskDetails.getTask_start_date());
        manage_end_date_dialog.setText(taskDetails.getTask_end_date());
        manage_start_date_dialog.setClickable(false);
        manage_end_date_dialog.setClickable(false);

        if (taskDetails.getTask_description().isEmpty())
            create_task_desc.setText("No details found");
        else
            create_task_desc.setText(html2text(taskDetails.getTask_description()));

        if (taskDetails.getTask_parent_id().equalsIgnoreCase("0"))
            sub_task_layout.setVisibility(View.GONE);
        else {
            sub_task_layout.setVisibility(View.VISIBLE);
            parent_task_name.setText(taskDetails.getSub_task_name());
        }

        button_layout.setVisibility(View.GONE);

        manage_user_name.setVisibility(View.VISIBLE);
        manage_priority_name.setVisibility(View.VISIBLE);
        manage_type_name.setVisibility(View.VISIBLE);
        manage_status_name.setVisibility(View.VISIBLE);

        manage_assign_to_dialog.setVisibility(View.GONE);
        select_task_priority.setVisibility(View.GONE);
        select_task_type.setVisibility(View.GONE);
        select_status_type.setVisibility(View.GONE);

        manage_task_name.setEnabled(false);
        create_task_desc.setEnabled(false);
        manage_start_date_dialog.setClickable(false);
        manage_end_date_dialog.setClickable(false);
    }

    public void getAllUserDetails(final Integer i) {

        String JSON_URL_TOKEN_EMPLOYEE_DETAILS = BASE_URL + "all_employees.php?";


        Log.d(TAG, "getAllUserDetails: " + JSON_URL_TOKEN_EMPLOYEE_DETAILS);

        try {
            // Volley's json array request object
            JsonArrayRequest req2 = new JsonArrayRequest(JSON_URL_TOKEN_EMPLOYEE_DETAILS,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d("getTaskDetailsData", response.toString());

                            try {
                                if (response.length() > 0) {

                                    EmployeeDetailsReturnJson uds = new EmployeeDetailsReturnJson(response.toString(), getApplicationContext());
                                    employee_detail = uds.EmployeeDetailsReturnJson();

                                    employee_name = new ArrayList<>();
                                    employee_id = new ArrayList<>();

                                    for (EmployeeDetails name : employee_detail) {
                                        employee_name.add(name.getEmployee_name());
                                        employee_id.add(name.getEmployee_id());
                                    }

                                    SetSpinnerValue(i);

                                } else {

                                }
                            } catch (Exception e) {
                                Log.d("getAllUserDetails", "onResponse: " + e);

                            }
                        }

                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Log.e("getAllUserDetails", "Server Error: " + error.getMessage());

                        }
                    });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            int socketTimeout = 600000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req2.setRetryPolicy(policy);

            requestQueue.add(req2);


        } catch (Exception e) {
            Log.d("getTaskDetailsData", "exception: " + e);
        }
    }

    public void SetSpinnerValue(final Integer i) {

        TaskDetails taskDetails = task_details.get(i);

        button_layout.setVisibility(View.VISIBLE);

        manage_user_name.setVisibility(View.GONE);
        manage_priority_name.setVisibility(View.GONE);
        manage_type_name.setVisibility(View.GONE);
        manage_status_name.setVisibility(View.GONE);

        manage_assign_to_dialog.setVisibility(View.VISIBLE);
        select_task_priority.setVisibility(View.VISIBLE);
        select_task_type.setVisibility(View.VISIBLE);
        select_status_type.setVisibility(View.VISIBLE);

        manage_task_name.setEnabled(true);
        create_task_desc.setEnabled(true);
        manage_start_date_dialog.setClickable(true);
        manage_end_date_dialog.setClickable(true);

        manage_project_name.setText(taskDetails.getProject_name());
        manage_assigned_by.setText(taskDetails.getTask_assign_by());
        manage_start_date_dialog.setText(taskDetails.getTask_start_date());
        manage_end_date_dialog.setText(taskDetails.getTask_end_date());
        manage_task_name.setText(task_name);

        if (taskDetails.getTask_parent_id().equalsIgnoreCase("0"))
            sub_task_layout.setVisibility(View.GONE);
        else {
            sub_task_layout.setVisibility(View.VISIBLE);
            parent_task_name.setText(taskDetails.getSub_task_name());
        }

        if (task_details.get(i).getTask_description().isEmpty())
            create_task_desc.setText("No details found");
        else
            create_task_desc.setText(html2text(task_details.get(i).getTask_description()));

        TaskTypeStatusList typeStatusList = new TaskTypeStatusList();

        task_type_list = typeStatusList.getTask_type_list();
        task_priority_list = typeStatusList.getTask_priority_list();
        task_status_list = typeStatusList.getTask_status_list();

        final int color = 0xff2d7da6;

        ArrayAdapter<String> adapter_employee = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, employee_name) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }
        };
        adapter_employee.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        manage_assign_to_dialog.setAdapter(adapter_employee);
        manage_assign_to_dialog.getBackground().setColorFilter(getResources().getColor(R.color.ItemColor), PorterDuff.Mode.SRC_ATOP);
        manage_assign_to_dialog.setSelection(employee_name.indexOf(task_details.get(i).getTask_assign_to()));
        assign_to_id_selected = task_details.get(i).getTask_assign_to_id();

        manage_assign_to_dialog.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int m, long l) {
                assign_to_id_selected = employee_id.get(m);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<String> adapter_task_type = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, task_type_list) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }
        };
        adapter_task_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        select_task_type.setAdapter(adapter_task_type);
        select_task_type.getBackground().setColorFilter(getResources().getColor(R.color.ItemColor), PorterDuff.Mode.SRC_ATOP);
        select_task_type.setSelection(task_type_list.indexOf(task_details.get(i).getTask_type()));
        selected_task_type = task_details.get(i).getTask_type();

        select_task_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int k, long l) {

                selected_task_type = task_type_list.get(k);
                Log.d(TAG, "onItemSelected: type " + selected_task_type);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<String> adapter_task_priority = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, task_priority_list) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }
        };
        adapter_task_priority.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        select_task_priority.setAdapter(adapter_task_priority);
        select_task_priority.getBackground().setColorFilter(getResources().getColor(R.color.ItemColor), PorterDuff.Mode.SRC_ATOP);
        select_task_priority.setSelection(task_priority_list.indexOf(task_details.get(i).getTask_priority()));
        selected_task_priority = task_details.get(i).getTask_priority();

        select_task_priority.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int k, long l) {

                selected_task_priority = task_priority_list.get(k);
                Log.d(TAG, "onItemSelected: type " + selected_task_priority);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<String> adapter_task_status = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, task_status_list);
        adapter_task_status.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        select_status_type.setAdapter(adapter_task_status);
        select_status_type.getBackground().setColorFilter(getResources().getColor(R.color.ItemColor), PorterDuff.Mode.SRC_ATOP);
        select_status_type.setSelection(task_status_list.indexOf(task_details.get(i).getTask_status()));
        selected_task_status = task_details.get(i).getTask_priority();

        select_status_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int n, long l) {

                selected_task_status = task_status_list.get(n);
                Log.d(TAG, "onItemSelected: type " + selected_task_status);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public boolean Validation() {
        boolean valid = true;

        Log.d(TAG, "Validation:  check value" + task_name + selected_task_status + details);

        if (task_name.isEmpty() || selected_task_priority.isEmpty() || selected_task_status.isEmpty() || details.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter all the values", Toast.LENGTH_SHORT).show();
            valid = false;
        }

        if (!start_date.isEmpty() && !end_date.isEmpty()) {
            try {
                Date start = dateFormatter.parse(start_date);
                Date end = dateFormatter.parse(end_date);

                if (end.before(start)) {
                    valid = false;
                    Toast.makeText(getApplicationContext(), "End date must be after start date", Toast.LENGTH_SHORT).show();
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (start_date.isEmpty()) {
            valid = false;
            manage_start_date_dialog.setError("");
        } else if (end_date.isEmpty()) {
            valid = false;
            manage_end_date_dialog.setError("");
        } else {
            valid = false;
            manage_start_date_dialog.setError("");
            manage_end_date_dialog.setError("");
        }


        return valid;

    }

    public String MakeSubTextToParse(String name) {

        String[] name1 = name.split("\\s");

        name = "";
        for (String n2 : name1) {
            Log.d("n2", "" + n2);
            name += n2 + "%20";
        }
        return name;

    }

    public static String html2text(String html) {
        return Html.fromHtml(html).toString();
    }

    public void ConfirmationAlert() {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle("Please confirm to submit");

        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (!parent_id.equalsIgnoreCase("0")) {
                    JSON_URL_TOKEN_EDIT_TASK = BASE_URL + "edit_task.php?task_id=" + TASK_ID + "&task_name=" + task_name + "&assign_by=" + user_emp_id + "&assign_to=" + assign_to_id_selected + "&task_priority=" + selected_task_priority + "&task_status=" + selected_task_status + "&task_type=" + selected_task_type + "&start_date=" + start_date + "&end_date=" + end_date + "&task_desc=" + details;
                } else {

                    JSON_URL_TOKEN_EDIT_TASK = BASE_URL + "edit_task.php?task_id=" + TASK_ID + "&task_name=" + task_name + "&assign_by=" + user_emp_id + "&assign_to=" + assign_to_id_selected + "&task_priority=" + selected_task_priority + "&task_status=" + selected_task_status + "&task_type=" + selected_task_type + "&start_date=" + start_date + "&end_date=" + end_date + "&task_desc=" + details;
                }


                try {
                    Log.d(TAG, "onClick:   json " + JSON_URL_TOKEN_EDIT_TASK);
                    StringRequest req2 = new StringRequest(JSON_URL_TOKEN_EDIT_TASK,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("MainActivity", response.toString());

                                    try {
                                        if (response.length() > 0) {
                                            if (response.toString().equalsIgnoreCase("1")) {
                                                Toast.makeText(getApplicationContext(), "Task has been updated successfully", Toast.LENGTH_SHORT).show();
                                                dialog2.dismiss();
                                                behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                                                getAssignTaskDetailsData();
                                            } else {
                                                Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                            }

                                        } else {
                                            Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                            dialog2.dismiss();
                                            behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                                        }
                                    } catch (Exception e) {
                                        Log.d("MainActivity", "onResponse: " + e);
                                        Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                                        dialog2.dismiss();
                                        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                                    }
                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("MainActivity", "Server Error: " + error.getMessage());
                            Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                            dialog2.dismiss();
                            behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                        }
                    });

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                    int socketTimeout = 600000;//30 seconds - change to what you want
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    req2.setRetryPolicy(policy);

                    requestQueue.add(req2);

                } catch (Exception e) {
                    Log.d("MainActivity", "getCustomer: " + e);
                    Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                    dialog2.dismiss();
                    behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
            }
        });

        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialog2.dismiss();
            }
        });


        dialog2 = dialog.show();

    }

    public void DeleteTask(final Integer i) {
        Snackbar snackbar;
        snackbar = Snackbar.make(swipe_layout, "Please confirm to delete", 3000);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(getResources().getColor(R.color.ButtonBackGround));
        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.setAction("Confirm", new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                JSON_URL_TOKEN_DELETE_TASK = BASE_URL + "delete_task.php?task_id=" + task_details.get(i).getTask_id();


                try {
                    Log.d(TAG, "onClick:   json" + JSON_URL_TOKEN_DELETE_TASK);
                    StringRequest req2 = new StringRequest(JSON_URL_TOKEN_DELETE_TASK,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("MainActivity", response.toString());

                                    try {
                                        if (response.length() > 0) {
                                            if (response.toString().equalsIgnoreCase("1")) {
                                                Toast.makeText(getApplicationContext(), "Task has been deleted successfully", Toast.LENGTH_SHORT).show();
                                                task_details.remove(i);
                                                managetask_project_recycler.getAdapter().notifyItemRemoved(i);
                                            } else {
                                                Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();

                                            }

                                        } else {
                                            Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();

                                        }
                                    } catch (Exception e) {
                                        Log.d("MainActivity", "onResponse: " + e);
                                        Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();

                                    }
                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("MainActivity", "Server Error: " + error.getMessage());
                            Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();

                        }
                    });

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    int socketTimeout = 600000;//30 seconds - change to what you want
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    req2.setRetryPolicy(policy);
                    requestQueue.add(req2);

                } catch (Exception e) {
                    Log.d("MainActivity", "getCustomer: " + e);
                    Toast.makeText(getApplicationContext(), "Server error! please try after some time", Toast.LENGTH_SHORT).show();
                }
            }
        });
        snackbar.show();
    }

    @Override
    public void onBackPressed() {
        if (behavior != null) {
            if (behavior.getState() != BottomSheetBehavior.STATE_HIDDEN)
                behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        myReceiver = new ConnectivityReceiver();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (myReceiver != null)
            unregisterReceiver(myReceiver);
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected(getApplicationContext());
        return isConnected;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.fab_open:
                SetShortCutLayout();
                break;
        }
    }

    public void SetShortCutLayout() {

        LayoutInflater inflate = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
        Snackbar snackbar = Snackbar.make(findViewById(R.id.contentmanage_task), "", 1000);
// Get the Snackbar's layout view
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);
        layout.setBackgroundColor(Color.parseColor("#3581a1"));
// Inflate our custom view
        View snackView = inflate.inflate(R.layout.snackbar_layout, null);
// Configure the view

        RelativeLayout dashboard = (RelativeLayout) snackView.findViewById(R.id.dashboard);
        RelativeLayout your_task = (RelativeLayout) snackView.findViewById(R.id.your_task);
        RelativeLayout all_task = (RelativeLayout) snackView.findViewById(R.id.all_task);
        RelativeLayout group_chat = (RelativeLayout) snackView.findViewById(R.id.group_chat);

        dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                in_shortcut = new Intent(getApplicationContext(), Dashboard.class);
                startActivity(in_shortcut);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        your_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                in_shortcut = new Intent(getApplicationContext(), NewTask_SubTask.class);
                in_shortcut.putExtra("sub_task", "no");
                startActivity(in_shortcut);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        all_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                in_shortcut = new Intent(getApplicationContext(), SeeYourAllTasks.class);
                startActivity(in_shortcut);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        group_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
// Add the view to the Snackbar's layout
        layout.addView(snackView, 0);
// Show the Snackbar
        snackbar.show();
    }
}
