package com.velarudh.tms.task;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.velarudh.tms.R;
import com.velarudh.tms.json.ProjectDetailsReturnJson;
import com.velarudh.tms.json.EmployeeDetailsReturnJson;
import com.velarudh.tms.main.Dashboard;
import com.velarudh.tms.model.EmployeeDetails;
import com.velarudh.tms.model.ProjectDetails;
import com.velarudh.tms.model.ProjectType;
import com.velarudh.tms.model.TaskDetails;
import com.velarudh.tms.model.TaskTypeStatusList;
import com.velarudh.tms.util.ConnectivityReceiver;
import com.velarudh.tms.util.ExpandableTextViewTwo;
import com.velarudh.tms.util.NetErrorActivity;
import com.velarudh.tms.util.Url;
import com.velarudh.tms.util.UserSessionManager;

import org.json.JSONArray;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class NewTask_SubTask extends AppCompatActivity implements View.OnClickListener {

    String TAG = "NewTask_SubTask";

    RelativeLayout content_new_task;

    Toolbar toolbar;
    Spinner select_project_name;
    Spinner select_user;
    Spinner task_priority;
    Spinner task_type;
    Spinner project_type;

    TextView projecttype;
    TextView projetname;
    TextView selectuser;
    TextView subtask;
    TextView taskname;
    TextView taskpriority;
    TextView tasktype;
    TextView startdate;
    TextView endtdate;
    TextView taskdetails;

    ExpandableTextViewTwo details_project_name;
    LinearLayout project_type_layout;
    LinearLayout sub_task_layout;
    LinearLayout task_layout;
    LinearLayout dynamic_add_layout;

    ImageButton add_more_task;

    EditText create_task_name;
    TextView create_sub_task_name;
    EditText create_task_desc;

    Button start_date;
    Button end_date;

    Button add_task;

    TaskDetails td;

    ArrayList<String> MultipleTask;
    ArrayList<ProjectType> project_type_name;
    ArrayList<String> project_type_list;
    ArrayList<String> project_type_id_list;

    ArrayList<EmployeeDetails> employee_detail;
    ArrayList<ProjectDetails> project_detail;

    ArrayList<String> employee_name;
    ArrayList<String> employee_id;
    ArrayList<String> project_name;
    ArrayList<String> project_id;
    ArrayList<String> task_priority_list;
    ArrayList<String> task_type_list;

    FloatingActionButton fab_open;

    Intent in = null;

    Calendar newCalendar = Calendar.getInstance();
    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd");

    ArrayList<TaskDetails> taskDetailses;

    String employee_name_selected;
    String employee_id_selected;
    String project_name_selected;
    String project_name_id_selected;
    String task_priority_selected;
    String task_type_selected;
    String select_task_name;
    String task_desc;
    String start_date_select;
    String end_date_select;
    String project_type_selected;
    String project_type_id_selected;

    String JSON_URL_TOKEN_NEW_TASK;

    UserSessionManager usm;
    HashMap<String, String> user_details;
    String U_EMP_ID = "user_id";
    String U_NAME = "user_name";
    String user_emp_id;
    String user_name;

    ConnectivityReceiver myReceiver;

    String BASE_URL = Url.BASE_URL;

    Typeface bahu_thin;
    Typeface bahu_bold;

    String SUB_TASK_TYPE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task__sub_task);

        if (checkConnection()) {
            init();
            PrepareData();
        } else {
            Intent in = new Intent(getApplicationContext(), NetErrorActivity.class);
            in.putExtra("layout_flag", "1");
            startActivity(in);
        }
    }

    public void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(R.string.title_activity_new_task__sub_task);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        content_new_task = (RelativeLayout) findViewById(R.id.content_new_task);
        project_type_layout = (LinearLayout) findViewById(R.id.project_type_layout);
        sub_task_layout = (LinearLayout) findViewById(R.id.sub_task_layout);
        task_layout = (LinearLayout) findViewById(R.id.task_layout);
        dynamic_add_layout = (LinearLayout) findViewById(R.id.dynamic_add_layout);
        add_more_task = (ImageButton) findViewById(R.id.add_more_task);

        projecttype = (TextView) findViewById(R.id.projecttype);
        projetname = (TextView) findViewById(R.id.projetname);
        selectuser = (TextView) findViewById(R.id.selectuser);
        subtask = (TextView) findViewById(R.id.subtask);
        taskname = (TextView) findViewById(R.id.taskname);
        taskpriority = (TextView) findViewById(R.id.taskpriority);
        tasktype = (TextView) findViewById(R.id.tasktype);
        startdate = (TextView) findViewById(R.id.startdate);
        endtdate = (TextView) findViewById(R.id.endtdate);
        taskdetails = (TextView) findViewById(R.id.taskdetails);

        select_project_name = (Spinner) findViewById(R.id.select_project_name);
        select_user = (Spinner) findViewById(R.id.select_user);
        task_priority = (Spinner) findViewById(R.id.task_priority);
        task_type = (Spinner) findViewById(R.id.task_type);
        project_type = (Spinner) findViewById(R.id.select_project_type);

        details_project_name = (ExpandableTextViewTwo) findViewById(R.id.details_project_name);
        create_task_name = (EditText) findViewById(R.id.create_task_name);
        create_sub_task_name = (TextView) findViewById(R.id.create_sub_task_name);
        create_task_desc = (EditText) findViewById(R.id.create_task_desc);

        start_date = (Button) findViewById(R.id.start_date);
        end_date = (Button) findViewById(R.id.end_date);
        add_task = (Button) findViewById(R.id.add_task);

        fab_open = (FloatingActionButton) findViewById(R.id.fab_open);

        fab_open.setOnClickListener(this);
        start_date.setOnClickListener(this);
        end_date.setOnClickListener(this);
        add_task.setOnClickListener(this);
        add_more_task.setOnClickListener(this);

        employee_name = new ArrayList<>();
        employee_id = new ArrayList<>();

        task_priority_list = new ArrayList<>();
        task_type_list = new ArrayList<>();

        employee_name.add("Select");
        employee_id.add("0");

        usm = new UserSessionManager(this);

        user_details = usm.getUserDetails();
        user_emp_id = user_details.get(U_EMP_ID);
        user_name = user_details.get(U_NAME);

        SUB_TASK_TYPE = getIntent().getStringExtra("sub_task").trim();
        SetTypeFace();

    }

    public void SetTypeFace() {

        bahu_thin = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_thin.ttf");
        bahu_bold = Typeface.createFromAsset(getAssets(), "fonts/bauhaus_bold.ttf");

        projecttype.setTypeface(bahu_bold);
        projetname.setTypeface(bahu_bold);
        selectuser.setTypeface(bahu_bold);
        subtask.setTypeface(bahu_bold);
        taskname.setTypeface(bahu_bold);
        taskpriority.setTypeface(bahu_bold);
        tasktype.setTypeface(bahu_bold);
        startdate.setTypeface(bahu_bold);
        endtdate.setTypeface(bahu_bold);
        taskdetails.setTypeface(bahu_bold);

        details_project_name.setTypeface(bahu_thin);
        create_task_name.setTypeface(bahu_thin);
        create_sub_task_name.setTypeface(bahu_thin);
        create_task_desc.setTypeface(bahu_thin);

        start_date.setTypeface(bahu_thin);
        end_date.setTypeface(bahu_thin);
        add_task.setTypeface(bahu_thin);

    }

    public void PrepareData() {
        getAllUserDetails();
        TaskTypeStatusList typeStatusList = new TaskTypeStatusList();
        task_priority_list = typeStatusList.getTask_priority_list();
        task_type_list = typeStatusList.getTask_type_list();
    }

    public void getAllUserDetails() {

        String JSON_URL_TOKEN_EMPLOYEE_DETAILS = BASE_URL + "all_employees.php?";


        Log.d(TAG, "getAllUserDetails: " + JSON_URL_TOKEN_EMPLOYEE_DETAILS);

        try {
            // Volley's json array request object
            JsonArrayRequest req2 = new JsonArrayRequest(JSON_URL_TOKEN_EMPLOYEE_DETAILS,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d("getTaskDetailsData", response.toString());

                            try {
                                if (response.length() > 0) {

                                    EmployeeDetailsReturnJson uds = new EmployeeDetailsReturnJson(response.toString(), getApplicationContext());
                                    employee_detail = uds.EmployeeDetailsReturnJson();

                                    Log.d(TAG, "onResponse: " + employee_detail.toString());
                                    for (EmployeeDetails name : employee_detail) {
                                        employee_name.add(name.getEmployee_name());
                                        employee_id.add(name.getEmployee_id());
                                    }
                                    getProjectDetails();

                                } else {

                                }
                            } catch (Exception e) {
                                Log.d("getAllUserDetails", "onResponse: " + e);

                            }
                        }

                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Log.e("getAllUserDetails", "Server Error: " + error.getMessage());

                        }
                    });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            int socketTimeout = 600000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req2.setRetryPolicy(policy);

            requestQueue.add(req2);


        } catch (Exception e) {
            Log.d("getTaskDetailsData", "exception: " + e);
        }
    }

    public void getProjectDetails() {

        String JSON_URL_TOKEN_PROJECT_DETAILS = BASE_URL + "all_project_list.php?";


        Log.d(TAG, "getProjectDetails: " + JSON_URL_TOKEN_PROJECT_DETAILS);

        try {
            // Volley's json array request object
            JsonArrayRequest req2 = new JsonArrayRequest(JSON_URL_TOKEN_PROJECT_DETAILS,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d("getProjectDetails", response.toString());

                            try {
                                if (response.length() > 0) {

                                    ProjectDetailsReturnJson pds = new ProjectDetailsReturnJson(response.toString());
                                    project_detail = pds.ProjectDetailsReturnJson();

                                    Log.d(TAG, "onResponse: project " + project_detail.size());
                                    setData();

                                } else {

                                }
                            } catch (Exception e) {
                                Log.d("getProjectDetails", "onResponse: " + e);

                            }
                        }

                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Log.e("getProjectDetails", "Server Error: " + error.getMessage());

                        }
                    });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            int socketTimeout = 600000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req2.setRetryPolicy(policy);

            requestQueue.add(req2);


        } catch (Exception e) {
            Log.d("getTaskDetailsData", "exception: " + e);
        }
    }

    public void setData() {

        final int color = 0xff2d7da6;

        ArrayAdapter<String> adapter_user = new ArrayAdapter<String>(this, R.layout.spinner_item, employee_name) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }
        };
        adapter_user.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        select_user.setAdapter(adapter_user);
        select_user.getBackground().setColorFilter(getResources().getColor(R.color.ItemColor), PorterDuff.Mode.SRC_ATOP);

        ArrayAdapter<String> adapter_priority = new ArrayAdapter<String>(this, R.layout.spinner_item, task_priority_list) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }
        };
        adapter_priority.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        task_priority.setAdapter(adapter_priority);
        task_priority.getBackground().setColorFilter(getResources().getColor(R.color.ItemColor), PorterDuff.Mode.SRC_ATOP);

        ArrayAdapter<String> adapter_task_type = new ArrayAdapter<String>(this, R.layout.spinner_item, task_type_list) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(color);
                ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                return v;
            }
        };
        adapter_task_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        task_type.setAdapter(adapter_task_type);
        task_type.getBackground().setColorFilter(getResources().getColor(R.color.ItemColor), PorterDuff.Mode.SRC_ATOP);

        Log.d(TAG, "setData: " + getIntent().getStringExtra("sub_task"));

        if (SUB_TASK_TYPE.equalsIgnoreCase("yes")) {
            taskDetailses = getIntent().getParcelableArrayListExtra("select_task");
            td = taskDetailses.get(0);

            details_project_name.setVisibility(View.VISIBLE);
            details_project_name.setText(td.getProject_name());
            select_project_name.setVisibility(View.GONE);

            sub_task_layout.setVisibility(View.VISIBLE);
            create_sub_task_name.setText(td.getTask_name());

            project_type_id_selected = td.getProject_type_id();
        } else {
            project_type_layout.setVisibility(View.VISIBLE);
            details_project_name.setVisibility(View.GONE);
            project_type_name = new ArrayList<>();
            project_type_list = new ArrayList<>();
            project_type_id_list = new ArrayList<>();

            project_type_list.add("Select");
            project_type_id_list.add("0");

            for (ProjectDetails project : project_detail) {

                String type = project.getProject_type();
                String name = project.getProject_name();
                String type_id = project.getProject_type_id();
                String name_id = project.getProject_id();

                ProjectType projectType = new ProjectType();

                if (project_type_list.isEmpty()) {
                    project_type_list.add(type);
                    project_type_id_list.add(type_id);
                } else {
                    if (!project_type_list.contains(type)) {
                        project_type_list.add(type);
                        project_type_id_list.add(type_id);
                    }
                }
                projectType.project_type = type;
                projectType.project_name = name;
                projectType.project_type_id = type_id;
                projectType.project_name_id = name_id;

                project_type_name.add(projectType);

            }

            ArrayAdapter<String> adapter_project_type = new ArrayAdapter<String>(this, R.layout.spinner_item, project_type_list) {
                @NonNull
                @Override
                public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);
                    ((TextView) v).setTextColor(color);
                    ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                    return v;
                }

                @Override
                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);
                    ((TextView) v).setTextColor(color);
                    ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                    return v;
                }
            };
            adapter_project_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            project_type.setAdapter(adapter_project_type);
            project_type.getBackground().setColorFilter(getResources().getColor(R.color.ItemColor), PorterDuff.Mode.SRC_ATOP);

            project_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    project_type_selected = project_type_list.get(i);
                    project_type_id_selected = project_type_id_list.get(i);

                    project_name = new ArrayList<String>();
                    project_id = new ArrayList<String>();

                    project_name.add("Select");
                    project_id.add("0");

                    for (ProjectType hp : project_type_name) {
                        if (hp.project_type.equalsIgnoreCase(project_type_selected)) {
                            Log.d(TAG, "onItemSelected:    " + hp.project_name);
                            project_name.add(hp.project_name);
                            project_id.add(hp.project_name_id);
                        }
                    }

                    ArrayAdapter<String> adapter_project = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, project_name) {
                        @NonNull
                        @Override
                        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            View v = super.getView(position, convertView, parent);
                            ((TextView) v).setTextColor(color);
                            ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                            return v;
                        }

                        @Override
                        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            View v = super.getView(position, convertView, parent);
                            ((TextView) v).setTextColor(color);
                            ((TextView) v).setTypeface(bahu_thin);//Typeface for normal view
                            return v;
                        }
                    };
                    adapter_project.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    select_project_name.setAdapter(adapter_project);
                    select_project_name.getBackground().setColorFilter(getResources().getColor(R.color.ItemColor), PorterDuff.Mode.SRC_ATOP);

                    select_project_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            project_name_selected = project_name.get(i).trim();
                            project_name_id_selected = project_id.get(i);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
        }

        select_user.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                employee_id_selected = employee_id.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.fab_open:
                SetShortCutLayout();
                break;

            case R.id.start_date:
                new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        newCalendar.set(year, monthOfYear, dayOfMonth, 0, 0);
                        start_date.setText(dateFormatter.format(newCalendar.getTime()));

                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;

            case R.id.end_date:
                new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        newCalendar.set(year, monthOfYear, dayOfMonth, 0, 0);
                        end_date.setText(dateFormatter.format(newCalendar.getTime()));

                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)).show();

                break;

            case R.id.add_task:
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(content_new_task.getWindowToken(), 0);

                if (Validation()) {
                    task_desc = MakeSubTextToParse(task_desc);

                    if (SUB_TASK_TYPE.equalsIgnoreCase("yes")) {
                        JSON_URL_TOKEN_NEW_TASK = BASE_URL + "new_task.php?user_id=" + user_emp_id + "&project_id=" + td.project_id + "&parent_task_id=" + td.getTask_id() + "&task_name=" + MultipleTask + "&assign_to=" + employee_id_selected + "&assign_by=" + user_emp_id + "&task_priority=" + task_priority_selected + "&task_type=" + task_type_selected + "&start_date=" + start_date_select + "&end_date=" + end_date_select + "&desc=" + task_desc;
                    } else {
                        JSON_URL_TOKEN_NEW_TASK = BASE_URL + "new_task.php?user_id=" + user_emp_id + "&project_id=" + project_name_id_selected + "&parent_task_id=0" + "&task_name=" + MultipleTask + "&assign_to=" + employee_id_selected + "&assign_by=" + user_emp_id + "&task_priority=" + task_priority_selected + "&task_type=" + task_type_selected + "&start_date=" + start_date_select + "&end_date=" + end_date_select + "&desc=" + task_desc;

                    }

                    try {
                        // Volley's json array request object
                        Log.d(TAG, "onClick: JSON   " + JSON_URL_TOKEN_NEW_TASK);
                        StringRequest req2 = new StringRequest(JSON_URL_TOKEN_NEW_TASK,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Log.d("MainActivity", response.toString());

                                        try {
                                            if (response.length() > 0) {
                                                if (response.toString().equalsIgnoreCase("1")) {
                                                    Snackbar snackbar = Snackbar.make(project_type_layout, "Task has been assigned to " + employee_name_selected + " successfully", Snackbar.LENGTH_SHORT);
                                                    snackbar.show();
                                                    onBackPressed();
                                                }

                                            } else {
                                                Snackbar snackbar = Snackbar.make(project_type_layout, "Server error! please try after some time", Snackbar.LENGTH_SHORT);
                                                snackbar.show();
                                            }
                                        } catch (Exception e) {
                                            Log.d("MainActivity", "onResponse: " + e);
                                            Snackbar snackbar = Snackbar.make(project_type_layout, "Server error! please try after some time", Snackbar.LENGTH_SHORT);
                                            snackbar.show();
                                        }
                                    }

                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("MainActivity", "Server Error: " + error.getMessage());
                                Snackbar snackbar = Snackbar.make(project_type_layout, "Server error! please try after some time", Snackbar.LENGTH_SHORT);
                                snackbar.show();
                            }
                        });

                        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                        int socketTimeout = 600000;//30 seconds - change to what you want
                        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                        req2.setRetryPolicy(policy);

                        requestQueue.add(req2);

                    } catch (Exception e) {
                        Log.d("MainActivity", "getCustomer: " + e);
                        Snackbar snackbar = Snackbar.make(project_type_layout, "Server error! please try after some time", Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    }
                }
                break;

            case R.id.add_more_task:
                validateDynamicLayout();
                break;


        }
    }

    public void validateDynamicLayout() {
        if (dynamic_add_layout.getChildCount() > 0) {
            EditText create_task_name_ = (EditText) dynamic_add_layout.getChildAt(dynamic_add_layout.getChildCount() - 1).findViewById(R.id.create_task_name_);
            if (create_task_name_.getText().toString().trim().isEmpty()) {
                create_task_name_.setError("cannot left empty");
            } else
                AddMoreTask();
        } else {
            if (create_task_name.getText().toString().trim().isEmpty()) {
                create_task_name.setError("cannot left empty");
            } else
                AddMoreTask();
        }

    }

    public void AddMoreTask() {
        Log.d(TAG, "AddMoreTask: " + dynamic_add_layout.getChildCount());

        LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
        final View layer1 = inflater.inflate(R.layout.add_task_dynamic, null);

        EditText create_task_name_ = (EditText) layer1.findViewById(R.id.create_task_name_);
        create_task_name_.setTypeface(bahu_thin);
        ImageButton remove_view = (ImageButton) layer1.findViewById(R.id.remove_view);
        ImageButton add_view = ((ImageButton) layer1.findViewById(R.id.add_view));

        remove_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ViewGroup) layer1.getParent()).removeView(layer1);
            }
        });

        add_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateDynamicLayout();
            }
        });
        dynamic_add_layout.addView(layer1);
    }

    public boolean Validation() {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");

        MultipleTask = new ArrayList<>();

        boolean valid = true;

        employee_name_selected = select_user.getSelectedItem().toString().trim();
        task_priority_selected = task_priority.getSelectedItem().toString().trim();
        task_type_selected = task_type.getSelectedItem().toString().trim();

        select_task_name = create_task_name.getText().toString().trim();
        task_desc = create_task_desc.getText().toString().trim();

        start_date_select = start_date.getText().toString().trim();
        end_date_select = end_date.getText().toString().trim();

        if (!SUB_TASK_TYPE.equalsIgnoreCase("yes")) {
            if (project_type_selected.isEmpty() || project_type_selected.equalsIgnoreCase("Select")) {
                valid = false;
                Toast.makeText(getApplicationContext(), "Please select project type", Toast.LENGTH_SHORT).show();
            }
            if (project_name_selected.isEmpty() || project_name_selected.equalsIgnoreCase("Select")) {
                valid = false;
                Toast.makeText(getApplicationContext(), "Please select project name", Toast.LENGTH_SHORT).show();
            }
        } else {
            try {
                Date parent_task_end = simpleDateFormat.parse(td.getTask_end_date());
                Date end = simpleDateFormat.parse(end_date_select);

                if (end.after(parent_task_end)) {
                    valid = false;
                    Toast.makeText(getApplicationContext(), "End date must be before of parent task end date", Toast.LENGTH_SHORT).show();
                    end_date.setError("End date must be before of parent task end date");
                } else {
                    end_date.setError(null);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (employee_name_selected.isEmpty() || employee_name_selected.equalsIgnoreCase("Select")) {
            valid = false;
            Toast.makeText(getApplicationContext(), "Please select employee name", Toast.LENGTH_SHORT).show();
        }
        if (task_priority_selected.isEmpty() || task_priority_selected.equalsIgnoreCase("Select")) {
            valid = false;
            Toast.makeText(getApplicationContext(), "Please select task priority", Toast.LENGTH_SHORT).show();
        }
        if (task_type_selected.isEmpty() || task_type_selected.equalsIgnoreCase("Select")) {
            valid = false;
            Toast.makeText(getApplicationContext(), "Please select task type", Toast.LENGTH_SHORT).show();
        }

        if (select_task_name.isEmpty()) {
            valid = false;
            create_task_name.setError("Enter task name");

        } else {
            create_task_name.setError(null);
            MultipleTask.add(select_task_name);
        }
        /// this validation for both the cases is necessary
        if (task_desc.isEmpty()) {
            valid = false;
            create_task_desc.setError("Enter task description");
        } else {
            create_task_desc.setError(null);
        }

        if (start_date_select.equalsIgnoreCase("SELECT DATE")) {
            valid = false;
            start_date.setError("Select start date of task");
        } else {
            start_date.setError(null);
        }
        if (end_date_select.equalsIgnoreCase("SELECT DATE")) {
            valid = false;
            end_date.setError("Select end date of task");
        } else {

            try {
                Date start = simpleDateFormat.parse(start_date_select);
                Date end = simpleDateFormat.parse(end_date_select);

                if (end.before(start)) {
                    valid = false;
                    end_date.setError("End date must be after start date");
                } else {
                    end_date.setError(null);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (dynamic_add_layout.getChildCount() != 0) {
            for (int i = 0; i < dynamic_add_layout.getChildCount(); i++) {
                EditText task = (EditText) dynamic_add_layout.getChildAt(i).findViewById(R.id.create_task_name_);
                String value = task.getText().toString().trim();
                if (value.isEmpty()) {
                    valid = false;
                    task.setError("Enter task name");
                } else
                    MultipleTask.add(value);
            }
        }

        Log.d(TAG, "Validation: " + MultipleTask.toString());

        return valid;

    }

    public void SetShortCutLayout() {

        LayoutInflater inflate = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
        Snackbar snackbar = Snackbar.make(findViewById(R.id.content_new_task), "", 1000);
// Get the Snackbar's layout view
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);
        layout.setBackgroundColor(Color.parseColor("#3581a1"));
// Inflate our custom view
        View snackView = inflate.inflate(R.layout.snackbar_layout, null);
// Configure the view

        RelativeLayout dashboard = (RelativeLayout) snackView.findViewById(R.id.dashboard);
        RelativeLayout your_task = (RelativeLayout) snackView.findViewById(R.id.your_task);
        RelativeLayout all_task = (RelativeLayout) snackView.findViewById(R.id.all_task);
        RelativeLayout group_chat = (RelativeLayout) snackView.findViewById(R.id.group_chat);

        dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                in = new Intent(getApplicationContext(), Dashboard.class);
                startActivity(in);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        your_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                in = new Intent(getApplicationContext(), SeeYourAllTasks.class);
                startActivity(in);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        all_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                in = new Intent(getApplicationContext(), ManageAllTask.class);
                startActivity(in);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        group_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
// Add the view to the Snackbar's layout
        layout.addView(snackView, 0);
// Show the Snackbar
        snackbar.show();
    }

    public String MakeSubTextToParse(String name) {

        String[] name1 = name.split("\\s");

        name = "";
        for (String n2 : name1) {
            Log.d("n2", "" + n2);
            name += n2 + "%20";
        }
        return name;

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        myReceiver = new ConnectivityReceiver();
        registerReceiver(myReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(myReceiver);
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected(getApplicationContext());
        return isConnected;
    }
}
